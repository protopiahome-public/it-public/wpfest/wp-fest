


jQuery(document).ready(function($)
	{
	$("[name='fmru_evlbl_roles[]']").live({change: evt =>
	{
		send( [ 'fmru_evlbl_roles' , $("[name='fmru_evlbl_roles[]']:checked").map( (i,e) => e.value ).get() ]);
	}});
	$('.fmru_stick_install').live({click:function(evt)
	{
		send( [ 'fmru_stick_install' ] );
	}});
	$('#user_id').live({change:function(evt)
	{
		send( [ 'fmru_user_cnt', $(evt.currentTarget).val() ] );
	}});
	$('#frmru_add_users').live({click:function(evt)
	{
		var data = {
			roles:	$("[name='fmru_add_user[]']:checked").map((n,e) => e.value).get(),
			ganres: $("[name='chexp[]']:checked").map((n,e) => e.value).get(),
			count: $("#add_experts_count").val(),
		};
		send( [ 'frmru_add_users', data] );
	}});
	$('.fmru_stick_close').live({click:function(evt)
	{
		send( [ 'fmru_stick_close' ] );
	}});
	$('#enabled_rules').live({click:function(evt)
	{
		send( [ 'enabled_rules', $(evt.currentTarget).is(":checked") ? 1 : 0 ] );
	}});
	$('.smc_desmiss_button').live({click:function(evt)
	{
		jQuery("#install_xx_notice").detach();
	}});
	$('#add_member').live({click:function(evt)
	{
		var count = parseInt($("#add_member_count").val());
		var ganres = $.map($("[name='chmemb[]']:checked"), e => e.value );
		//console.log(ganres.join(","));
		if(!count)
		{
			a_alert("Set number of new Members more then 0");
			return;
		}
		send( [ 'add_member', count, ganres.join(",") ] );
	}});
})
var ssend = params =>
{
	var fd = new FormData();
	fd.append("type", params[0]);
	fd.append("data", params[1]);
	jQuery.post	(
		mpp.url,
		{
			action	: 'mpp',
			nonce	: mpp.nonce,
			params	: params
		},
		response =>
		{
			//console.log(response);
		}
	);
}
var send = function (params)
{
	//console.log(params);
	jQuery.post	(
		myajax.url,
		{
			action	: 'myajax',
			nonce	: myajax.nonce,
			params	: params
		},
		function( response ) 
		{
			//console.log(response);
			try
			{
				var dat		= JSON.parse(response);
			}
			catch (e){return;}
			//alert(dat);
			var command	= dat[0];
			var datas	= dat[1];
			//console.log(command);
			switch(command)
			{
				case "fmru_stick_install":
					jQuery("#install_xx_notice").detach();
					break;
				case "fmru_stick_close":
					jQuery("#install_xx_notice").detach();
					break;
				default:
					var customEvent = new CustomEvent("_send_", {bubbles : true, cancelable : true, detail : dat})
					document.documentElement.dispatchEvent(customEvent);					
					break;
			}
			
			if(datas['exec'] && datas['exec'] != "")
			{
				window[datas['exec']](datas['args']);
			}
			if(datas['a_alert'])
			{
				a_alert(datas['a_alert']);
			}
		}		
	);
} 
<?php
/**
 * Festival page template (for FestPage)
 *
 * @since    1.0.0
 */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php header("Cache-Control: no-store, no-cache, must-revalidate");?>
<html <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php wp_head(); ?>		
		<?php do_action("ermak_body_script"); ?>
		
	</head>
	<body id="<?php print get_stylesheet(); ?>" <?php body_class(); ?>>
		<?php FmRU::$options["include_header"] ? get_header() : ""; ?>
		<?php echo apply_filters( 'ermak_body_before', "" ); 
		global $wp;////
		//echo "<h1>". add_query_arg( [], $wp->request ) . " -- ".  ( stripos(add_query_arg( [], $wp->request ), "mru_member" ) ) ."</h1>";
		wp_footer(); ?>
	</body>
	<script>
		
	</script>

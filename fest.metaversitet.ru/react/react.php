<?php
function send_react()
{
	return '
	<div id="sample"></div>
	<script type="text/babel">
	var TimerExample = React.createClass({

		getInitialState: function()
		{
			return { elapsed: 0 };
		},
		componentDidMount: function()
		{
			this.timer = setInterval(this.tick, 25);
		},
		componentWillUnmount: function()
		{
			clearInterval(this.timer);
		},
		tick: function()
		{  
			this.setState({elapsed: new Date() - this.props.start});
		},
		render: function() 
		{
			var elapsed = Math.round(this.state.elapsed/10);
			var seconds = (elapsed / 100).toFixed( 2 );    
			return <section>
				<div className="container">
					<div className="display-4 text-center">Этот пример запущен <b>{seconds} сек.</b> назад</div>
				</div>
			</section>;
		}
	});

	ReactDOM.render( <TimerExample start={Date.now()} />, document.getElementById("sample"));
	</script>

	';
}
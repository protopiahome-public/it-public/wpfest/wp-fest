<?php
function get_react()
{
	$is_expert		= FmRU_Expert::is_expert();
	$raitings 		= FmRU::get_raiting_data();
	$all_memebers 	= FmRU_Member::get_all(-1, -1, 0, "meta_value_num", "ASC", "order");
	$display_name 	= is_user_logged_in() ? wp_get_current_user()->display_name : "";
	$logo 			= wp_get_attachment_image_src(FmRU::$options['logotype_0'], "full")[0];
	$members		= array();
	foreach($all_memebers as $member)
	{
		$mem = FmRU_Member::get_instance($member);
		$rat = 0;
		$experts = array();
		foreach($raitings as $rait)
		{
			if($rait['member_id'] == $mem->id)
			{
				$experts[] = $rait['expert_id'];
				if($is_expert && $rait['expert_id'] != get_current_user_id())
					continue;
				$rat += $rait['raiting'];
			}
		}
		$mmbr 		= new stdClass;
		$mmbr->cl 	= $rat ? '' : 'noselect';
		$mmbr->id 	= $mem->id;
		$mmbr->title= $mem->get("post_title");
		$mmbr->o 	= $mem->get_meta("order");
		$mmbr->e 	= count(array_unique($experts));
		$mmbr->img	= $mem->get_thrumbnail();
		$terms		= get_the_terms( $mem->id, FRMRU_GROUP );
		$ganres		= array( );		
		if($terms)
		{			
			foreach( $terms as $term)
			{
				$color 		= get_term_meta($term->term_id, "color", true);
				$icon  		= get_term_meta($term->term_id, "icon", true);
				$d 			= wp_get_attachment_image_src($icon, array(100, 100));
				$cur_bgnd 	= $d[0];
				$ganres[]	= array("id"=>$term->term_id, "icon" => $cur_bgnd, "color" => $color, "name" => $term->name);
				//$cardIcons	.= "<div class='card-icon' style='background-image:url($cur_bgnd); background-color:$color' title='" .$term->name  . "'></div>";
				//$ganres[]	= "<strong style='color:$color;'>".$term->name."</strong>";
			}
		}
		$mmbr->ganres	= $ganres;
		$members[] 	= $mmbr;
	}
	$Data = new stdClass;
	$Data->logo 	= $logo;
	$Data->name 	= $display_name;
	$Data->user_id 	= wp_get_current_user()->ID;
	$Data->members 	= $members;
	$Data->page_type = "page";
	$str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
		return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
	}, json_encode($Data));
	return $Data;
	
	$html = "
	<div class='container' id='main'></div>
	<script type='text/babel'>	
		var DATA = JSON.parse('" . json_encode( $Data ) . "');	
		console.log('" . json_encode( $Data ) . "');
		var FmRU_Head = React.createClass({
			render: function() 
			{ 
				return (
					<section>
						<div className='row justify-content-md-center'>	
							<div className='col-md-3'>
								
							</div>
							<div className='col-md-6 text-center'>
								<a href='/' ><img src={this.props.p.logo} className='fmru_logo' /></a>
							</div>
							<div className='col-md-3'>
								<FmRU_LoginForm profileData = {this.props.p} />
							</div>
							
						</div>	
					</section>
				);
			}
		});
		var FmRU_LoginForm = React.createClass({
			render: function() 
			{ 
				var user=this.props.profileData;
				if(user.user_id > 0)
					return <span><a href='/wp-admin/profile.php' title='".__("Edit profile", FRMRU)."'>{user.name}</a> | <a href='" . wp_logout_url( get_permalink() )."' title='" . __("Logout") . "'>" . __("Log out") . "</a></span>;
				else
					return <button className='btn btn-primary' data-toggle='modal' data-target='#loginModal'>".
						__("Log in for experts", FRMRU).
					"</button>;
			}
		});
		var FmRU_MemberBox = React.createClass({
			render: function()
			{
				var stationComponents = this.props.members.map(
					function(dat, i) 
					{
						var cl = dat.cl;
						return (
							<div className='col-md-2 col-sm-3 col-6' key={dat.id}>
								<div className='member' data-class={cl} data-mid={dat.id} data-fmru_type='fmru_player' data-args={dat.id}>
									<img src='" . FMRU_URLPATH . "img/empty.png' className='ssfr'/>
									<div className='member_title'>
										{dat.o}
									</div>
									<div className='xperts ' data-cl={ dat.e==0 ? 'hidden' : ''}>
										{dat.e}
									</div>
								</div>
							</div>
						);
					}					
				);
				return <div className='row'>{stationComponents}</div>;
			}
		});
		var FmRU_App = React.createClass({
			render: function() 
			{ 
				return (
					<div className='container colored'>
						< FmRU_Head p={this.props.p} />
						<section id='members'>
							< FmRU_MemberBox members={this.props.p.members} />								
						</section>
					</div>
				);
			}
		});
		ReactDOM.render( <FmRU_App p={ DATA } />, document.getElementById('main'));
	</script>";	
	return $html;
}
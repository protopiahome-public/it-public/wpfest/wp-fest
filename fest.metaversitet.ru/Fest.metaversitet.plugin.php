<?php
/*
Plugin Name: Fetival.Metaversitet
Plugin URI: http://wp-ermak.ru/
Description: Services for eductional Concurces, Festivals and Exibitions
Version: 0.0.1
Author: Genagl
Author URI: http://wp-ermak.ru/author
License: GPL2
Text Domain:   fmru
Domain Path:   /lang/
*/
/*  Copyright 2018  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 

//библиотека переводов
function init_textdomain_fmru() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("fmru", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_fmru');

//Paths
define('FMRU_URLPATH', WP_PLUGIN_URL.'/fest.metaversitet.ru/');
define('FMRU_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('FRMRU', 'fmru');
define('FRMRU_EXPERT', 'fmru_expert');
define('FRMRU_CRITERY', 'fmru_critery');
define('FRMRU_CATEGORY', 'fmru_category');
define('FRMRU_PLAYER', 'fmru_player');
define('FRMRU_GROUP', 'fmru_group_player');
define('FRMRU_DIARY', 'fmru_diary');
define('BEFORE_START_PHASE', 0);
define('PREPAIRE_PHASE', 1);
define('PRESENTATION_PHASE', 2);
define('AFTER_FINISH_PHASE', 3);

require_once(FMRU_REAL_PATH.'class/FmRU.class.php');
require_once(FMRU_REAL_PATH.'class/FmRu_ajax.class.php');
require_once(FMRU_REAL_PATH.'class/SMC_Post.php');
require_once(FMRU_REAL_PATH.'class/SMC_Object_type.php');
require_once(FMRU_REAL_PATH.'class/FmRU_Expert.class.php');
require_once(FMRU_REAL_PATH.'class/FmRU_Member.class.php');
require_once(FMRU_REAL_PATH.'class/FmRU_Critery.class.php');
require_once(FMRU_REAL_PATH.'class/FmRU_Pagi.class.php');
require_once(FMRU_REAL_PATH.'tpl/auth.php');

$frmu_special_roles = [  "Expert", "Project_author", "Tutor", "Project_member" ];
function get_frmu_special_roles()
{
	return [  "Expert", "Project_author", "Tutor", "Project_member" ];
}
register_activation_hook( __FILE__, array( FmRU, 'activate' ) );
if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array(FmRU, 'deactivate'));
}
$SMC_Object_type = new SMC_Object_type();
FmRU::get_instance();
FmRU_ajax::get_instance();
FmRU_Expert::init();
FmRU_Critery::init();
FmRU_Member::init();
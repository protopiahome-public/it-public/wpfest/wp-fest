<?php

function FmRU404()
{
	ob_start();
	var_dump( $component );
	$var = ob_get_contents();
	ob_clean();
	$html = '
	<div id="main">
		<div class="container text-center colored" id="cont">
			<div class="jumbotron align-items-center">
				<div class="row  justify-content-md-center  justify-content-sm-center">
					<div class="display-1 col-md-12">
						404
					</div>
					<div class="spacer-30" ></div>
					<div class="spacer-30" ></div>
					<div 
						class="btn btn-primary " 
						data-fmru_type="page" 
						data-args="1" 
					>
					' . __("return to home", FRMRU). '
					</div>
					<div class="spacer-30" />
				</div>
			</div>
		</div>
	</div>
	';
	return $html;
}
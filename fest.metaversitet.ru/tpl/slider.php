<?php 
function get_slider_( $slides )
{
	$smc_width = 500;
	$smc_height = 600;
	$html	= '
			<div style="width:100%;height:100%;padding:0px;" id="jssor_parent">
				<div class="__container colored" id="mfcc">
					<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: ' . ($smc_width) . 'px; height: '. $smc_height . 'px; overflow: hidden; max-width:1140px; display:none">'.
						$slides.
					'</div>				
					 <!-- navigator container -->
					<div u="navigator" class="navi_buttons">
							<div class="sm_u_nav">						
							</div>					
							<div u="prototype" hint="exploide" style="POSITION: absolute; WIDTH: 35px; HEIGHT: 35px;" glass="u_eye">
							<div></div>
						</div>
					</div>
					<!-- Navigator Skin End -->	
					<!-- ThumbnailNavigator Skin Begin>
					<div u="thumbnavigator" class="jssort11" style="position: absolute; width: '.$smc_width.'px; height: '. ($smc_height -30 ). 'px; left:-'.$smc_width.'px; top:10px;">
						<div u="slides" style="cursor: pointer;">
							<div u="prototype" class="p" style="position:absolute; width:70px; height:70px; top:0; left:0;">								
								<thumbnailtemplate style=" width:100%;height:100%;border:none;position:absolute;top:0;left:0;">
								</thumbnailtemplate>
							</div>
						</div>
					</div >
					<!-- Thumbnail Item Skin End -->
					<!-- Arrow Left -->
					<span u="arrowleft" class="jssorn08l" style="width: 50px; height: 50px; top: -40px; left: 8px;">
					</span>
					<!-- Arrow Right -->
					<span u="arrowright" class="jssorn08r" style="width: 50px; height: 50px; bottom: -70px; left: 8px">
					</span-->			
				</div>
			</div>'.
			"<!--div id='master_full_screen' class='hint hint--left' data-hint='".__("Full Screen ", "ermak_miph")."'>
				<i class='fa fa-arrows-alt'></i>
			</div>
			<div id='top_panel'>".
				//apply_filters("smc_lp_head_1", "", $loca_id).
			"</div-->";
	return $html;
}		
function get_slide_( $content='', $class='', $style='', $id='', $exec='', $args='')
{
	$html = "<div class='eb_slider_container $class' style='$style' slide_id='$id' eb_exec='$exec', eb_args='$args' slide_title='$title'>
		<!--div u='thumb'>
			<div class='i' style='background-image:url(" . SMC_URLPATH . "/img/key_icon.png)!important;'></div>													
		</div-->	
		<div style='height:100%;'>
			$content					
		</div>
	</div>";
	return $html;
}	
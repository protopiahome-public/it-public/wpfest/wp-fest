<?php
function get_full_expert_list()
{
	global $wpdb;
	$s_delem1	= ";	";
	$l_delem1	= "
";
	$query = "
	SELECT u.display_name as expert, p.post_title as critery, m.post_title as member, raiting, r.description, me.descr
FROM `" . $wpdb->prefix . "frmru_member_raiting` AS r
LEFT JOIN " . $wpdb->prefix . "users AS  u ON u.ID=r.expert_id
LEFT JOIN " . $wpdb->prefix . "posts AS p ON p.ID=r.critery_id
LEFT JOIN " . $wpdb->prefix . "posts AS m ON m.ID=r.member_id
LEFT JOIN " . $wpdb->prefix . "frmru_member_expert AS me ON (me.expert_id = r.expert_id AND me.member_id=r.member_id)
ORDER BY r.expert_id, r.member_id, r.critery_id ASC;";
	$res  = $wpdb->get_results($query);
	$text = [implode($s_delem1, ["EXPERT LOGIN", "PROJECT NAME", "CRITERY", "RITING", "DESCRIPTION FOR RITING", "EXPERT'S MESSAGE TO PROJECT"])];
	foreach($res as $re)
	{
		$t = [];
		$t[] = $old_member != $re->member ? $re->expert : "-";
		$t[] = $old_member != $re->member ? $re->member : "-";
		$t[] = $re->critery;	
		$t[] = $re->raiting;
		$t[] = str_replace($l_delem1, " - ", $re->description);
		//$t[] = str_replace($l_delem1, " - ", $re->descr) ;
		$t[] = $old_descr != str_replace($l_delem1, " - ", $re->descr) ?  str_replace($l_delem1, " - ", $re->descr) : "--";
		$text[] = implode($s_delem1, $t);
		$old_expert = $re->expert;
		$old_critery = $re->critery;
		$old_member = $re->member;
		//$old_description = $re->description;
		$old_descr = str_replace($l_delem1, " - ", $re->descr);
	}
	$csv 	= implode($l_delem1, $text);
	$csv	= iconv(mb_detect_encoding($csv), 'cp1251', $csv);
	//print_r ($csv);
	return $csv;
}
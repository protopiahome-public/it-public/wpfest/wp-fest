<?php
function FmRU_header()
{
	$logo = wp_get_attachment_image_src(FmRU::$options['logotype_0'], "full")[0];
	return '
	<nav class="navbar navbar-expand-lg navbar-xl navbar-dark " style="background-color: #222;">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
		<span class="navbar-brand" href="/" frmru_type="main" frmru_args="">
			<img src="'.$logo.'" />
		</span>
		
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		  <!--li class="nav-item active">
			<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Link</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link disabled" href="#">Disabled</a>
		  </li-->
		</ul>
		
		<div class="pull-right">
			<div class="fmRU_button light xl" title="' . __("Statistics", FRMRU) . '">
				<span class="dashicons dashicons-chart-bar"></span>
			</div>
			<div class="fmRU_button light xl" title="' . __("Filters", FRMRU) . '">
				<span class="dashicons dashicons-filter"></span>
			</div>'.
			FmRU::get_login_form().	
			'
		</div>
		<!--form class="form-inline my-2 my-lg-0">
		  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form-->
	  </div>
	</nav>';
}
<?php
class Member_Raiting_CSV
{
	public $id;
	public $title;
	public $raiting;
	public static $instances;
	public static function get_instance($id)
	{
		if(!static::$instances)
			static::$instances = array();
		if(!static::$instances[$id])
			static::$instances[$id] = new static($id);
		return static::$instances[$id];
	}
	public function __construct($id)
	{
		$this->raiting = 0;
		$this->id = $id;
	}
	public function add_raiting($raiting, $expert_count, $cat_count)
	{
		$this->raiting += $raiting/$expert_count / $cat_count / 2; 
	}
	public function add_critery($id, $raiting, $expert_count, $cat_count)
	{
		$r = Critery_Member_Raiting_CSV::get_instance($id);
		$r->add_member($this->id, $raiting/$expert_count / $cat_count / 2);
	}
	public function add_cat($id, $raiting, $expert_count, $cat_count)
	{
		$r = Category_Member_Raiting_CSV::get_instance($id);
		$r->add_member($this->id, $raiting/$expert_count / $cat_count / 1);
	}
	public function get_critery_raiting($id)
	{
		$r = Critery_Member_Raiting_CSV::get_instance($id);
		return $r->members[$this->id];
	}
	public function get_cat_raiting($id)
	{
		$r = Category_Member_Raiting_CSV::get_instance($id);
		//var_dump($r->members);
		//wp_die();
		return $r->members[$this->id];
	}
}
class Critery_Member_Raiting_CSV
{
	public $members;
	public $critery_id;
	public $title;
	public static $instances;
	public static function get_instance($id)
	{
		if(!static::$instances)
			static::$instances = array();
		if(!static::$instances[$id])
			static::$instances[$id] = new static($id);
		return static::$instances[$id];
	}
	function __construct($id)
	{
		$this->members = array();
		$this->critery_id = $id;
	}
	function add_member($id, $raiting)
	{
		if(!$this->members[$id])
			$this->members[$id] = 0;
		$this->members[$id] += $raiting;
	}
}
class Category_Member_Raiting_CSV
{
	public $members;
	public $term_id;
	public $name;
	static $instances;
	static function get_instance($id)
	{
		if(!static::$instances)
			static::$instances = array();
		if(!static::$instances[$id])
			static::$instances[$id] = new static($id);
		return static::$instances[$id];
	}
	function __construct($id)
	{
		$this->members = array();
		$this->term_id = $id;
	}
	function add_member($id, $raiting)
	{
		if(!$this->members[$id])
			$this->members[$id] = 0;
		$this->members[$id] += $raiting;
	}
}


// @result_type  : csv, table
function get_fmru_members_csv( $result_type = "csv")
{
	$s_delem1	= ";	";
	$l_delem1	= "
";
	$s_delem2	= "</td><td>";
	$l_delem2	= "</tr><tr>";
	
	switch($result_type)
	{
		case "csv":
			$sdelem	= $s_delem1;
			$ldelem	= $l_delem1;
			$d1 = "";
			$d2 = "";
			$h41 = "";
			$h42 = "";
			break;
		case "scv_table":
			$sdelem	= $s_delem2;
			$ldelem	= $l_delem2;
			$d1 = "<div class='vert'><div><span>";
			$d2 = "</span></div></div>";
			$h41 = "<h4>";
			$h42 = "</h4>";
			break;
		case "table":
			break;
	}
	
	//
	$all_cr = FmRU_Critery::get_all();
	$all_cat = FmRU_Critery::get_all_categories();
	//
	global $wpdb;
	$query = "
	SELECT fmr.*,member_id AS mid,  post.post_title AS member, crpost.post_title AS critery, rsh.term_taxonomy_id AS term_id, terms.name AS category, 
		(SELECT COUNT(DISTINCT expert_id) 
         FROM " .$wpdb->prefix . "frmru_member_raiting 
         WHERE " .$wpdb->prefix . "frmru_member_raiting.member_id=mid) AS expert_count
	FROM `" .$wpdb->prefix . "frmru_member_raiting` AS fmr
	LEFT JOIN " .$wpdb->prefix . "posts AS post ON post.ID =  fmr.member_id
	LEFT JOIN " .$wpdb->prefix . "posts AS crpost ON crpost.ID =  fmr.critery_id
	LEFT JOIN " .$wpdb->prefix . "term_relationships AS rsh on rsh.object_id = fmr.critery_id
	LEFT JOIN " .$wpdb->prefix . "terms AS terms on terms.term_id = rsh.term_taxonomy_id 
	ORDER BY mid, critery_id ASC";
	$all = $wpdb->get_results($query);
		
	//echo $query;	
	//wp_die();
		
	$html = "<table class='table table-striped'>";
	$doc = array();
	foreach($all as $mem)
	{
		$id = $mem->member_id;
		$member = Member_Raiting_CSV::get_instance($id);
		$member->title = $mem->member;
		$member->add_raiting($mem->raiting, $mem->expert_count, count($all_cat));
		$member->add_critery($mem->critery_id, $mem->raiting, $mem->expert_count, 1);//count($all_cat));
		$member->add_cat($mem->term_id, $mem->raiting, $mem->expert_count, 1);//count($all_cat));		
	}
	//var_dump(Category_Member_Raiting_CSV::$instances);
	foreach(Member_Raiting_CSV::$instances as $member)
	{
		$memb = FmRU_Member::get_instance($member->id);
		$cats 	= array( $h41.__("Member", FRMRU).$h41, $h41.__("Raiting", FRMRU).$h42 );
		$str	= array() ;
		$str[]	= "";//$member->title;
		$str[]	= $h41.$memb->get_meta("order").$h42;//$member->title;
		$str[]	= $h41.sprintf("%.2f", $member->raiting)."---".$h42;
		$html 	.= "
		<tr scope='row top'>
			<td class='top' style='background-color:rgba(0,0,0,0.75); color:#FFF;'>
				<div class='display-4 critery_cell2'>" . $member->title . "<div>
			</td>
			<td class='top'  style='background-color:rgba(0,0,0,0.75); color:#FFF;'>
				<div class='critery_cell2' ><h3>" . 
					sprintf("%.2f", $member->raiting) . 
				"<h3></div>
			</td>
		</tr>
		";
		foreach($all_cat as $cat)
		{			
			$color  = get_term_meta($cat->term_id, "color", true);
			$cr 	= sprintf("%.2f", $member->get_cat_raiting($cat->term_id));
			$html 	.= "
			<tr scope='row table-sm'>
				<td style='background-color:$color; color:#FFF;'>
					<div class='critery_cell2' ><h6>" . $cat->name . "<h6></div>
				</td>
				<td style='background-color:$color; color:#FFF;'>
					<div class='critery_cell2' ><h6>" .  $cr . "<h6></div>
				</td>
			</tr>";
			$cats[]	= $h41.$cat->name.$h42;
			$str[] = $cr;
			foreach($all_cr as $cr)
			{
				$crit = FmRU_Critery::get_instance($cr);
				if( !in_array($cat, $crit->categories)) continue;
				$crraiting = sprintf("%.2f", $member->get_critery_raiting($crit->id));
				$html .= "
				<tr scope='row table-sm'>
					<td> 
						<div class='row'>
							<div class='col-md-11 offset-md-1'>" . 
								$crit->get("post_title") . 
							"</div>
						</div>		
					</td>
					<td><div class='padding_left_30'>" . $crraiting . "</div></td>
				</tr>
				";
				$cats[] = " -- " . $crit->get("post_title") ;
				$str[] = $crraiting;
			}
		}
		$doc[]	= implode($sdelem, $str);
	}
	$html .= "</table>";
	$txt = 
		$d1 .  " "  . $sdelem . $d1 .
		implode( $d2 . $sdelem . $d1 , $cats) . 
		$d2 .
		$ldelem . 
		implode($ldelem, $doc);
	
	//file_put_contents( FMRU_REAL_PATH . "results/results.csv" , $txt);
	switch($result_type)
	{
		case "csv":
			$txt = iconv(mb_detect_encoding($txt), 'cp1251', $txt);
			return 
			//"<textarea rows=90 style='width:100%'>". 
			$txt 
			//. "</textarea>"
			;
		case "scv_table":
			return "
			<table class='table '><tr><td>".
				$d1 . 
					implode( $d2 . $sdelem . $d1 , $cats) . 
				$d2 .
				$ldelem .
				implode($ldelem, $doc);
			"</td></tr></table>";
		case "table":
			return $html;
	}
	return ""; 
}

function get_experts_member( $result_type="csv")
{
	global $wpdb;
	$s_delem1	= ";	";
	$l_delem1	= "
";
	$query = "SELECT DISTINCT frm.expert_id AS eid, u.display_name, 
GROUP_CONCAT( DISTINCT member_id ) AS member
FROM ".$wpdb->prefix."frmru_member_raiting AS frm
LEFT JOIN ".$wpdb->prefix."users AS u ON u.ID=frm.expert_id
GROUP BY expert_id;";
	$res = $wpdb->get_results($query);
	$html = "<table  class='table table-hover table-striped'>
		<thead class='thead-dark'>
		<tr>
			<th scope='col'>" . __("Name") . "</th>
			<th scope='col'>" . __("Count", FRMRU) . "</th>
			<th scope='col'>" . __("Members", FRMRU) . "</th>
		</tr>
		</thead>
		<tbody>";
	$thead = array(__("Name"), __("Count", FRMRU), __("Members", FRMRU));
	$doc = array();
	foreach($res AS $r)
	{
		$member_ids = explode(",", $r->member);
		$html .= "
		<tr>
			<th scope='row'>".
				$r->display_name . 
			"</th>
			<td>".
				count($member_ids ) .
			"</td>
			<td>";
		$cul = array($r->display_name, count($member_ids ));
		$m = array();
		foreach($member_ids AS $mid)
		{
			$member = FmRU_Member::get_instance($mid);
			$html .= "<span class='member_span'>" . $member->get_meta("order") ."</span>";
			$m[] = $member->get_meta("order");
		}
		$cul[] = implode(", ", $m);
		$html .= "
			</td>
		</tr>";
		$doc[] = implode( $s_delem1, $cul);
	}

	$html .= "
		</tbody>
	</table>";
	switch($result_type)
	{
		case "csv":
			$txt = implode( $s_delem1, $thead ) . $l_delem1 . implode( $l_delem1, $doc );
			$txt = iconv(mb_detect_encoding($txt), 'cp1251', $txt);
			return $txt;
		case "table":
			return $html;
	}
	return $html;
	/**/
}
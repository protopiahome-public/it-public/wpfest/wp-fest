<?php
function commands_create( $count = 0, $ganres=-1 )
{
	if(!is_array($ganres))	$ganres = array();
	$dmds = array();
	for($i = 0; $i < $count; $i++)
	{
		$n = 0;
		unset($exists);
		$all = FmRU_Member::get_all();
		foreach( $all as $member_data)
		{
			$n++;
			if($member_data->post_title != "Команда " . ($n+1)) 
				//break;
				$exists = $n;
		}
		//if(!$exists) continue;
		$new = FmRU_Member::insert(
			array(
				'post_name'    	=> "Команда " . ($n + 1),
				'post_title'    => "Команда " . ($n + 1),
				"order"			=> FmRU::$options['member_id'],
				'post_content'  => "",
			));		
		wp_set_object_terms( $new->id, $ganres, FRMRU_GROUP );
		$dmds[] = $new;
		FmRU::$options['member_id']++;
	}
	update_option(FRMRU, FmRU::$options);
	return $dmds;
}
function experts_create( $count = 0 )
{
	require_once ABSPATH . WPINC . '/user.php';
	$contents = "EXPERTS \n";
	for($i = 0; $i < $count; $i++)
	{
		$upas 	= substr(MD5(rand(1,100)), 0, 4);
		$n 		= 0;
		while( username_exists( 'expert'.($n + 1) ) )
		{
			$n++;
		};
		
		$userdata = array(
			'user_pass'       => $upas,
			'user_login'      => 'expert'.($n + 1),
			'user_email'      => '',
			'display_name'    => 'expert'.($n + 1),
			'role'            => 'Expert', 
		);
		$userId = wp_insert_user( $userdata );
		if(!is_wp_error($userId))
		{			
			$contents .= ($i + 1) . ". Login: expert".($n + 1).", Password: " .  $upas ."  \n";
		}
	}
	$file = FMRU_REAL_PATH . 'experts.txt';
	file_put_contents($file, $contents);
}
function default_creating()
{	
	global $wpdb;
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );
	FmRU_Critery::init();
	FmRU_Member::init();
	
	$cnt 	= FmRU_Member::get_all();
	$crt	= FmRU_Critery::get_all();
	$query 	= "SELECT term_id FROM ".$wpdb->prefix."term_taxonomy WHERE taxonomy='".FRMRU_GROUP."'";
	$ganres = $wpdb->get_var($query);
	$query 	= "SELECT term_id FROM ".$wpdb->prefix."term_taxonomy WHERE taxonomy='".FRMRU_CATEGORY."'";
	$cats = $wpdb->get_var($query);
		
	if(!(int)$ganres)
	{
		$gnr1 	= wp_insert_term( "test ganre", FRMRU_GROUP, array(
			'description' => '',
			'parent'      => 0,
			'slug'        => 'test',
		) );
		$media_id = media_sideload_image( FMRU_URLPATH . "img/writing.png", 0, "", "id");
		if(!is_wp_error($media_id))
			update_term_meta($gnr1['term_id'], "icon",  $media_id);
			update_term_meta($gnr1['term_id'], "color", "#6c757d");
		
		
		$gnr 	= get_term_by( "term_id", $gnr1['term_id'], FRMRU_GROUP);
	}
	else 
		$gnr 	= get_term_by( "term_id", (int)$ganres, FRMRU_GROUP);
	
	if(!(int)$cats)
	{
		$cat1 	= wp_insert_term( "test category", FRMRU_CATEGORY, array(
			'description' => '',
			'parent'      => 0,
			'slug'        => 'test',
		) );
		$cat	= get_term_by( "term_id", $cat1['term_id'], FRMRU_CATEGORY);
		update_term_meta($cat1['term_id'], "color", "#6c757d");
	}
	else
		$cat	= get_term_by( "term_id", (int)$cats, FRMRU_GROUP);
	
	if(!count($cnt))
	{
		$cmds = commands_create(1);	
		wp_set_object_terms( $cmds[0]->id, $gnr->term_id, FRMRU_GROUP );
		wp_set_object_terms( $cmds[0]->id, $cat->term_id, FRMRU_CATEGORY );
	}
	if(!count($crt))
	{
		$crit = FmRU_Critery::insert([
			"post_name"		=> "test critery",
			"post_title"	=> "test critery",
			"post_content"	=> "",
		]);
		wp_set_object_terms( $crit->id, $gnr->term_id, FRMRU_GROUP );
		wp_set_object_terms( $crit->id, $cat->term_id, FRMRU_CATEGORY );
	}
	if(!FmRU::$options['logotype_url'])
	{
		$media_id = media_sideload_image( FMRU_URLPATH . "img/FestLogo.png", 0, "", "id");
		if(!is_wp_error($media_id))
			FmRU::$options['logotype_url'] = $media_id;	
	}
	if(!FmRU::$options['default_member_thrumb'])
	{
		$media_id = media_sideload_image( FMRU_URLPATH . "img/pg_bg2.png", 0, "", "id");
		if(!is_wp_error($media_id))
			FmRU::$options['default_member_thrumb'] = $media_id;
	}	
	update_option(FRMRU, FmRU::$options);
}
<?php
function get_member_data($post)
{
	global $wpdb;
	$member 		= FmRU_Member::get_instance($post);
	$is_expert 		= FmRU_Expert::is_expert();
	$experts 		= $member->get_own_experts();
	$count_juri 	= count($experts) > 0 ? count($experts) : 1;
	$rating_data 	= $member->get_raiting_data();
	$rait	 		= (float)FmRU_Critery::count_offer_raiting($member->get_member_raiting(), $count_juri * 3);	
	$r2				= $rait > 8 ? 8 : round($rait);
	$cats 			= FmRU_Critery::get_all_categories();
	$categories		= array();
	foreach( $cats as $cat)
	{
		$crat = 0;
		$get_all_by_cat = FmRU_Critery::get_all_by_cat($cat->term_id);
		$clr	 = get_term_meta( $cat->term_id, 'color', true ); 
		$category 			= new StdClass;
		$category->id 		= $cat->term_id;
		$category->name		= $cat->name;
		$category->color	= $clr;
		$category->criteries = array();
		if($get_all_by_cat)
		{
			foreach($get_all_by_cat as $cr)
			{
				$cr_rating 	= (float)$member->get_raiting_critery( $rating_data, $cr->ID, $is_expert);
				$rt 		= (float)FmRU_Critery::count_offer_raiting( $cr_rating, $is_expert ? 1 : $count_juri );
				$crat 		+= $rt;
				$ctirery	= new StdClass;
				$ctirery->is 	 = $cr->ID;
				$ctirery->title  = $cr->post_title;
				$ctirery->rating = sprintf( "%.2f", $rt );
				$category->criteries[] = $ctirery;
			}
		}
		$category->rating 	= sprintf( "%.2f", $crat);
		$categories[] 		= $category;
	}
	
	$DATA = new StdClass;
	$DATA->member		= array("id"=>$member->id, "title"=> $member->get("post_title" ));
	$DATA->is_expert	= $is_expert;
	$DATA->experts		= $experts;
	$DATA->count_juri	= $count_juri;
	$DATA->rait			= sprintf( "%.2f", $rait);
	$DATA->r2			= $r2;
	$DATA->categories	= $categories;
	//wp_die(json_encode($DATA));
	/**/
	return $str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
		return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
	}, json_encode($DATA));
}
function get_member_screen($post)
{
	global $wpdb;
	$member = FmRU_Member::get_instance($post);
	$is_expert = FmRU_Expert::is_expert();
	$experts = $member->get_own_experts();
	$juri = array();
	if(count($experts))	
	{		
		foreach( $experts as $expert)
		{
			$juri[] =  $expert->display_name . " ";
		}
		$ex = "<span>" . implode(" </span>,<span> ", $juri) ."</span>";
	}
	else
		$ex = "<span>" . __("Nothing", FRMRU) ."</span>";
	$count_juri = count($experts) > 0 ? count($experts) : 1;
	//
	$rating_data 	= $member->get_raiting_data();
	$rait	 		= (float)FmRU_Critery::count_offer_raiting($member->get_member_raiting(), $count_juri * 3);	
	$r2				= $rait > 8 ? 8 : round($rait);
	//
	//$experts = FmRU_Member::get_experts();
	$cats = FmRU_Critery::get_all_categories();
	$i = 0;
	foreach( $cats as $cat)
	{
		$get_all_by_cat = FmRU_Critery::get_all_by_cat($cat->term_id);
		$clr	 = get_term_meta( $cat->term_id, 'color', true ); 
		$color = $i%2 == 0 ? " grey " : " darkgrey";
		$rows .= "
		<div class='col-md-7 col-sm-7 critery_cell first' style='background-color:$clr'>
			<h4>" .
				$cat->name .
			"</h4>
		</div>
		<div class='col-md-5 col-sm-5 critery_cell first' style='background-color:$clr'>
			<h4 cat_id='$cat->term_id'>" .
				(float)FmRU_Critery::count_offer_raiting( $member->get_member_cat_raiting($cat->term_id), $count_juri ).
			"</h4>
		</div>";
		if(!$is_expert)
		{			
			$rows .= FmRU_Member::get_accordeon_start_form("_" . $cat->term_id, "background-color:$clr;");
		}
		$crat = 0;
		if($get_all_by_cat)
		{
			foreach($get_all_by_cat as $cr)
			{
				$idd 	= $cat->term_id ."_" .$cr->ID;
				$cr_rating 	= (float)$member->get_raiting_critery( $rating_data, $cr->ID, $is_expert);
				$rt 		= (float)FmRU_Critery::count_offer_raiting( $cr_rating, $is_expert ? 1 : $count_juri );
				$crat 		+= $rt;
				$raiting_ = FmRU_Member::get_raiting_block( $rt, $cat, $cr);
				$rows .= "
				<div class='col-md-12 equal' style='padding:0;'>
					<div class='col-md-7 col-sm-7 $color critery_cell ' >
						<div class='footer-widget'>" .
							$cr->post_title .
						"</div>
					</div>
					<div class='col-md-5 col-sm-5  $color critery_cell ' old_raiting='$rt'  critery_id='$cr->ID'>
						<div class='footer-widget'>" .
							$raiting_. 
						"</div>
					</div>
				</div>";
			}
		}
		else
		{
			$rows .= "
			<div class='col-md-12 critery_cell ' style='background-color:$clr'>".
				__("No one present", FRMRU).
			"</div>";
		}
		//commentaries
		if(!$is_expert)
		{
			$descrs = FmRU_Critery::get_descriptions_category($cat->term_id, $member->id);
			if(count($descrs))
			{
				$rows .= "
					<div class='col-md-12 critery_cell2 $color first'>".
						__("Expert's commentaries:", FRMRU).
					"</div>
					<div class='critery_cell col-md-12 $color'>";
				foreach($descrs as $descr)
				{
					$expertt = FmRU_Expert::get_instance($descr->expert_id)->user->display_name;
					$rows .= "
					<div class='col-md-12'>
						<blockquote>
							$descr->description
							<footer>$expertt</footer>
						</blockquote>
					</div>";
				}
				$rows .= "</div>";
			}
		}
		$rows .= $is_expert ? "" : FmRU_Member::get_accordeon_finish_form()."
		<script>
			//jQuery('[cat_id=".$cat->term_id."]').text( '$crat' );
		</script>";		
		$i++;
	}
	
	$new_expert_c_form = $is_expert ? 
		FmRU_Critery::get_expert_new_cr_form( $member->id ) : 
		"";
	//
	$uns = FmRU_Critery::get_all_uniq($member->id);
	if(count($uns))
	{
		foreach($uns as $un)
		{
			$critery = FmRU_Critery::get_instance($un);
			$cr_rating 	= (float)$member->get_raiting_critery( $rating_data, $critery->id, $is_expert);
			$rt 		= (float)FmRU_Critery::count_offer_raiting( $cr_rating, $is_expert ? 1 : $count_juri );
			$crat 		+= $rt;
			$cat = wp_get_post_terms( $critery->id, FRMRU_CATEGORY )[0];				
			$color = get_term_meta($cat->term_id, "color", true);
			$_uniques .= "
				<div class='col-md-7 col-sm-7 critery_cell first' style='background-color:$color;'>" .
					$critery->body->post_title.
				"</div>
				<div class='col-md-5 col-sm-5 critery_cell first' style='background-color:$color;' old_raiting='$rt' critery_id='$critery->id'>" .
					FmRU_Member::get_raiting_block( $rt, $cat, $critery->body) .
				"</div>";
		}
	}
	else
	{
		$_uniques .= "
			<div class='col-md-1'></div>
			<div class='col-md-10'>
				<div class='small'>" .__("No exist yet.", FRMRU). "</div>
			</div>";
	}
	
	//expert's descriptios
	$dess = $member->get_expert_descs();
	$descriptions = "";
	if(count($dess))
	{
		foreach($dess as $des)
		{
			$expert = FmRU_Expert::get_instance($des->expert_id);
			$descriptions .= "			
			<div class='col-md-12 grey critery_cell3'>
				<blockquote>
					$des->descr
					<footer>".$expert->user->display_name."</footer>
				</blockquote>
			</div>";
		}
	}
	else
	{
		$descriptions .=  "
		<div class='col-md-1'>
		</div>
		<div class='col-md-11 small'>".
			__("Nobody no comment this.", FRMRU).
		"</div>";
	}
	$meExpert = FmRU_Expert::get_instance(get_current_user_id());
	$exp_comment_form = $is_expert ? $member->nn() : "";
	
	//
	$terms		= get_the_terms( $member->id, FRMRU_GROUP );
	foreach( $terms as $term)
	{
		$color 		= get_term_meta($term->term_id, "color", true);
		$icon  		= get_term_meta($term->term_id, "icon", true);
		$d 			= wp_get_attachment_image_src($icon, array(100, 100));
		$cur_bgnd 	= $d[0];
		$cardIcons	.= "<div class='card-icon' style='background-image:url($cur_bgnd); background-color:$color' title='" .$term->name  . "'></div>";
		$gs			.= " <strong>" . $term->name . "</strong> ";
	}
	//
	$logo = wp_get_attachment_image_src(FmRU::$options['logotype_0'], "full")[0];	
	global $post, $wpsociallikes;
	$post = $member->body;
	$likes = isset($wpsociallikes) ? $wpsociallikes->build_buttons( $post ) . "<p>" : "";
	require_once(FMRU_REAL_PATH."tpl/header.php");
	$html =."
	<div class='container' member_id='$member->id'>".
		FmRU_header() . 
		"<!--section id='head'>
			<div class='row'>				 
				<div class='col-md-3 col-sm-12  col-centered' frmru_type='main' frmru_args=''>
					<img src='$logo' style='margin:0;'/>						
				</div>
				<div class='col-md-4 col-sm-12 display-4 text-center'>". 
					$member->body->post_title.
				"</div>	
				<div class='col-md-5 col-sm-12 login_form'>". 
					FmRU::get_login_form().
				"</div>	
			</div>
		</section>	
		<section id='raiting'>
			<div class='row raiting'>	
				<div class='col-md-3'>".
					__("Valuations:", FRMRU). 
					"<span id='_valuations'>".
						$rait . 
					"</span>
				</div>	
				<div class='col-md-3'>".
					__("Raiting:", FRMRU). 
					"<span id='_raiting'>".
						$r2 . 
					"</span>
				</div>	
				<div class='col-md-6'>".
					__("Experts, who put raiting:", FRMRU). " $ex 
				</div>						
			</div>
		</section-->
		
		<section id='card'>
			<div class='row'>
				<div class='col-lg-12'>
					<div class='' >					  
						<div class='row'>
							<div class='col-lg-5 col-md-6 col-sm-12'>
								<div class='card-trumb' style='background-image:url(".$member->get_thrumbnail().");'>
									
								</div>
								<div class='critery_cell2'>
									<div class='display-4'>
										".$member->get("post_title")."
									</div>
								</div>
							</div>
							<div class='col-lg-7 col-md-6 col-sm-12'>
								<ul class='list-group list-group-flush'>
									<li class='list-group-item'>
										<div class='row critery_cell2'>
											<div class='col-md-7 col-sm-7  '>".
												__('Ganres:', FRMRU) . "<p>" . $gs .
											"</div>
											<div class='col-md-5 col-sm-5  lead '>
												$cardIcons
											</div>
										</div>
									</li>
									<li class='list-group-item'>
										<div class='row critery_cell2'>
											<div class='col-md-7 col-sm-7  '>".
												__('Valuations:', FRMRU) .
											"</div>
											<div class='col-md-5 col-sm-5  lead '>
												<strong>$rait</strong>
											</div>
										</div>
									</li>
									<li class='list-group-item'>
										<div class='row critery_cell2'>
											<div class='col-md-7 col-sm-7'>".
												__('Raiting:', FRMRU).
											"</div>
											<div class='col-md-5 col-sm-5  lead '>
												<strong>$r2</strong>
											</div>
										</div>
									</li>
									<li class='list-group-item'>
										<div class='row critery_cell2'>
											<div class='col-md-12  '>".
												__('Experts, who put raiting:', FRMRU) .
											"</div>
											<div class='col-md-12  '>
												<strong>$ex</strong>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class='row'>						
							<div class='col-md-12 post_content'>" . 
								$likes .
								apply_filters("the_content", $member->get("post_content")) . 
							"</div>
						</div>
					</div>
					<div class='spacer-30'></div>
				</div>
			</div>
		</section>
		
		
		
		<section id='bcriteries' class='full_section-'>
			<div class='row'>
				<div class='col-md-1'></div>
				<div class='col-md-10'>
					<div class='display-4'>" .__("Basic Criteries", FRMRU). "</div>
				</div>
				<div class='col-md-1'></div>
			</div>
			<div class='spacer-10'></div>				
			<div class='row'>
				$rows
			</div>
		</section>
		<section id='ucriteries' class='full_section-'>
			<div class='row'>
				<div class='col-md-1'></div>
				<div class='col-md-10'>
					<div class='display-4'>" .__("Unique Criteries from Experts", FRMRU). "</div>
				</div>
				<div class='col-md-1'></div>
			</div>
			<div class='spacer-10'></div>
			<div class='row'>
				$_uniques
				$new_expert_c_form
			</div>
		</section>
		<section id='experts_descr' class='full_section-'>
			<div class='row'>
				<div class='col-md-1'></div>
				<div class='col-md-10'>
					<div class='display-4'>" .__("Expert's descriptions", FRMRU). "</div>
				</div>
				<div class='col-md-1'></div>
			</div>
			<div class='spacer-10'></div>
			<div class='row first_row'>
				$descriptions
			</div>
		</section>
		<div id='my_comment'>
			$exp_comment_form
		</div>
	</div>";
	
	return $html;
}
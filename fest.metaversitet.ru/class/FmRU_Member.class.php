<?php

class FmRU_Member extends SMC_Post
{
	static function init()
	{	
		add_action('init',					array(__CLASS__, 'add_class'), 14 );	
		add_action('delete_user', 			array(__CLASS__, 'delete_user'));
		add_action('before_delete_post ', 	array(__CLASS__, 'before_delete_post ')); 
		//add_action('admin_menu',			array(__CLASS__, 'my_extra_fields2'));
		
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 12);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 11);
		add_filter("manage_edit-".FRMRU_GROUP."_columns", 	array( __CLASS__,'ctg_columns')); 
		add_filter("manage_".FRMRU_GROUP."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		add_action( FRMRU_GROUP.'_edit_form_fields', 		array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_'.FRMRU_GROUP, 					array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_'.FRMRU_GROUP, 					array( __CLASS__, 'save_ctg'), 10);	
		
		//add_filter("manage_edit-post_columns", 				array(__CLASS__, 'add_views_post_column'), 4);
		//add_filter("manage_edit-post_sortable_columns", 	array(__CLASS__, 'add_views_sortable_post_column'));
		//add_filter("manage_posts_custom_column", 			array(__CLASS__, 'fill_views_post_column'), 5, 2);
		parent::init();
		return;	
		
		add_action( 'rest_api_init', 		array(__CLASS__, "rest_api_init"));			
		
		
		
	}
	
	
	static function my_extra_fields2() 
	{
		add_meta_box( 'extra_fields2', __('Parameters', FRMRU), array(__CLASS__, 'extra_fields_box_func2'), static::get_type(), 'normal', 'high'  );
		
	}
	static function extra_fields_box_func2( $post )
	{	
		$lt					= static::get_instance( $post );
		//echo static::get_type();
		echo static::view_admin_edit($lt);			
	}
	
	
	
	
	
	
	
	static function get_type()
	{
		return FRMRU_PLAYER;
	}
	static function add_class()
	{
		$labels = array(
			'name' => __('Member', FRMRU),
			'singular_name' => __("Member", FRMRU),
			'add_new' => __("add Member", FRMRU),
			'add_new_item' => __("add Member", FRMRU),
			'edit_item' => __("edit Member", FRMRU),
			'new_item' => __("add Member", FRMRU),
			'all_items' => __("all Members", FRMRU),
			'view_item' => __("view Member", FRMRU),
			'search_items' => __("search Member", FRMRU),
			'not_found' =>  __("Member not found", FRMRU),
			'not_found_in_trash' => __("no found Member in trash", FRMRU),
			'menu_name' => __("Member", FRMRU)
		);
		$args = array(
			 'labels' => $labels
			,'public' => true
			,'show_ui' => true // показывать интерфейс в админке
			,'has_archive' => true 
			,'exclude_from_search' => true
			,'menu_position' => 0.11 // порядок в меню
			,'show_in_menu' => "rrmru_page"
			,'show_in_rest' => true
			,'supports' => array(  'title', "editor", "thumbnail")
			,'capability_type' => 'page'
		);
		register_post_type(FRMRU_PLAYER, $args);
		register_meta(FRMRU_PLAYER, "order", array(
			'type'              => 'integer',
			'description'       => __("Order"),
			'single'            => true,
			'sanitize_callback' => null,
			'auth_callback'     => null,
			'show_in_rest'      => true,
		));
	}
	static function rest_api_init()
	{
		register_rest_field( FRMRU_PLAYER, 'img', array(
			'get_callback' => function( $player ) 
			{
				$member = static::get_instance($player['id']);
				return $member->get_thrumbnail();
			},
			'update_callback' => function( $karma, $comment_obj ) 
			{				
				return true;
			},
			'schema' => array(
				'description' => __( 'Post thrumbnail URL' ),
				'type'        => 'integer'
			),
		) );
	}
	/*
	
	
	*/
	
	static function create_taxonomy()
	{
		register_taxonomy(
			FRMRU_GROUP, 
			array( static::get_type(), "post", FmRU_Critery::get_type() ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Ganre", FRMRU),
					'singular_name'     => __("Ganre", FRMRU),
					'search_items'      => __('Search Ganre', FRMRU),
					'all_items'         => __('All Ganres', FRMRU),
					'view_item '        => __('View Ganre', FRMRU),
					'parent_item'       => __('Parent Ganre', FRMRU),
					'parent_item_colon' => __('Parent Ganre:', FRMRU),
					'edit_item'         => __('Edit Ganre', FRMRU),
					'update_item'       => __('Update Ganre', FRMRU),
					'add_new_item'      => __('Add New Ganre', FRMRU),
					'new_item_name'     => __('New Ganre Name', FRMRU),
					'menu_name'         => __('Ganre', FRMRU),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		register_taxonomy(
			FRMRU_DIARY, 
			array("post"), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Member Diary", FRMRU),
					'singular_name'     => __("Member Diary", FRMRU),
					'search_items'      => __('Search Member Diary', FRMRU),
					'all_items'         => __('All Member Diaries', FRMRU),
					'view_item '        => __('View Member Diary', FRMRU),
					'parent_item'       => __('Parent Member Diary', FRMRU),
					'parent_item_colon' => __('Parent Member Diary:', FRMRU),
					'edit_item'         => __('Edit Member Diary', FRMRU),
					'update_item'       => __('Update Member Diary', FRMRU),
					'add_new_item'      => __('Add New Member Diary', FRMRU),
					'new_item_name'     => __('New Member Diary Name', FRMRU),
					'menu_name'         => __('Member Diary', FRMRU),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == FRMRU_GROUP )
			$parent_file = 'rrmru_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'rrmru_page', 
			__("Ganres", FRMRU), 
			__("Ganres", FRMRU), 
			'manage_options', 
			'edit-tags.php?taxonomy='.FRMRU_GROUP
		);
    }
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			'id' 			=> 'id',
			'name' 			=> __('Name'),
			'icon' 			=> __('Icon'),
			'color'			=> __("Color", FRMRU),
			'order'			=> __("Order", FRMRU),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'order': 
				$order = get_term_meta( $term_id, 'order', true ); 
				$out 		.= $order;
				break;	
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	  
			case "icon":
				$icon = get_term_meta( $term_id, 'icon', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />";
				break;
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(FMRU_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id = $term->term_id;
			$color = get_term_meta($term_id, "color", true);
			$order = get_term_meta($term_id, "order", true);
			$icon  = get_term_meta($term_id, "icon", true);
			$icon  = is_wp_error($icon) ? "" :  $icon;
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="color">
					<?php echo __("Color", FRMRU);  ?>
				</label> 
			</th>
			<td>
				<div class="bfh-colorpicker" data-name="color" data-color="<?php echo $color ?>">
				</div>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="icon">
					<?php echo __("Icon", FRMRU);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $icon, "group_icon", 0 );
				?>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "color", $_POST['color']);
		update_term_meta($term_id, "order", $_POST['order']);
		update_term_meta($term_id, "icon",  $_POST['group_icon0']);
	}
	
	static function add_views_post_column( $columns )
	{
		$columns['diary'] 		= __("Diary", FRMRU);
		return $columns;
	}
	static function add_views_sortable_post_column($post)
	{
	
	}
	static function fill_views_post_column( $column, $post_id )
	{
		if($column == 'diary')
		{
			$diaries = get_the_terms($post_id, FRMRU_DIARY);
			if(is_wp_error($diaries) || !$diaries || !count($diaries))	return "--";
			foreach($diaries as $d)
			{	
				$p = get_post( (int)$d->name );
				echo "<a href='/wp-admin/post.php?post=" . $p->ID . "&action=edit'>" . $p->post_title . "</a>";
				//$diary = get_term_by("name", (string)$p->ID, FRMRU_DIARY);
				//if( $d ) wp_delete_term( $d->term_id, FRMRU_DIARY );
			}
		}
		return; 
	}
	
	static function view_admin_edit($obj)
	{
		$order		= $obj->get_meta("order");
		$fb_url		= $obj->get_meta("fb_url");
		$vk_url		= $obj->get_meta("vk_url");
		$instagramm_url= $obj->get_meta("instagramm_url");
		$youtube_url= $obj->get_meta("youtube_url");
		$valuations	= static::validate( $obj->get_raiting_data());	
		$tutor_id 	= $obj->get_meta("tutor_id");
		$leader_id 	= $obj->get_meta("leader_id");
		$member0 	= $obj->get_meta("member_0");
		$member1 	= $obj->get_meta("member_1");
		$member2 	= $obj->get_meta("member_2");
		$member3 	= $obj->get_meta("member_3");
		$member4 	= $obj->get_meta("member_4");
		$member5 	= $obj->get_meta("member_5");
		$member6 	= $obj->get_meta("member_6");
		$member7 	= $obj->get_meta("member_7");
		$member8 	= $obj->get_meta("member_8");
		$member9 	= $obj->get_meta("member_9");
		$member_sels = "";
		for($i=0; $i<10; $i++)
		{
			$member_sels .= 
			wp_dropdown_users( array(
				'show_option_none'        => '--',
				'hide_if_only_one_author' => '',
				'orderby'                 => 'display_name',
				'order'                   => 'ASC',
				"class"					  => "form-control",
				'multi'                   => false,
				'show'                    => 'display_name',
				'echo'                    => false,
				'selected'                => $obj->get_meta("member_$i"),
				'name'                    => "member_$i",
				'id'                      => "member_$i",
				"role"					  => "Project_member"
			) ) . "<div class='spacer-10'></div>";
		}
		$data 		= "";
		foreach($valuations as $user_id=>$udata)
		{
			$data	.= "
				<div class='spacer-10'>
				</div>
				<div class='row'>
					<div class='col-md-12 lead'>".
						get_user_by("id", $user_id)->display_name.
					"</div>
					<div class='spacer-10'>
					</div>";
			$rat = array();
			foreach($udata as $valuation=>$val)
			{
				$valu = FmRU_Critery::get_instance((int)$valuation);
				$data	.= "
					<div class='col-md-12'>
						<div class='col-md-2'>				
							<select class='form-control' cid='$valuation' eid='$user_id'>
								<option value='0' " . selected(0, $val, 0) . ">0</option>
								<option value='1' " . selected(1, $val, 0) . ">1</option>
								<option value='2' " . selected(2, $val, 0) . ">2</option>
								<option value='3' " . selected(3, $val, 0) . ">3</option>
							</select>
						</div>
						<div class='col-md-10 text-left'>".
							$valu->body->post_title.
						"</div>
					</div>
					<div class='spacer-10'></div>";
				$rat[] = $valuation . "." . $val;
			}
			$data	.= "
				</div>
				<div class='spacer-10'></div>
				<div >	
					<input type='hidden' name='valuations[]' exid='$user_id' value='" . $user_id.",".implode( ",", $rat) . "' />
				</div>
				<div class='spacer-10'></div>
				<hr/>";
		}
		$raiting= $obj->get_meta("raiting");
		$html	= "
		<section>
			<div class='container'>
				<ul class='list-group'>
					<li class='list-group-item'>
						<div class='col-md-4'>
							<label class='display-4'>" . __("Order", FRMRU) . "</label>
						</div>
						
						<div class='col-md-8'>
							<input type='number' value='$order' name='order'  class='form-control' />
						</div>
					</li>			
					<li class='list-group-item'>
						<div class='col-md-4'>
							<label class='display-4'>" . __("Facebook", FRMRU) . "</label>
						</div>
						
						<div class='col-md-8'>
							<input type='' value='$fb_url' name='fb_url'  class='form-control' />
						</div>
					</li>			
					<li class='list-group-item'>
						<div class='col-md-4'>
							<label class='display-4'>" . __("Vkontkte", FRMRU) . "</label>
						</div>
						
						<div class='col-md-8'>
							<input type='' value='$vk_url' name='vk_url'  class='form-control' />
						</div>
					</li>			
					<li class='list-group-item'>
						<div class='col-md-4'>
							<label class='display-4'>" . __("Instagramm", FRMRU) . "</label>
						</div>
						
						<div class='col-md-8'>
							<input type='' value='$instagramm_url' name='instagramm_url'  class='form-control' />
						</div>
					</li>				
					<li class='list-group-item'>
						<div class='col-md-4'>
							<label class='display-4'>" . __("Youtube", FRMRU) . "</label>
						</div>
						
						<div class='col-md-8'>
							<input type='' value='$youtube_url' name='youtube_url'  class='form-control' />
						</div>
					</li>			
					<li class='list-group-item'>
						
						<div class='col-md-4'>				
							<label class='display-4'>" . __("Tutor", FRMRU) . "</label>
						</div>
						<div class='col-md-8'>".
							wp_dropdown_users( array(
								'show_option_none'        => '--',
								'hide_if_only_one_author' => '',
								'orderby'                 => 'display_name',
								'order'                   => 'ASC',
								"class"					  => "form-control",
								'multi'                   => false,
								'show'                    => 'display_name',
								'echo'                    => false,
								'selected'                => $tutor_id,
								'name'                    => 'tutor_id',
								'id'                      => 'tutor_id',
								"role"					  => "Tutor"
							) )	.
						"</div>
					</li>					
					<li class='list-group-item'>
						
						<div class='col-md-4'>				
							<label class='display-4'>" . __("Project author", FRMRU) . "</label>
						</div>
						<div class='col-md-8'>".
							wp_dropdown_users( array(
								'show_option_none'        => '--',
								'hide_if_only_one_author' => '',
								'orderby'                 => 'display_name',
								'order'                   => 'ASC',
								"class"					  => "form-control",
								'multi'                   => false,
								'show'                    => 'display_name',
								'echo'                    => false,
								'selected'                => $leader_id,
								'name'                    => 'leader_id',
								'id'                      => 'leader_id',
								"role"					  => "Project_author"
							) )	.
						"</div>
					</li>						
					<li class='list-group-item'>
						
						<div class='col-md-4'>				
							<label class='display-4'>" . __("Project members", FRMRU) . "</label>
						</div>
						<div class='col-md-8'>".
							$member_sels	.
						"</div>
					</li>			
					<li class='list-group-item'>
						
						<div class='col-md-4'>				
							<label class='display-4'>" . __("Raiting", FRMRU) . "</label>
						</div>
						<div class='col-md-8'>
							<input type='number' value='$raiting' name='raiting'  disabled='disabled' class='form-control' />
						</div>
					</li>			
					<li class='list-group-item' style='vertical-align:top;'>
						<div class='col-md-4'>				
							<label class='display-4'>" . __("Valuations", FRMRU) . "</label>
						</div>
						<div class='col-md-8'>
							<div class=''>
								<input type=checkbox name=update_valuatons value='1'/>
								<label>Update valuations</label>
							</div>
							$data
						</div>
					</li>
				</ul>
			</div>
		</section>";
		return $html;
	}
	static function save_admin_edit($obj)
	{
		//var_dump($_POST);
		//wp_die( static::get_type() );
		global $wpdb;
		$vdata = array();
		$query = "REPLACE INTO " . $wpdb->prefix . "frmru_member_raiting VALUES ";
		$raiting = 0;
		foreach($_POST['valuations'] as $valuation)
		{
			$ee = explode(",", $valuation);
			$i = 0;
			foreach($ee as $key => $val)
			{
				if($i == 0)
				{
					$i++;
					continue;
				}
				$crd = explode(".", $val);
				$_vdata[]  = array(
					"member_id"		=> $obj->id,
					"expert_id"		=> $ee[0],
					"critery_id"	=> $crd[0],
					"raiting"		=> $crd[1]
				);
				$raiting += $crd[1];
				$vdata[] = "( null, $obj->id, $ee[0], $crd[0], $crd[1], " .time() . ", '' )";
				$i++;
			}
		}
		$query .=  implode(",", $vdata);
		if($_POST['update_valuatons'])
		{
			$wpdb->query( $query );
		}
		$cats = count(FmRU_Critery::get_all_categories());
		$users = count(static::get_experts());
		$arr = array(
			"order"			=> $_POST['order'],
			"fb_url"		=> $_POST['fb_url'],
			"vk_url"		=> $_POST['vk_url'],
			"instagramm_url"=> $_POST['instagramm_url'],
			"youtube_url"	=> $_POST['youtube_url'],
			"tutor_id"		=> $_POST['tutor_id'],
			"leader_id"		=> $_POST['leader_id'],
			"member_0"		=> $_POST['member_0'],
			"member_1"		=> $_POST['member_1'],
			"member_2"		=> $_POST['member_2'],
			"member_3"		=> $_POST['member_3'],
			"member_4"		=> $_POST['member_4'],
			"member_5"		=> $_POST['member_5'],
			"member_6"		=> $_POST['member_6'],
			"member_7"		=> $_POST['member_7'],
			"member_8"		=> $_POST['member_8'],
			"member_9"		=> $_POST['member_9'],
			"raiting"		=> round($raiting / $cats / $users, 2)  //$_POST['raiting'],
		);
		return $arr;
	}
	
	
	
	
	
	static function add_views_column( $columns )
	{
		//return $columns;
		$columns	= parent::add_views_column( $columns );
		
		
		unset($columns["raiting"]);
		unset($columns["fb_url"]);
		unset($columns["vk_url"]);
		unset($columns["instagramm_url"]);
		unset($columns["youtube_url"]);
		unset($columns["tutor_id"]);
		unset($columns["leader_id"]);
		unset($columns["member_0"]);
		unset($columns["member_1"]);
		unset($columns["member_2"]);
		unset($columns["member_3"]);
		unset($columns["member_4"]);
		unset($columns["member_5"]);
		unset($columns["member_6"]);
		unset($columns["member_7"]);
		unset($columns["member_8"]);
		unset($columns["member_9"]);
		unset($columns["member_10"]);
		$columns["team"] = __("Team", FRMRU);
		$columns["socials"] = __("Socials", FRMRU);
		$columns["group"] = __("Ganre", FRMRU);
		return $columns;
	}
	
	static function fill_views_column($column_name, $post_id) 
	{		
		$obj	= static::get_instance($post_id);
		switch($column_name)
		{
			case "valuations":
				echo "-- | " . $obj->get_meta("raiting");
				break;
			case "order":
				echo $obj->get_meta("order");
				break;
			case "socials":
				$sc = array(
					array(
						"icon"=> "fab fa-stack-2 fa-facebook",	
						"name" => "facebook", 	
						"url"=> $obj->get_meta("fb_url")
					),
					array(
						"icon"=> "fab fa-stack-2 fa-vk",			
						"name" => "vk", 		
						"url"=> $obj->get_meta("vk_url")
					),
					array(
						"icon"=> "fab fa-stack-2 fa-instargamm",	
						"name" => "instargamm", 
						"url"=> $obj->get_meta("instagramm_url")
					),
					array(
						"icon"=> "fab fa-stack-2 fa-youtube",		
						"name" => "youtube", 	
						"url"=> $obj->get_meta("youtube_url")
					)
				);
				echo "<ul>";
				foreach($sc as $s)
				{
					if($s['url'] !== "")
						//echo " <i class='" . $s['icon'] . "'></i> ";
						echo " <li><a href='" .$s['url'] ."'>".$s['name'] ."</a></li>";
				}
				echo "</ul>";
				break;
			case "group":
				echo get_the_term_list( $post_id, FRMRU_GROUP, "", ", ");
				break;
			case "team":
				$tutor = FmRU_Expert::get_instance( $obj->get_meta("tutor_id") );
				$leader = FmRU_Expert::get_instance( $obj->get_meta("leader_id") );
				$member0 = FmRU_Expert::get_instance( $obj->get_meta("member_0") );
				$member1 = FmRU_Expert::get_instance( $obj->get_meta("member_1") );
				$member2 = FmRU_Expert::get_instance( $obj->get_meta("member_2") );
				$member3 = FmRU_Expert::get_instance( $obj->get_meta("member_3") );
				$member4 = FmRU_Expert::get_instance( $obj->get_meta("member_4") );
				$member5 = FmRU_Expert::get_instance( $obj->get_meta("member_5") );
				$member6 = FmRU_Expert::get_instance( $obj->get_meta("member_6") );
				$member7 = FmRU_Expert::get_instance( $obj->get_meta("member_7") );
				$member8 = FmRU_Expert::get_instance( $obj->get_meta("member_8") );
				$member9 = FmRU_Expert::get_instance( $obj->get_meta("member_9") );
				echo "
				<table class='table11'>
					<tbody class=''>
						<tr>
							<td>" . __("Tutor", FRMRU). "</td><td><b>" . $tutor->user->display_name . "</b><td>
						</tr>
						<tr>
							<td>" . __("Project author", FRMRU). "</td><td><b>" . $leader->user->display_name . "</b><td>
						</tr>
						<tr>
							<td>". __("Members", FRMRU). "</td><td><b>" . 
							implode(" <span style='opacity:0.25;'>|</span> ", array(
								$member0->user->display_name,
								$member1->user->display_name,
								$member2->user->display_name,
								$member3->user->display_name,
								$member4->user->display_name,
								$member5->user->display_name,
								$member6->user->display_name,
								$member7->user->display_name,
								$member8->user->display_name,
								$member9->user->display_name,
							)) . "</b><td>
						</tr>
					</tbody>
				</table>";
				
				break;
			case "image":
				$d = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(100, 100));
				$cur_bgnd = $d[0];
				echo "<div style='width:160px;height:150px;background:url($cur_bgnd);background-size:cover;'></div>";
				break;
			default:
				parent::fill_views_column($column_name, $post_id) ;
				break;
		}
		
	}
	static function delete_user ( $user_id )
	{
		global $wpdb;
		$query = "DELETE FROM " . $wpdb->prefix . "frmru_member_raiting WHERE expert_id=" . $user_id ;
		$wpdb->query( $query );
		$query = "DELETE FROM " . $wpdb->prefix . "frmru_member_expert WHERE expert_id=" . $user_id ;
		$wpdb->query( $query );
	}
	static function before_delete_post( $postid )
	{
		global $wpdb;
		$diary = get_term_by("name", (string)$postid, FRMRU_DIARY);
		if($diary)
			wp_delete_term( $diary->term_id, FRMRU_DIARY );
		$post = get_post( $postid );
		if( ! $post || $post->post_type !== static::get_type() ) 
		{
			$query = "DELETE FROM " . $wpdb->prefix . "frmru_member_raiting
			WHERE member_id=" . $user_id ;
		}		
		if( ! $post || $post->post_type !== FmRU_Critery::get_type() ) 
		{
			$query = " DELETE FROM " . $wpdb->prefix . "frmru_member_raiting WHERE critery_id=" . $user_id ;
			$query2 = "DELETE FROM " . $wpdb->prefix . "frmru_member_expert WHERE critery_id=" . $user_id ;
			$wpdb->query( $query2 );
		}		
		$wpdb->query( $query );
		
	}
	
	static function get_experts()
	{
		$args = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => "Expert",
			'orderby'      => 'id',
			'order'        => 'ASC',
			'fields'       => 'ID'
		);
		$users = get_users( $args );
		return $users;
	}
	static function validate ($valuations)
	{
		if(!is_array($valuations))
			$valuations = array();
			
		//var_dump($valuations);
		$allc = FmRU_Critery::get_all();
		$arr = array();		
		$users = static::get_experts();
		foreach( $users as $user )
		{
			if(!is_array($arr[$user]))  
				$arr[$user] = array();
			foreach($allc as $criter)
			{
				//$critery = FmRU_Critery::get_instance($criter);
				//$arr[$user][$criter] = !$valuations[$user][$criter] ? 1 : $valuations[$user][$criter];
				unset($fff);
				foreach($valuations as $v)
				{
					if($v['expert_id'] == $user && $v['critery_id'] == $criter->ID)
					{
						$fff = $v;
						break;
					}	
				}
				if(isset($fff))
				{
					$arr[$user][$criter->ID] = $v['raiting'];
				}
				else
				{
					$arr[$user][$criter->ID] = 0;
				}
			}
		}
		return $arr;
	}
	function save_descr($expert_id, $critery_id, $descr)
	{
		global $wpdb;
		$query = "UPDATE `" . $wpdb->prefix . "frmru_member_raiting` SET `description`='$descr' 
		WHERE  expert_id=$expert_id AND member_id=$this->id AND critery_id=$critery_id;";
		$wpdb->query($query);
	}
	function get_descr($expert_id, $critery_id)
	{
		global $wpdb;
		//get old description
		$query = "SELECT description FROM " . $wpdb->prefix . "frmru_member_raiting WHERE member_id = $this->id AND  expert_id=$expert_id AND critery_id=$critery_id";
		return (string)$wpdb->get_var($query);
	}
	function save_raiting( $expert_id, $critery_id, $raiting, $descr)
	{
		global $wpdb;
		$old_description = (string)$this->get_descr($expert_id, $critery_id);
		if($raiting)
			$query = "REPLACE INTO " . $wpdb->prefix . "frmru_member_raiting 
			VALUE(null, $this->id, $expert_id, $critery_id, $raiting, " . time() . ", '$descr')";
		else
			$query = "DELETE FROM " . $wpdb->prefix . "frmru_member_raiting
			WHERE expert_id='$expert_id' AND critery_id='$critery_id' AND member_id='$this->id';";
		//wp_die($query);
		$wpdb->query($query);
		return [ "old"=> (string)$old_description, "new"=>(string)$descr ];
	}
	function get_raiting_data()
	{
		global $wpdb;
		$query = "SELECT * FROM " . $wpdb->prefix . "frmru_member_raiting WHERE member_id=" .$this->id;
		return $wpdb->get_results($query, ARRAY_A );
	}
	function get_member_raiting()
	{
		global $wpdb;
		$query = "SELECT SUM(raiting) FROM " . $wpdb->prefix . "frmru_member_raiting WHERE member_id=" .$this->id;
		return $wpdb->get_var($query);
	}
	function get_member_cat_raiting( $category_id )
	{
		global $wpdb;
		$query = "SELECT SUM(raiting) FROM " . $wpdb->prefix . "frmru_member_raiting WHERE member_id=" .$this->id .
		" AND critery_id IN (
			SELECT post.ID 
			FROM fst_posts AS post 
			LEFT JOIN ".$wpdb->prefix."term_relationships AS term ON term.object_id = post.ID 
			WHERE term.term_taxonomy_id='$category_id'
		)";
		return $wpdb->get_var($query);
	}
	function get_raiting_critery( $rating_data, $critery_id, $mine_only=false )
	{
		$raiting = 0;
		foreach( $rating_data as $r)
		{
			if($r['critery_id'] == $critery_id )
			{
				if($mine_only && $r['expert_id'] != get_current_user_id())
					continue;
				$raiting += $r['raiting']; 
			}
		}
		return $raiting;
	}
	function get_own_experts()
	{
		global $wpdb;
		$query = "
		SELECT DISTINCT user.display_name FROM `" . $wpdb->prefix . "users` AS user
		LEFT JOIN `" . $wpdb->prefix . "frmru_member_raiting` AS fmr ON fmr.expert_id = user.ID
		WHERE fmr.member_id = ".$this->id;
		//wp_die(($query));
		$experts = $wpdb->get_results($query);
		return $experts;
	}
	
	
	static function get_screen($post)
	{
		require(FMRU_REAL_PATH . "tpl/member_get_screen.php");
		//return get_member_data($post);
		return get_member_screen($post);
	}
	function nn()
	{
		return FmRU_Expert::get_instance(get_current_user_id())->get_new_descript_form( $this->id );
	}
	static function get_raiting_block($rt, $cat, $cr)
	{
		$is_expert 	= FmRU_Expert::is_expert();
		$idd 		= $cat->term_id ."_" .$cr->ID;
		return $is_expert ? "
				<input 
					type='radio' 
					name='".$idd."' 
					id='c_". $idd."_0' 
					value='0' 
					class='radio srait' " . 
					checked( 0, $rt, false) . "
				/>
				<label for='c_". $idd."_0' data-hint='-'></label>						
				<input 
					type='radio' 
					name='".$idd."' 
					id='c_". $idd."_1' 
					value='1' 
					class='radio srait' " . 
					checked( 1, $rt, false) . "
				/>
				<label for='c_". $idd."_1' data-hint='1'></label>						
				<input 
					type='radio' 
					name='".$idd."' 
					id='c_". $idd."_2' 
					value='2' 
					class='radio srait' " . 
					checked( 2, $rt, false) . "
				/>
				<label for='c_". $idd."_2' data-hint='2'></label>						
				<input 
					type='radio' 
					name='".$idd."' 
					id='c_". $idd."_3' 
					value='3' 
					class='radio srait' " . 
					checked( 3, $rt, false) . "
				/>
				<label for='c_". $idd."_3' data-hint='3'></label>	
		"  : sprintf("%.2f", $rt) ;
	}
	function get_full_raiting()
	{
		global $wpdb;
		$query = "SELECT SUM(raiting) AS raiting, COUNT(DISTINCT expert_id) AS experts 
		FROM `" .$wpdb->prefix . "frmru_member_raiting` 
		WHERE member_id='$this->id';";
		$res = $wpdb->get_results($query);
		$round = (int)$res[0]->raiting / $res[0]->experts / count(FmRU_Critery::get_all_categories());
		return substr( $round, 0,4 ) ;
	}
	
	static function get_accordeon_form($id, $style='', $content)
	{
		return 	static::get_accordeon_start_form($id, $style).
					$content .
				static::get_accordeon_finish_form();
	}
	static function get_accordeon_start_form($id, $style="")
	{
		return "<div class='container'>  
				  <div class='clearfix'></div>
				  <div class='panel-group' id='acc$id' role='tablist' aria-multiselectable='true'>
					<div class='panel panel-default'>
					  <div class='panel-heading' role='tab' id='head$id'>
						<div class='panel-title all_crits'>
							<div role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse$id' aria-expanded='false' aria-controls='collapse$id' style='$style'>".
								//__("all Criteries",FRMRU). " <span class='dashicons dashicons-arrow-down'></span>".
								"<div class='fmRU_button'>
									<i class='fa fa-caret-left'></i>
								</div>".
							"</div>
					  </div>
					  </div>
					  <div id='collapse$id' class='panel-collapse collapse in' role='tabpanel' aria-labelledby='head$id'>
						<div class='panel-body'>";
	}
	static function get_accordeon_finish_form()
	{
		return "</div>
					  </div>
					</div>
				  </div>
				</div>";
	}
	function get_expert_descs()
	{
		global $wpdb;
		$query = " 
		SELECT * FROM ".$wpdb->prefix."frmru_member_expert
		WHERE member_id=".$this->id ." ";
		return $wpdb->get_results($query);
	}
	function is_me_descs()
	{
		global $wpdb;
		$query = " 
		SELECT COUNT(*) FROM ".$wpdb->prefix."frmru_member_expert
		WHERE member_id=".$this->id ." AND expert_id='" . get_current_user_id() . "';";
		return $wpdb->get_var($query);
	}
	
	function get_short_form( $data = -1, $rat, $is_expert, $raitings )
	{
		$experts = array();
		foreach($raitings as $rait)
		{
			if($rait['member_id'] == $this->id)
			{
				$experts[] = $rait['expert_id'];
				if($is_expert && $rait['expert_id'] != get_current_user_id())
					continue;
				$rat += $rait['raiting'];
			}
		}
		$class = $rat ? '' : ' noselect ';
		$xperts = count(array_unique($experts));
		$_xperts = $xperts ? "
			<div class='xperts'>".
				$xperts.
			"</div>" : "";
		
			
		$members = "
		<div class='col-md-2 col-sm-3 col-6'>". 
				"<div class='member $class' mid='".$this->id."' frmru_type='" . FmRU_Member::get_type() . "' frmru_args='".$this->id."'>
					<img src='" . FMRU_URLPATH . "img/empty.png' class='ssfr'/>
					<div class='member_title'>" .
						$this->get_meta("order") .
					"</div>
					$_xperts
				</div>".
		"</div> ";
		return $members;
	}
	function get_my_comment( $critery_id)
	{
		global $wpdb;
		$query = "SELECT description FROM  `".$wpdb->prefix."frmru_member_raiting` WHERE member_id=$this->id AND critery_id=$critery_id LIMIT 1 ";
		$res = $wpdb->get_var($query);
		return $res ? $res : "";
	}
	function get_thrumbnail()
	{
		$thrumb_id	= get_post_thumbnail_id($this->id);
		$thrumb_id	= $thrumb_id ? $thrumb_id : FmRU::$options['default_member_thrumb'];
		$d 			= wp_get_attachment_image_src($thrumb_id, "full");
		return $d[0];
	}
	function get_large_form($data = -1)
	{
		if(!is_array($data))	$data = ["experts"=>44];
		$id 		= $this->id;
		$order		= $this->get_meta("order");
		$title		= $this->get("post_title");
		$experts	= $data["experts"];
		$valuats	= $data["valuations"];
		$raiting	= $data["raiting"];
		$class 		= (int)$valuats > 0 ? '' : " noselect $valuats ";
		$bck		= $this->get_thrumbnail();
		$terms		= get_the_terms( $id, FRMRU_GROUP );
		$ganres		= array( );		
		if($terms)
		{			
			foreach( $terms as $term)
			{
				$color 		= get_term_meta($term->term_id, "color", true);
				$icon  		= get_term_meta($term->term_id, "icon", true);
				$d 			= wp_get_attachment_image_src($icon, array(100, 100));
				$cur_bgnd 	= $d[0];
				$cardIcons	.= "<div class='card-icon' style='background-image:url($cur_bgnd); background-color:$color' title='" .$term->name  . "'></div>";
				$ganres[]	= "<strong style='color:$color;'>".$term->name."</strong>";
			}
		}
		$html = "<div class='col-lg-4 col-md-6 col-sm-12 col-12 ' >
				<div class='card' >				  
					<div class='card-img' data-mid='$id' data-fmru_type='fmru_player' data-args='$id' style='background-image:url($bck)!important; border-color:$color; '>
							<div class='card-id'>$order</div>
					</div>
					<div class='card-icons'>$cardIcons</div>
					<div class='card-header' title='$title' style='height:61px; overflow:hidden; padding:0 1.25em;position: relative;display: table;'>
						<h5 class='card-title' style='display: table-cell; vertical-align:middle;'>".
							wp_trim_words($title, 12).
						"</h5>
					</div>
					<ul class='list-group list-group-flush'>
						<li class='list-group-item'>
							<span class='data'><span class='hideColor'>.</span>".
								implode(", ", $ganres).
							"</span>
						</li>
						<li class='list-group-item'>
							<span class='discr'>".
								__('Valuations:', FRMRU).
							"</span>
							<span class='data'>$valuats</span>
						</li>
						<li class='list-group-item'>
							<span class='discr'>".
								__('Raiting:', FRMRU).
							"</span>
							<span class='data'>$raiting</span>
						</li>
						<li class='list-group-item'>
							<span class='discr'>".
								__('Expert count:', FRMRU).
							"</span>
							<span class='data selected'>".($experts === 0 ? '-' : $experts)."</span>
						</li>
					</ul>
					<div class='card-body align-self-center'>
						<div class='fmRU_button ' data-mid='$id' data-fmru_type='fmru_player' data-args='$id' >
							<i class='fa fa-caret-right'></i>
							
						</div>
					</div>
				</div>
				<div class='spacer-30'></div>
			</div>";
		return $html;
	}
	function get_owners_id()
	{
		return array(
			$this->get_meta("tutor_id"),
			$this->get_meta("leader_id"),
			$this->get_meta("member_0"),
			$this->get_meta("member_1"),
			$this->get_meta("member_2"),
			$this->get_meta("member_3"),
			$this->get_meta("member_4"),
			$this->get_meta("member_5"),
			$this->get_meta("member_6"),
			$this->get_meta("member_7"),
			$this->get_meta("member_8"),
			$this->get_meta("member_9"),
		);
	}
	function get_diary($offset=0, $numberposts = 4)
	{
		$args = array(
			"post_type" 	=> "post",
			"offset" 		=> $offset,
			"post_status"	=> is_user_logged_in() && in_array(  get_current_user_id(), static::get_owners_id() ) ? [ "publish", "private" ] : "publish",
			"numberposts"	=> $numberposts,
			"tax_query"		=> array(
				array(
					'taxonomy'	=> FRMRU_DIARY,
					'field'    	=> 'name',
					'terms'    	=> "$this->id"
				)
			),
		);
		return get_posts($args);
	}
	function get_all_diaries()
	{
		$args = array(
			"post_type" 	=> "post",
			"post_status"	=> "publish",
			"numberposts"	=> -1,
			"fields"		=> "ids",
			"tax_query"		=> array(
				array(
					'taxonomy'	=> FRMRU_DIARY,
					'field'    	=> 'name',
					'terms'    	=> "$this->id"
				)
			),
		);
		return get_posts($args);
	}
	static function get_ganre_swicher($params = -1)
	{
		if(!is_array($params))
			$params = ["prefix" =>"ganre" ];
		$ganres	= FmRU::get_all_terms( FRMRU_GROUP );
		$html 	= "<div class='row'>";
		foreach($ganres as $ganre)
		{
			$icon 		= get_term_meta($ganre->term_id, "icon", true);
			$d 			= wp_get_attachment_image_src($icon, array(100, 100));
			$cur_bgnd 	= $d[0];
			$html .= "
			<div class='col-6'>
				<input 
					type='checkbox' 
					name='" . $params['prefix'] . "[]'
					id='" . $params['prefix'] . "_" . $ganre->term_id . "'
					term_id='" . $ganre->term_id . "'
					class='ganre_checkbox'
					value='" . $ganre->term_id . "' ".
					checked(1, $params['checked'], false).
				"/>
				<label for='" . $params['prefix'] . "_" . $ganre->term_id . "'>
					" . $ganre->name . "
					<img src='$cur_bgnd' alt='' />
				</label>
			</div>";
		}
		$html .= "</div>";
		return $html;
	}
	
}
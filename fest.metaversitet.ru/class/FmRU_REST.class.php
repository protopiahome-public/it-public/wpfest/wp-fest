<?php
class FmRU_REST
{
	static function auth($code, $login)
	{
		global $wpdb, $wp_json_basic_auth_error;;
		//$username 		= $_SERVER['PHP_AUTH_USER'];
		//$password 		= $_SERVER['PHP_AUTH_PW'];
		$user = wp_signon(array(
			'user_login'    => $username,
			'user_password' => $password,
			'remember'      => true,
		), true);
		/*$user = wp_authenticate( $username, $password );*/
		if(!is_wp_error($user))
		{
			wp_set_current_user( $user->ID, $user->user_login );
			wp_set_auth_cookie( $user->ID );
			do_action( 'wp_login', $user->user_login );
			$wp_json_basic_auth_error = true;
			return $user;
		}
		else
		{
			$wp_json_basic_auth_error = $user;
			return null;
		}
	}
	static function send( $recieve )
	{
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		$command			= $recieve->get_param('code');
		$args 				= $recieve->get_param('args');
		$username			= $recieve->get_param('username');
		$password			= $recieve->get_param('password');
		$mpp				= $recieve->get_param('mpp');
		$gfilter			= $recieve->get_param('gfilter');
		$user				= static::auth("auth", $username);
		$all_ganres			= static::get_all_terms( $user );
		$gfilter			= is_array($gfilter) && count($gfilter) ? $gfilter : static::get_full_gfilter($all_ganres);
		$all_ganres			= static::get_all_terms( $user, $gfilter );
		switch($command)
		{
			case "test":
				$response = $args;
			case "myConsole":
				$response 	= static::myConsole( $args, $user, $all_ganres );
				break;
			case "user":
				$response 	= static::user( $args, $user );
				break;
			case FRMRU_PLAYER:
				$response 	= static::player( $args, $user );
				break;
			case "newProjectDoc":
				$response 	= static::newProjectDoc( $args, $user );
				break;
			case "send_member_descr":
				$response 	= static::send_member_descr( $args, $user );
				break;
			case "ozenka":
				$response 	= static::ozenka( $args, $user );
				break;
			case "set_exp_descr":
				$response 	= static::set_exp_descr( $args, $user );
				break;
			case "myProjectsList":
				$response 	= static::myProjectsList( $args, $user, $all_ganres );
				break;
			case "get_my_project":
				$response 	= static::get_my_project( $args, $user, $all_ganres );
				break;
			case "create_project":
				$response 	= static::create_project( $args, $user, $all_ganres );
				break;
			case "all_members":
				$response 	= static::all_members( $args, $user );
				break;
			case "update_my_project":
				$response 	= static::update_my_project ( $args, $user, $all_ganres );
				break;
			case "get_my_roles":
				$response 	= static::get_my_roles( $args, $user );
				break;
			case "get_rolereq_page":
				$response 	= static::get_rolereq_page( $args, $user );
				break;
			case "req_role":
				$response 	= static::req_role( $args, $user );
				break;
			case "allow_role_req":
				$response 	= static::allow_role_req( $args, $user );
				break;
			case "veto_role_req":
				$response 	= static::veto_role_req( $args, $user );
				break;
			case "save_user_name":
				$response 	= static::save_user_name( $args, $user );
				break;
			case "save_avatar":
				$response 	= static::save_avatar( $args, $user );
				break;
			case "create_critery":
				$response 	= static::create_critery( $args, $user );
				break;
			case "insert_diary":
				$response 	= static::insert_diary( $args, $user );
				break;
			case "register_user":
				$response 	= static::register_user( $args, $gfilter, $mpp );
				break;
			case "get_admin":
			case "get_requests":
				$response 	= static::get_admin( $args, $user );
				break;
			case "invalid_username":
			case "incorrect_password":
				$response 	= static::invalid_username( $args, $user);
				break;
			case "list":
				$response 	= static::main( $args, $user, $gfilter, $mpp );
				break;
			case "page":
				$response 	= FmRU::$options["enabled_rules"] ? static::about( $args, $user) : static::main( $args, $user, $gfilter, $mpp );
			default:
				break;
		}
		//
		$response->name 	= $user ? $user->display_name : "";
		$avatar 			= $user && is_plugin_active('add-local-avatar/avatars.php') ? get_avatar($user->ID, 100) : "";
		preg_match( "@src='([^']+)'@" , $avatar, $match );
		$response->avatar	= strpos($match[1], "http")===false ? $match[1] : "";
		$response->user_id 	= $user ? $user->ID : 0;
		$response->roles	= FmRU_Expert::get_roles();
		$response->mpp		= $mpp;
		$response->code_old	= $command;
		$response->logo 	= wp_get_attachment_image_src(FmRU::$options['logotype_0'], "full")[0];
		$response->mtype 	= FmRU_Expert::is_expert() && FmRU::$options['status'] == PRESENTATION_PHASE ? 
			"box" : 
			FmRU::$options['member_card'];
		$response->is_expert= FmRU_Expert::is_expert() ? 1 : 0;
		$response->ganres	= $all_ganres;
		$response->gfilter	= $gfilter;
		$response->is_register = FmRU::$options['is_register'];
		$response->status 	= FmRU::$options['status'];
		$response->enabled_rules = FmRU::$options['enabled_rules'] ? 1 : 0;
		$response->fmru_evlbl_roles = FmRU::$options['fmru_evlbl_roles'];
		$response->args		= $args;
		return $response;
	}
	static function about( $args, $user )
	{
		$paged			= 5;
		$offset			= is_array($args) ? $args['o'] : $args;
		$Data 			= new StdClass;
		$page_id2		= (int)FmRU::$options['page_id2'];
		if($page_id2 > 0)
		{
			$cont		= get_post($page_id2);			
			$Data->title	= $cont->post_title;
			$Data->content	= $cont->post_content;
		}
		else
		{		
			$Data->title	= __('Festival rules', FRMRU);
			$Data->content	= "content";
		}
		$posts = get_posts([
			"numberposts"	=> $paged,
			"offset"		=> $paged * $offset,
			"post_type"		=> "post",
			"post_status"	=> "publish",
			'tax_query' => array(
				array(
					'taxonomy' => FRMRU_DIARY,
					'operator' => "EXISTS",
				)
			)
		]);
		$Data->posts	= array();	
		foreach($posts as $p)
		{
			$diary 		= get_the_terms($p->ID, FRMRU_DIARY);
			$drid		= $diary[0]->name;
			$Data->posts[]	= [
				"post_title"	=> $p->post_title,
				"post_content"	=> apply_filters("the_content", $p->post_content),
				"post_author"	=> get_user_by("id", $p->post_author)->display_name,
				"post_date"		=> $p->post_date,
				"id"			=> $p->ID,
				"diary"			=> get_post($drid)->post_title,
				"prid"			=> $drid
			];
		}
		
		$Data->colm 	= static::get_all_posts_count();
		$Data->pagi 	= array( (int)( $offset ), ceil( $Data->colm /$paged) );
		$Data->code		= "page";
		$Data->my_members = static::get_my_projects( FmRU::get_raiting_data(), $user );
		return $Data;
	}
	static function register_user( $args, $gfilter, $mpp )
	{
		$userdata = array(
			'user_pass'       => $args['psw'], // обязательно
			'user_login'      => $args['log'], // обязательно
			'user_email'      => $args['eml'],
			'rich_editing'    => 'true', // false - выключить визуальный редактор
			'role'            => 'Project_member', // (строка) роль пользователя
		);
		$id 	= wp_insert_user( $userdata );
		$userr	= get_user_by("id", $id);
			$user = wp_signon(array(
				'user_login'    => $args['log'],
				'user_password' => $args['psw'],
				'remember'      => true,
			), true);
		/**/
		$Data = static::main(0, $user, $gfilter, $mpp);
		$Data->aa = $args;
		$Data->prompt = __("User register successful", FRMRU);
		return $Data;
	}
	static function create_critery( $args, $user )
	{
		if(!FmRU_Expert::is_user_role("Expert"))
		{
		}
		if($args['text'] == "")
		{
		}
		$expert		= FmRU_Expert::get_instance( $user->ID );
		$data		= array(
			'post_name'		=> $args['text'],
			'post_title'	=> $args['text'],
			'post_content'	=> "",
			"expert_id"		=> $expert->user_id
		);
		$new	= $expert->insert_critery( $data, $args['cr'] );
		$Data 	= static::player( $args['id'], $user );
		$Data->prompt = array(
			"title"		=> __("Hura!!!!", FRMRU),
			"content"	=> __("You succsessful create new own critery!", FRMRU)
		);
		return $Data;
	}
	static function get_all_terms( $user, $gfilter=-1, 	$ids_only=false )
	{
		$terms = get_terms( array(
			'taxonomy'      => array( FRMRU_GROUP ),
			'orderby'       => 'name',
			'order'         => 'ASC',
			'hide_empty'    => false,
			'fields'        => 'all'
		));
		$ganres		= array( );
		$ids		= array( );
		if($terms)
		{
			foreach( $terms as $term)
			{
				$color 		= get_term_meta($term->term_id, "color", true);
				$icon  		= get_term_meta($term->term_id, "icon", true);
				$d 			= wp_get_attachment_image_src($icon, array(100, 100));
				$cur_bgnd 	= $d[0];
				if(is_array($gfilter))
					$check	= in_array($term->term_id, $gfilter) ? 1 : 0;
				else
					$check	= 1;
				$ganres[]	= array("id"=>$term->term_id, "icon" => $cur_bgnd, "color" => $color, "name" => $term->name, "check" => $check);
				$ids[] 		= $term->term_id;
			}
		}
		$null_icon 	= FMRU_URLPATH . "img/empty.png";
		if(is_array($gfilter))
			$check	= in_array(-1, $gfilter) ? 1 : 0;
		else
			$check	= 1;
		$ganres[]	= array("id"=>-1, "icon" => $null_icon, "color" => "#6c757d", "name" => __("Vague Ganres", FRMRU), "check" => $check);
		return 	$ids_only ? $ids : $ganres;
	}
	static function get_full_gfilter($all_ganres = -1)
	{
		$arr = array();
		if(!is_array($all_ganres) || !count($all_ganres))
		{
			$all_ganres = static::get_all_terms( -1 );
		}
		foreach($all_ganres as $ganre)
		{
			$arr[] = $ganre["id"];
		}
		return $arr;
	}
	static function user( $args, $user )
	{
		global $wpdb;
		if(!$user->display_name)
			return static::main(0, $user);
		$Data 			= new StdClass;
		//mine projects
		$query = "SELECT DISTINCT post.post_title, post.ID, GROUP_CONCAT(DISTINCT meta.meta_key) AS roles FROM $wpdb->posts AS post
LEFT JOIN  $wpdb->postmeta AS meta ON meta.post_id=post.ID
WHERE (meta.meta_key='member_0' OR meta.meta_key='member_1' OR meta.meta_key='member_2' OR meta.meta_key='member_3' OR meta.meta_key='member_4' OR meta.meta_key='member_5'  OR meta.meta_key='member_6'  OR meta.meta_key='member_7'  OR meta.meta_key='member_8'  OR meta.meta_key='member_9' OR  meta.meta_key='leader_id' OR meta.meta_key='tutor_id')
AND meta.meta_value=$user->ID
AND post.post_type='fmru_player'
AND post.post_status='publish'
GROUP BY post.ID; ";
		$projects		= $wpdb->get_results( $query );
		$Data->projects	= $projects;
		$Data->ext		= new StdClass;
		$Data->ext->type= "null";
		$Data->code		= "user";
		if(in_array("administrator", $user->roles))
		{
			$requests = get_option("roles_requests");
			$requests = !is_array($requests) ? array() : $requests;
			$Data->role_requests = $requests;
		}
		$Data->ext->all = FmRU_Member::get_all_count();
		return $Data;
	}
	static function invalid_username($args, $user)
	{
		$Data 			= $main(0, $user);
		$Data->prompt 	= "Invalid user name";
		return $Data;
	}
	static function main( $args, $user, $gfilter=-1, $mpp=-1 )
	{
		$mpp == -1 ? FmRU::$options['member_per_page'] : $mpp;
		$Data 			= new StdClass;
		$args			= !is_numeric($args) ? 0 : $args;
		$paged 			= $mpp;
		if(!$paged)$paged = 120;
		$is_expert		= FmRU_Expert::is_expert();
		$raitings 		= FmRU::get_raiting_data();
		$pars = array(
			"numberposts"	=> $paged,
			"offset"		=> $args * $paged,
			"post_type"		=> FmRU_Member::get_type(),
			"post_status"	=> "publish",
			'orderby' 		=> 'meta_value_num',
			'meta_key' 		=> 'order',
			'order' 		=> 'ASC',
			//'meta_query'	=> $notowners
		);
		if( !count($gfilter) || !is_array($gfilter) )
		{
			$gfilter = FmRU_Critery::get_all_categories("term_id");
		}
		$pars['tax_query'] 	= array(
			"relation"		=> "OR",
			array(
				'taxonomy' 	=> FRMRU_GROUP,
				'field'    	=> 'term_id',
				'terms'    	=> $gfilter,
				"operator" 	=> "IN"
			),
			//
		);
		if(in_array("-1", $gfilter))
		{
			$pars['tax_query'][] = array(
				'taxonomy' 	=> FRMRU_GROUP,
				"operator" 	=> "NOT EXISTS"
			);
		}
		$all_memebers 	= get_posts($pars);
		$members		= array();
		foreach($all_memebers as $member)
		{
			$mem 		= FmRU_Member::get_instance($member);
			$rat 		= 0;
			$all_owners = $mem->get_owners_id();
			$experts = array();
			foreach($raitings as $rait)
			{
				if($rait['member_id'] == $mem->id)
				{
					$experts[] = $rait['expert_id'];
					if($is_expert && $rait['expert_id'] != get_current_user_id())
						continue;
					$rat += $rait['raiting'];
				}
			}
			$mmbr 		= new stdClass;
			$mmbr->cl 	= !$rat && in_array( FmRU::$options['status'], [PRESENTATION_PHASE, AFTER_FINISH_PHASE]) ? 'noselect' : '';
			$mmbr->r	= $rat;
			$mmbr->id 	= $mem->id;
			$mmbr->title= $mem->get("post_title");
			$mmbr->content = wp_trim_words(apply_filters("the_content", $mem->get("post_content")), 15, "...");
			$mmbr->o 	= $mem->get_meta("order");
			$mmbr->e 	= count(array_unique($experts));
			$mmbr->img	= $mem->get_thrumbnail();
			$terms		= get_the_terms( $mem->id, FRMRU_GROUP );
			$ganres		= array( );
			$gdiff		= array( );
			if($terms)
			{
				foreach( $terms as $term)
				{
					$color 		= get_term_meta($term->term_id, "color", true);
					$icon  		= get_term_meta($term->term_id, "icon", true);
					$d 			= wp_get_attachment_image_src($icon, array(100, 100));
					$cur_bgnd 	= $d[0];
					$ganres[]	= array("id"=>$term->term_id, "icon" => $cur_bgnd, "color" => $color, "name" => $term->name);
					$gdiff[] 	= $term->term_id;
				}
			}
			$mmbr->ganres	= $ganres;
			$members[]		= $mmbr;
		}
		$Data->my_members = static::get_my_projects( $raitings, $user );
		$Data->members 	= $members;
		$Data->colm 	= FmRU_Member::get_all_count( $pars );
		$Data->pagi 	= array( (int)($args), ceil( $Data->colm /$paged) );
		$Data->code 	= "list";
		return $Data;
	}
	static function newProjectDoc($args, $user)
	{
		$Data 				= new StdClass;
		$member 			= FmRU_Member::get_instance($id);
		$is_expert 			= FmRU_Expert::is_expert($user->ID);
		$Data->is_expert	= $is_expert;
		$Data->code			= "newProjectDoc";
	}
	static function player( $args = 155, $user )
	{
		global $wpdb;
		$Data 			= new StdClass;
		$id				= is_array($args) ? $args['id'] : $args;
		$offset			= is_array($args) ? $args['offset'] : 0;
		$id				= $id == -1 ? 155 : $id;
		$member 		= FmRU_Member::get_instance($id);
		$is_expert 		= FmRU_Expert::is_expert($user->ID);
		$experts 		= $member->get_own_experts();
		$count_juri 	= count($experts) > 0 ? count($experts) : 1;
		$rating_data 	= $member->get_raiting_data();
		$rait	 		= (float)FmRU_Critery::count_offer_raiting($member->get_member_raiting(), $count_juri * 3);
		$r2				= (float)$rait;// > 8 ? 8 : round($rait);
		$cats 			= FmRU_Critery::get_all_categories();
		$categories		= array();
		if($cats)
		{
			foreach( $cats as $cat)
			{
				$crat = 0;
				//$get_all_by_cat = FmRU_Critery::get_all_by_cat($cat->term_id);
				$get_all_by_cat	= FmRU_Critery::get_by_member( $id, $cat->term_id );
				$clr	 = get_term_meta( $cat->term_id, 'color', true );
				$category 			= new StdClass;
				$category->id 		= $cat->term_id;
				$category->name		= $cat->name;
				$category->color	= $clr;
				$category->criteries = array();
				if($get_all_by_cat)
				{
					foreach($get_all_by_cat as $cr)
					{
						$cr_rating 	= (float)$member->get_raiting_critery( $rating_data, $cr->ID, $is_expert);
						$rt 		= (float)FmRU_Critery::count_offer_raiting( $cr_rating, $is_expert ? 1 : $count_juri );
						$crat 		+= $rt;
						$ctirery	= new StdClass;
						$ctirery->id 		= $cr->ID;
						$ctirery->title  	= $cr->post_title;
						$ctirery->rating 	= sprintf( "%.2f", $rt );
						$ctirery->descr 	= $member->get_my_comment($cr->ID);
						$category->criteries[] = $ctirery;
					}
				}
				$descriptions = array();
				//if(!$is_expert)
				{
					$descrs = FmRU_Critery::get_descriptions_category($cat->term_id, $member->id);
					if(count($descrs))
					{
						foreach($descrs as $descr)
						{
							$expertt = FmRU_Expert::get_instance($descr->expert_id)->user->display_name;
							$descriptions[] = array("txt"=>$descr->description, "auth"=>$expertt, "id"=>$descr->expert_id);
						}
					}
				}
				$category->d 		= $descriptions;
				$category->rating 	= sprintf( "%.2f", $crat);
				$categories[] 		= $category;
			}
		}
		$terms		= get_the_terms( $member->id, FRMRU_GROUP );
		$ganres		= array( );
		if($terms)
		{
			foreach( $terms as $term)
			{
				$color 		= get_term_meta($term->term_id, "color", true);
				$icon  		= get_term_meta($term->term_id, "icon", true);
				$d 			= wp_get_attachment_image_src($icon, array(100, 100));
				$cur_bgnd 	= $d[0];
				$ganres[]	= array("id"=>$term->term_id, "icon" => $cur_bgnd, "color" => $color, "name" => $term->name);
			}
		}
		// Uniq Criteries List
		$uns = FmRU_Critery::get_all_uniq($member->id);
		$uniqs 				= array();
		if(count($uns))
		{
			foreach($uns as $un)
			{
				$cr = FmRU_Critery::get_instance($un);
				$cr_rating 	= (float)$member->get_raiting_critery( $rating_data, $cr->id, $is_expert);
				$rt 		= (float)FmRU_Critery::count_offer_raiting( $cr_rating, $is_expert ? 1 : $count_juri );
				$cat = wp_get_post_terms( $cr->id, FRMRU_CATEGORY )[0];
				$color = get_term_meta($cat->term_id, "color", true);
				$ctirery	= new StdClass;
				$ctirery->id 		= $cr->id;
				$ctirery->color 	= $color;
				$ctirery->title  	= $cr->body->post_title;
				$ctirery->rating 	= sprintf( "%.2f", $rt );
				$uniqs[] = $ctirery;
			}
		}
		if( $is_expert )
		{
			$expert 		= FmRU_Expert::get_instance( $user->ID);
			$aut_criteries 	= $expert->is_critery_author( $id );
			$Data->aut_criteries = $aut_criteries;
		}
		$Data->uniqs = $uniqs;
		//expert's descriptios
		$dess = $member->get_expert_descs();
		$eDess = array();
		if(count($dess))
		{
			foreach($dess as $des)
			{
				$expert = FmRU_Expert::get_instance($des->expert_id);
				if($des->expert_id == $user->ID)
					$enabled = true;
				$eDess[] = ["txt" => $des->descr, "auth" => $expert->user->display_name, "id" => $des->expert_id ];
			}
		}
		$Data->expert_descr = $eDess;
		$Data->denabled = $enabled ? 1 : 0;
		$Data_diary			= array();
		$diaries			= $member->get_diary($offset*5, 5);
		if(count($diaries))
		{
			foreach($diaries as $diary)
			{
				$d			= new StdClass;
				$d->id 				= $diary->ID;
				$d->post_title 		= $diary->post_title;
				$d->post_content 	= $diary->post_content;
				$d->post_date	 	= $diary->post_date;
				$d->post_status	 	= $diary->post_status;
				$d->post_author	 	= get_userdata($diary->post_author)->display_name;
				$Data_diary[]		= $d;
			}
		}
		$Data->member		= array(
			"id" => $member->id, 
			"title" => $member->get("post_title" ), 
			"name" => $member->get("post_name" ), 
			"img" => $member->get_thrumbnail(), 
			"content" => apply_filters("the_content",  $member->get("post_content" )), 
			"ganres" => $ganres, 
			"o" => $member->get_meta("order"), 
			"fb_url" => $member->get_meta("fb_url"), 
			"vk_url" => $member->get_meta("vk_url"), 
			"instagramm_url" => $member->get_meta("instagramm_url"), 
			"youtube_url" => $member->get_meta("youtube_url"),  
			"diary" => $Data_diary, 
			"slide_id" => $is_expert ? "1" : "0"
		);
		$cdiary					= count($member->get_all_diaries());
		$Data->member['cdiary']	= $cdiary;
		$Data->member['pagi']	= array((int)($offset),  ceil($cdiary / 5 ) );
		$Data->lurl			= wp_logout_url();
		$Data->is_expert	= $is_expert;
		$Data->is_roles		= FmRU_Expert::get_roles( );
		$Data->owners		= $member->get_owners_id();
		$Data->experts		= $experts;
		$Data->count_juri	= $count_juri;
		$Data->rait			= sprintf( "%.2f", $rait);
		$Data->r2			= $r2;
		$Data->categories	= $categories;
		$Data->code			= FRMRU_PLAYER;
		return $Data;
	}
	static function ozenka($args, $user)
	{
		$Data = new StdClass;
		if(!FmRU_Expert::is_user_role("Expert", $user->ID))
		{
			$Data->alert = "You haven't rights!";
			return $Data;
		}
		extract( $args );
		$Data = static::player( $mid, $user );
		$member 	= FmRU_Member::get_instance($mid);
		$descr 		= $member->get_descr($user->ID, $crid);
		if(!$d)	$d 	= $descr;
		if( $c == 4 && preg_replace( "/^(\w+\S+)$/g", "", $descr ) == "" )
		{
			$Data = static::player( $mid, $user );
			$Data->prompt = array(
					"title"		=> __("You MUST insert comment!!!!", FRMRU),
					"content"	=> $desciptions[1] == "" ? (string)$desciptions[0]  : (string)$desciptions[1],
					"textarea"	=> !$descr ? " " : $descr,
					"checkTitle"=> __("Do comments permanently", FRMRU),
					"check"		=> "expert_comments",
					"is_check"	=> true,
					"args"		=> [ "mid" => $mid, "crid" => $old_crid, "old" => $descr, "new" => $descr ],
					"okHandler"	=> "set_exp_descr"
				);
		}
		else if($c == 0 )
		{
			$desciptions = $member->save_raiting( $user->ID, $crid, $c, $d);
			$_raiting 	= $member->get_full_raiting();
			$Data = static::player( $mid, $user );
		}
		else
		{
			$desciptions = $member->save_raiting( $user->ID, $crid, $c, $d);
			$_raiting 	= $member->get_full_raiting();
			//update_post_meta($mid, "raiting", $_raiting);
			$Data = static::player( $mid, $user );
			if($is_comment)
				$Data->prompt = array(
					"title"		=> __("Insert comment please", FRMRU),
					"content"	=> $desciptions[1] == "" ? (string)$desciptions[0]  : (string)$desciptions[1],
					"textarea"	=> !$descr ? " " : $descr,
					"checkTitle"=> __("Do comments permanently", FRMRU),
					"check"		=> "expert_comments",
					"is_check"	=> true,
					"args"		=> [ "mid" => $mid, "crid" => $crid, "old"=>$desciptions[0], "new"=>$desciptions[1] ],
					"okHandler"	=> "set_exp_descr"
				);
			$answer_code = "";
		}
		return $Data;
	}
	static function myConsole($args, $user, $all_ganres)
	{
		$Data = static::user( $args, $user );
		/**/
		$Data->ext->type	= "myConsole";
		$Data->ext->title	= __("My Console", FRMRU);
		$ganres = $all_ganres;
		$Data->ext->ganres = array();
		$args = array(
			"post_type"		=> FmRU_Member::get_type(),
			"post_status" 	=> "publish",
			"numberposts" 	=> -1,
			"fields"		=> "ids",
			'tax_query' 	=> array(
				'relation' 	=> 'AND',
				array(
					'taxonomy' 	=> FRMRU_GROUP,
					'field' 	=> 'id',
					'terms' 	=> static::get_all_terms( $user, -1, true ),
					'operator' 	=> 'NOT IN',
				)
			)
		);
		$not_ganres = get_posts( $args );
		foreach($ganres as $ganre)
		{
			$ganre["count"] = $ganre['id'] == -1 ? count($not_ganres) : static::get_members_in_ganre( $ganre['id']);
			$Data->ext->ganres[] = $ganre;
		}
		$Data->ext->data 	= static::get_critery_time_data();		
		$Data->ext->data2 	= static::get_avg_member();
		return $Data;
	}
	static function get_admin($args, $user)
	{
		$Data = static::user( $args, $user );
		$Data->args = $args;
		$Data->ext->type	= "get_admin";
		$Data->ext->title	= __("Administrate", FRMRU);
		$Data->ext->name	= "";
		return $Data;
	}
	static function send_member_descr( $args, $user )
	{
		extract( $args );
		$Data = static::player( $member_id, $user );
		$member 	= FmRU_Member::get_instance($member_id);
		if( $descrr = $member->is_me_descs() )
		{
			$Data = static::player( $member_id, $user );
			$Data->prompt = __("You allready send description", FRMRU);
		}
		else
		{
			$expert = FmRU_Expert::get_instance( $user->ID );
			$succs 	= $expert->descript_member($member_id, $text);
			$Data = static::player( $member_id, $user );
		}
		return $Data;
	}
	static function set_exp_descr($args, $user)
	{
		extract( $args );
		$member 	= FmRU_Member::get_instance($mid);
		$member->save_descr( $user->ID, $crid, $discr);
		$Data = static::player( $mid, $user );
		$Data->last_descr = $member->get_descr($user->ID, $crid);
		return $Data;
	}
	static function myProjectsList( $args, $user, $all_ganres )
	{
		$Data = static::user( $args, $user );
		$Data->ext->type	= "myProjectsList";
		$Data->ext->projects = $Data->projects;
		return $Data;
	}
	static function get_my_project($args, $user, $all_ganres)
	{
		global $wpdb;
		$pr 		= FmRU_Member::get_instance( $args );
		$query 		= "SELECT term_taxonomy_id FROM `" . $wpdb->prefix . "term_relationships` WHERE object_id=$args";
		$Data 		= static::user( $args, $user );
		$proj 			= array();
		$proj['id']		= $pr->id;
		$proj['title']	= $pr->get("post_title");
		$proj['content']	= $pr->get("post_content");
		$proj['ganres']	= $wpdb->get_results($query);
		$Data->ext->maxGanres	= FmRU::$options['max_ganres'];
		$Data->ext->media_img	= $pr->get_thrumbnail();
		$Data->ext->media_id	= get_post_thumbnail_id( $pr->id );
		$Data->ext->type	= "myProject";
		$Data->ext->title	= "";
		$Data->ext->name	= $proj['title'];
		$Data->ext->content	= $proj['content'];
		$Data->ext->fb_url			= $pr->get_meta('fb_url');
		$Data->ext->vk_url			= $pr->get_meta('vk_url');
		$Data->ext->instagramm_url	= $pr->get_meta('instagramm_url');
		$Data->ext->youtube_url		= $pr->get_meta('youtube_url');
		$Data->ext->id		= $proj['id'];
		$Data->ext->tutor_id 	= $pr->get_meta("tutor_id");
		$Data->ext->leader_id 	= $pr->get_meta("leader_id");
		$Data->ext->members 	= [
			$pr->get_meta("member_0") ,
			$pr->get_meta("member_1") ,
			$pr->get_meta("member_2") ,
			$pr->get_meta("member_3") ,
			$pr->get_meta("member_4") ,
			$pr->get_meta("member_5") ,
			$pr->get_meta("member_6") ,
			$pr->get_meta("member_7") ,
			$pr->get_meta("member_8") ,
			$pr->get_meta("member_9") ,
		];
		$Data->ext->args	= $args;
		$Data->ext->ganres	= array();
		foreach($all_ganres as $ganre)
		{
			$nganre = $ganre;
			$pp = new StdClass;
			$pp->term_taxonomy_id = $ganre["id"];
			$nganre['check'] = in_array($pp, $proj['ganres']) ? 1 : 0;
			$Data->ext->ganres[] = $nganre;
		}
		$Data->users 		= static::get_users();
		$Data->criteries	= static::get_criteries();
		return $Data;
	}
	static function create_project( $args, $user, $all_ganres )
	{
		$Data = static::user( $args, $user );
		$Data->args = "";
		$Data->ext->type	= "newProject";
		$Data->ext->title	= __("create new Project", FRMRU);
		$Data->ext->name	= "";
		$Data->ext->args	= $args;
		$Data->ext->ganres	= array();
		$Data->ext->fb_url			= "";
		$Data->ext->vk_url			= "";
		$Data->ext->instagramm_url	= "";
		$Data->ext->youtube_url		= "";
		$Data->ext->tutor_id 	= -1;
		$Data->ext->leader_id 	= $user->ID;
		$Data->ext->members 	= [
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
		];
		foreach($all_ganres as $ganre)
		{
			$nganre = $ganre;
			$nganre['check'] = 0;
			$Data->ext->ganres[] = $nganre;
		}
		$Data->ext->maxGanres	= FmRU::$options['max_ganres'];
		$Data->users 		= static::get_users();
		$Data->criteries	= static::get_criteries();
		return $Data;
	}
	static function all_members( $args, $user )
	{
		$Data = static::user( $args, $user );
		$Data->args = "";
		$Data->ext->type	= "all_members";
		$pars = array(
			"numberposts"	=> -1,
			"offset"		=> 0,
			"post_type"		=> FmRU_Member::get_type(),
			"post_status"	=> "publish",
			'orderby' 		=> 'meta_value_num',
			'meta_key' 		=> 'order',
			'order' 		=> 'ASC',
		);
		$all_memebers 	= get_posts($pars);
		$members		= array();
		foreach($all_memebers as $member)
		{
			$mem 		= FmRU_Member::get_instance($member);
			$rat 		= 0;
			$all_owners = $mem->get_owners_id();
			$experts = array();
			//TODO addraitings data
			/*
			foreach($raitings as $rait)
			{
				if($rait['member_id'] == $mem->id)
				{
					$experts[] = $rait['expert_id'];
					if($is_expert && $rait['expert_id'] != get_current_user_id())
						continue;
					$rat += $rait['raiting'];
				}
			}*/
			$mmbr 		= new stdClass;
			$mmbr->id 	= $mem->id;
			$mmbr->title= $mem->get("post_title");
			$mmbr->content = wp_trim_words($mem->get("post_content"), 10);
			$mmbr->o 	= $mem->get_meta("order");
			$mmbr->e 	= count(array_unique($experts));
			$mmbr->img	= $mem->get_thrumbnail();
			$terms		= get_the_terms( $mem->id, FRMRU_GROUP );
			$ganres		= array( );
			if($terms)
			{
				foreach( $terms as $term)
				{
					$color 		= get_term_meta($term->term_id, "color", true);
					$icon  		= get_term_meta($term->term_id, "icon", true);
					$d 			= wp_get_attachment_image_src($icon, array(100, 100));
					$cur_bgnd 	= $d[0];
					$ganres[]	= array("id"=>$term->term_id, "icon" => $cur_bgnd, "color" => $color, "name" => $term->name);
				}
			}
			$mmbr->ganres		= $ganres;
			$members[]			= $mmbr;
		}
		$Data->ext->members 	= $members;
		return $Data;
	}
	static function update_my_project ( $args, $user, $all_ganres )
	{
		/**/
		if($args['id'] ==  -1)
		{
			$order = FmRU::$options['member_id'];
			FmRU::$options['member_id']++;
			update_option(FRMRU, FmRU::$options);
			$id = FmRU_Member::insert(array(
				"post_title"	=> $args['name'],
				"post_name"		=> $args['name'],
				"post_content"	=> $args['content'],
				"order"			=> $order,
				"fb_url"		=> $args['fb_url'],
				"vk_url"		=> $args['vk_url'],
				"instagramm_url"=> $args['instagramm_url'],
				"youtube_url"	=> $args['youtube_url'],
				"tutor_id"		=> $args['tutor'],
				"leader_id"		=> $args['leader'],
				"member_0"		=> $args['members'][0] ? $args['members'][0] : -1,
				"member_1"		=> $args['members'][1] ? $args['members'][1] : -1,
				"member_2"		=> $args['members'][2] ? $args['members'][2] : -1,
				"member_3"		=> $args['members'][3] ? $args['members'][3] : -1,
				"member_4"		=> $args['members'][4] ? $args['members'][4] : -1,
				"member_5"		=> $args['members'][5] ? $args['members'][5] : -1,
				"member_6"		=> $args['members'][6] ? $args['members'][6] : -1,
				"member_7"		=> $args['members'][7] ? $args['members'][7] : -1,
				"member_8"		=> $args['members'][8] ? $args['members'][8] : -1,
			));
		}
		else
		{
			$id = FmRU_Member::get_instance( $args['id'] );
			$arr = array(
				"tutor_id"		=> $args['tutor'],
				"leader_id"		=> $args['leader'],
				"fb_url"		=> $args['fb_url'],
				"vk_url"		=> $args['vk_url'],
				"instagramm_url"=> $args['instagramm_url'],
				"youtube_url"	=> $args['youtube_url'],
				"member_0"		=> $args['members'][0] ? $args['members'][0] : -1,
				"member_1"		=> $args['members'][1] ? $args['members'][1] : -1,
				"member_2"		=> $args['members'][2] ? $args['members'][2] : -1,
				"member_3"		=> $args['members'][3] ? $args['members'][3] : -1,
				"member_4"		=> $args['members'][4] ? $args['members'][4] : -1,
				"member_5"		=> $args['members'][5] ? $args['members'][5] : -1,
				"member_6"		=> $args['members'][6] ? $args['members'][6] : -1,
				"member_7"		=> $args['members'][7] ? $args['members'][7] : -1,
				"member_8"		=> $args['members'][8] ? $args['members'][8] : -1,
			);
			$id->update_metas( $arr);
			$my_post = array();
			$my_post['ID'] = $args['id'];
			$my_post['post_title'] = $args['name'];
			$my_post['post_content'] = $args['content'];
			wp_update_post( wp_slash($my_post) );
			wp_delete_object_term_relationships( $args['id'], FRMRU_GROUP );
		}
		$terms = array();
		foreach( $args['ganres'] as $gnr )
		{
			$terms[] = (int)$gnr;
		}
		$succs	= wp_set_object_terms($id->id, $terms, FRMRU_GROUP);
		$Data 		= static::get_my_project($id->id, $user, $all_ganres);
		if( $args['media_id'] == -1)
		{
			if(!function_exists('wp_handle_upload')){
				require_once ABSPATH . 'wp-admin/includes/image.php';
				require_once ABSPATH . 'wp-admin/includes/file.php';
				require_once ABSPATH . 'wp-admin/includes/media.php';
			}
			// get rid of everything up to and including the last comma
			$imgData1 = $args['media_img'];
			$imgData1 = substr($imgData1, 1+strrpos($imgData1, ','));
			// write the decoded file
			$filePath	= FMRU_REAL_PATH. 'temp/' . $args['media_name'];
			$fileURL	= FMRU_URLPATH  . 'temp/' . $args['media_name'];
			file_put_contents($filePath, base64_decode($imgData1));
			$mediaId = media_sideload_image( $fileURL, $id->id, $args['media_name'], "id" );
			$media_thrumb	= set_post_thumbnail( $id->id, $mediaId );
			unlink($filePath);
		}
		// add leader role
		wp_get_current_user()->add_role("Project_author");
		$Data->arr = $arr;
		/*
		$Data = static::user( $args, $user );
		$Data->media_path 	= $filePath;
		$Data->media_url 	= $fileURL;
		$Data->media_img 	= $args['media_img'];
		$Data->args 		= 1000;
		$Data->ext 			= new StdClass;
		$Data->ext->type	= "myProject";
		$Data->ext->title	= "";
		$Data->ext->name	= $args['name'];
		$Data->ext->args	= $args;
		$Data->ext->ganres	= array();
		$Data->ext->tutor_id 	= $args['tutor'];
		$Data->ext->leader_id 	= $args['leader'];
		$Data->ext->members 	= [
			$args['members'][0] ? $args['members'][0] : -1,
			$args['members'][1] ? $args['members'][1] : -1,
			$args['members'][2] ? $args['members'][2] : -1,
			$args['members'][3] ? $args['members'][3] : -1,
			$args['members'][4] ? $args['members'][4] : -1,
			$args['members'][5] ? $args['members'][5] : -1,
			$args['members'][6] ? $args['members'][6] : -1,
			$args['members'][7] ? $args['members'][7] : -1,
			$args['members'][8] ? $args['members'][8] : -1,
		];
		foreach($all_ganres as $ganre)
		{
			$nganre = $ganre;
			$nganre['check'] = in_array($nganre['id'], $args['ganres']) ? 1 : 0;
			$Data->ext->ganres[] = $nganre;
		}
		$Data->ext->maxGanres	= FmRU::$options['max_ganres'];
		$Data->users 		= static::get_users();
		$Data->criteries	= static::get_criteries();
		*/
		return $Data;
	}
	static function save_avatar($args, $user)
	{
		if(!is_plugin_active('add-local-avatar/avatars.php'))
		{
			$Data 	 = static::get_my_roles($args, $user);
			return $Data;
		}
		global $add_local_avatars;
		if(!function_exists('wp_handle_upload')){
				require_once ABSPATH . 'wp-admin/includes/image.php';
				require_once ABSPATH . 'wp-admin/includes/file.php';
				require_once ABSPATH . 'wp-admin/includes/media.php';
			}
			// get rid of everything up to and including the last comma
			$imgData1 = $args['media_img'];
			$imgData1 = substr($imgData1, 1+strrpos($imgData1, ','));
			// write the decoded file
			//$filePath	= FMRU_REAL_PATH. 'temp/' . $args['media_name'];
			$root = $add_local_avatars->avatar_root();
			$path = trailingslashit($add_local_avatars->avatar_options['upload_dir']);
			$file = sanitize_file_name($args['media_name']);
			file_put_contents($root . $path . $file, base64_decode($imgData1));
			chmod($root . $path . $file, 0644);
			update_usermeta($user->ID, 'avatar', $path . $file);
			$Data 	 = static::get_my_roles($args, $user);
			return $Data;
	}
	static function insert_diary($args, $user)
	{
		$member = FmRU_Member::get_instance($args['mid']);
		if(!in_array( $user->id, $member->get_owners_id()))
		{
			//return ["alert" =>"You not owner of this Project", "code"=>"alert"];
		}
		$id = wp_insert_post(array(
			"post_title"	=> $args['title'],
			"post_name"		=> $args['title'],
			"post_content"	=> $args['content'],
			'post_status'   => $args['is_private'] ? 'private' : 'publish',
		));
		wp_set_object_terms($id, $args['mid'], FRMRU_DIARY);
		/**/
		$Data = static::player( $args['mid'], $user );
		return $Data;
	}
	static function save_user_name($args, $user)
	{
		$user_id = wp_update_user( array( 'ID' => $user->ID, 'display_name' => $args ) );
		$Data 	 = static::get_my_roles($args, $user);
		return $Data;
	}
	static function get_my_roles($args, $user)
	{
		$Data 				= static::user( $args, $user );
		$Data->reqRoles 	= get_user_meta($user->ID, "roles_req", true);
		$Data->reqRoles 	= !is_array($Data->reqRoles) ? [] : $Data->reqRoles;
		update_user_meta($user->ID, "roles_req", $Data->reqRoles );
		//update_user_meta($user->ID, "roles_req", []);
		$Data->ext->type	= "get_my_roles";
		return $Data;
	}
	static function get_rolereq_page($args, $user)
	{
		$Data 				= static::user( $args, $user );
		$Data->ext->type	= "get_rolereq_page";
		$requests 			= get_option("roles_requests");
		$requests 			= is_array($requests) ? $requests : array();
		$reqs  = [];
		foreach($requests as $req)
		{
			$reqs[]			= ["id"=>$req[0], "name"=> get_userdata($req[0])->display_name, "role" => $req[1]];
		}
		$Data->ext->requests = $reqs;
		return $Data;
	}
	static function allow_role_req($args, $user)
	{
		if(!in_array("administrator", $user->roles))
		{
			$Data 				= static::get_rolereq_page( $args, $user );
			$Data->prompt	= __("You are not Administrator!!!!!!", FmRU);
			return $Data;
		}
		$requests 			= get_option("roles_requests");
		$r = [];
		foreach($requests as $request)
		{
			if($request[0] == $args['id'] && $request[1] == $args['role'])
			{
				$ruser 		= get_userdata($args['id']);
				$ruser->add_role($args['role']);
				$req		= get_user_meta($args['id'], "roles_req", true);
				$nreq		= [];
				foreach($req as $reqq)
				{
					if($reqq === $args['role'])
					{
					}
					else
					{
						$nreq = $reqq;
					}
				}
				update_user_meta($args['id'], "roles_req",$nreq);
			}
			else
			{
				$r[] = $request;
			}
		}
		update_option("roles_requests", $r);
		$Data 				= static::get_rolereq_page( $args, $user );
		$Data->rroles = $ruser->roles;
		$Data->rroles2 = is_array($roles);
		return $Data;
	}
	static function veto_role_req($args, $user)
	{
		if(!in_array("administrator", $user->roles))
		{
			$Data 				= static::get_rolereq_page( $args, $user );
			$Data->prompt	= __("You are not Administrator!!!!!!", FmRU);
			return $Data;
		}
		$requests 			= get_option("roles_requests");
		$r = [];
		foreach($requests as $request)
		{
			if($request[0] == $args['id'] && $request[1] == $args['role'])
			{
				$req		= get_user_meta($args['id'], "roles_req", true);
				$nreq		= [];
				foreach($req as $reqq)
				{
					if($reqq === $args['role'])
					{
					}
					else
					{
						$nreq = $reqq;
					}
				}
				update_user_meta($args['id'], "roles_req",$nreq);
			}
			else
			{
				$r[] = $request;
			}
		}
		update_option("roles_requests", $r);
		$Data 				= static::get_rolereq_page( $args, $user );
		return $Data;
	}
	static function req_role($args, $user)
	{
		$roles				= FmRU_Expert::get_roles();
		if(in_array($args, $roles) || in_array($args, $Data->reqRoles))
		{
			$Data 			= static::get_my_roles( $args, $user );
			return $Data;
		}
		$Data 				= static::get_my_roles( $args, $user );
		$Data->reqRoles 	= !is_array($Data->reqRoles) ? [] : $Data->reqRoles;
		$Data->reqRoles[]	= $args;
		update_user_meta($user->ID, "roles_req", $Data->reqRoles);
		$requests 			= get_option("roles_requests");
		$requests 			= is_array($requests) ? $requests : array();
		if(!in_array([$user->ID, $args], $requests))
		{
			$requests[]		= [$user->ID, $args];
			update_option("roles_requests", $requests);
		}
		return $Data;
	}
	static function get_users()
	{
		$args = array(
			'role__in'     => array("Expert", "Tutor", "Project_author", "Project_member"),
			'orderby'      => 'display_name',
			'order'        => 'ASC',
			'search_columns' => array(),
			'fields'       => 'all'
		);
		$us	 			= get_users( $args );
		$users 			= array();
		foreach($us as $user)
		{
			$roles 		= array();
			foreach($user->roles as $role)
			{
				$roles[] = $role;
			}
			$users[]	= array( "id" => $user->ID, "name" => $user->display_name, "roles" => $roles );
		}
		return $users;
	}
	static function get_criteries()
	{
		$all = FmRU_Critery::get_all();
		$crits = array();
		foreach($all as $cr)
		{
			$critery 		= FmRU_Critery::get_instance($cr);
			$cdata			= new StdClass;
			$cdata->id		= $critery->id;
			$cdata->name	= $critery->get("post_title");
			$cdata->cats	= wp_get_object_terms($critery->id, FRMRU_CATEGORY, array("fielsd"=>"ids "));
			$cdata->ganres	= wp_get_object_terms($critery->id, FRMRU_GROUP, array("fielsd"=>"ids "));
			$crits[] = $cdata;
		}
		return $crits;
	}
	static function get_critery_time_data()
	{
		global $wpdb;
		$dd		= 1000;
		$all 	= FmRU::get_all_terms(FRMRU_CATEGORY);
		$clrs	= array();
		$data 	= array();
		$categories = array();
		$xDomainRange	= [1000000000000000, 0];
		$yDomainRange	= [1000000000000000, 0];
		foreach($all as $cr)
		{
			$count 		= 0;
			$c			= get_term_meta($cr->term_id, "color", true);
			$clrs[]		= $c;
			$query 		= "SELECT
	fmr.critery_id AS critery_id,
	posts.post_title AS name,
	fmr.raiting AS raiting,
	fmr.unixtime AS unixtime,
	GROUP_CONCAT(DISTINCT ftm.name) AS terms,
	GROUP_CONCAT(DISTINCT ftm.term_id) AS term_ids
FROM ".$wpdb->prefix."frmru_member_raiting AS fmr
LEFT JOIN ".$wpdb->prefix."term_relationships as rsh ON rsh.object_id=fmr.critery_id
LEFT JOIN ".$wpdb->prefix."posts as posts ON posts.ID=fmr.critery_id
LEFT JOIN ".$wpdb->prefix."term_taxonomy AS rtx ON rsh.term_taxonomy_id=rtx.term_id
LEFT JOIN ".$wpdb->prefix."terms AS ftm ON ftm.term_id=rtx.term_id
WHERE rtx.taxonomy='fmru_category' AND rtx.term_id=".$cr->term_id."
GROUP BY fmr.ID
ORDER BY term_ids, unixtime ASC;";
			$res		= $wpdb->get_results($query);
			$dots		= array();
			$date		= $res[0]->unixtime;
			$y			= $res[0]->raiting;
			foreach($res as $re)
			{
				if($re->unixtime < $date + $dd)
				{
					$y	+= $re->raiting;
				}
				else
				{
					$dots[] = [
						"x"	=> $date,
						"y"	=> $y
					];
					$xDomainRange[0] = min($xDomainRange[0], $date);
					$xDomainRange[1] = max($xDomainRange[1], $date);
					$yDomainRange[0] = min($yDomainRange[0], $y - 10);
					$yDomainRange[1] = max($yDomainRange[1], $y + 10);
					$date 	+= $dd;
					$y 		= 0;
				}
				$count++;
			}
			$data[] 		= $dots;
			$categories[] 	= ["color" => $c, "name" => $cr->name, "id" => $cr->term_id, "count" => $count];
		}
		return [ "lineColors"=>$clrs, "data"=>$data, "categories"=> $categories, "xDomainRange"=>$xDomainRange, "yDomainRange" => $yDomainRange ];
	}	
	static function get_avg_member()	
	{		
		global $wpdb;		
		$query = "SELECT ft.name, ft.term_id  AS id, AVG(fmr.raiting ) AS avg FROM ".$wpdb->prefix."term_taxonomy AS ftt LEFT JOIN ".$wpdb->prefix."term_relationships AS tr ON tr.term_taxonomy_id = ftt.term_id LEFT JOIN ".$wpdb->prefix."terms AS ft ON ft.term_id=ftt.term_id LEFT JOIN ".$wpdb->prefix."frmru_member_raiting AS fmr ON fmr.member_id=tr.object_id WHERE taxonomy='fmru_group_player'GROUP BY ftt.term_id ORDER BY ft.name ASC;";		
		$res = $wpdb->get_results($query);		
		$Data = array();		
		foreach($res as $re)		
		{			
			$icon		= get_term_meta($re->id, "icon", true);			
			$d 			= wp_get_attachment_image_src($icon, array(100, 100));			
			$cur_bgnd 	= $d[0];			
			$Data[] = [ 
				"id" 	=> $re->id, 
				"name" 	=> $re->name, 
				"avg"	=> $re->avg, 
				"icon" 	=> $cur_bgnd, 
				"color"	=> get_term_meta($re->id, "color", true)
			];		
		}		
		return $Data;	
	}
	static function get_members_in_ganre($ganre_id)
	{
		global $wpdb;
		$query = "SELECT COUNT(posts.ID)
FROM " . $wpdb->prefix . "term_relationships AS tr
LEFT JOIN " . $wpdb->prefix . "posts AS posts ON posts.ID = tr.object_id
WHERE tr.term_taxonomy_id=$ganre_id 
AND posts.post_type='" . FRMRU_PLAYER . "';";
		return (int) $wpdb->get_var($query);
	}
	static function get_all_posts_count()
	{
		global $wpdb;
		$query = "SELECT COUNT(*) FROM ".$wpdb->prefix."posts AS p
LEFT JOIN ".$wpdb->prefix."term_relationships AS tr  ON  tr.object_id=p.ID
LEFT JOIN ".$wpdb->prefix."term_taxonomy AS tt ON tt.term_taxonomy_id = tr.term_taxonomy_id 
WHERE post_type='post' AND post_status='publish' AND tt.taxonomy='fmru_diary';";
		return (int)$wpdb->get_var($query);
	}
	
	
	
	static function get_my_projects( $raitings, $user )
	{
		if($user->ID)
		{
			$owners		= array(
				"relation"		=> 'OR',
				array(
					"key"		=> "tutor_id",
					"value"		=> $user->ID),
				array(
					"key"		=> "leader_id",
					"value"		=> $user->ID),
			);
			for($i=0; $i<10; $i++)
			{
				$owners[]	= array(
					"key"		=> "member_$i",
					"value"		=> $user->ID
				);
			}
			$notowners		= array(
				"relation"		=> 'AND',
				array(
					"key"		=> "tutor_id",
					"value"		=> $user->ID,
					"compare"	=> 'NOT LIKE'
				),
				array(
					"key"		=> "leader_id",
					"value"		=> $user->ID,
					"compare"	=> 'NOT LIKE'
				)
			);
			for($i=0; $i<10; $i++)
			{
				$notowners[]	= array(
					"key"		=> "member_$i",
					"value"		=> $user->ID,
					"compare"	=> 'NOT LIKE'
				);
			}
		}
		else
		{
			$owners		= array();
			$notowners	= array();
		}
		$my_members = array();
		if($user->ID && 1)
		{
			$args1 	= array(
				"numberposts"	=> -1,
				"offset"		=> 0,
				"post_type"		=> FmRU_Member::get_type(),
				"post_status"	=> "publish",
				'orderby' 		=> 'meta_value_num',
				'meta_key' 		=> 'order',
				'order' 		=> 'ASC',
				'meta_query'	=> $owners
			);
			$mm 	= get_posts($args1);
			foreach($mm as $member)
			{
				$mem 		= FmRU_Member::get_instance($member);
				$rat 		= 0;
				$all_owners = $mem->get_owners_id();
				$experts = array();
				foreach($raitings as $rait)
				{
					if($rait['member_id'] == $mem->id)
					{
						$experts[] = $rait['expert_id'];
						if($is_expert && $rait['expert_id'] != get_current_user_id())
							continue;
						$rat += $rait['raiting'];
					}
				}
				$mmbr 		= new stdClass;
				//$mmbr->cl 	= $rat ? '' : 'noselect';
				$mmbr->id 	= $mem->id;
				$mmbr->title= $mem->get("post_title");
				$mmbr->content = wp_trim_words(apply_filters("the_content", $mem->get("post_content")), 15, "...");
				$mmbr->o 	= $mem->get_meta("order");
				$mmbr->e 	= count(array_unique($experts));
				$mmbr->img	= $mem->get_thrumbnail();
				$terms		= get_the_terms( $mem->id, FRMRU_GROUP );
				$ganres		= array( );
				$gdiff		= array( );
				if($terms)
				{
					foreach( $terms as $term)
					{
						$color 		= get_term_meta($term->term_id, "color", true);
						$icon  		= get_term_meta($term->term_id, "icon", true);
						$d 			= wp_get_attachment_image_src($icon, array(100, 100));
						$cur_bgnd 	= $d[0];
						$ganres[]	= array("id"=>$term->term_id, "icon" => $cur_bgnd, "color" => $color, "name" => $term->name);
						$gdiff[] 	= $term->term_id;
					}
				}
				$mmbr->ganres	= $ganres;
				$my_members[]	= $mmbr;
			}
		}
		return $my_members;
	}
}
<?php 
	class SMC_Post
	{
		public $id;
		public $ID;
		public $body;
		public $meta;
		static $instances;
		static $all_ids;
		static $all_posts;
		function __construct($id)
		{
			if(isset($id->ID))
			{
				$this->id		= $this->ID	= $id->ID;
				$this->body		= $id;
				//insertLog("__construct", $this->id);
			}
			else
			{
				$this->id		= $id;
				$this->body		= get_post($id);
			}
		}
		function is_enabled()
		{
			return isset($this->body->ID);
		}
		public static function get_instance($id)
		{
			$obj				= is_numeric($id) ?	$id :	$id->ID;
			if(!static::$instances)	static::$instances = array();
			if(!static::$instances[$obj])
				static::$instances[$obj] = new static($obj);
			return static::$instances[$obj];
		}
		static function insert($data)
		{
			//insertLog("SMC_Post.insert", $data);
			$id		= wp_insert_post(
				array(
					"post_type"		=> static::get_type(),
					'post_name'    	=> $data['post_name'],
					'post_title'    => $data['post_title'],
					'post_content'  => $data['post_content'],
					'post_status'   => 'publish',
				)
			);
			$post	= static::get_instance($id);
			//insertLog("insert", static::get_type() );
			//insertLog("insert", $id );
			$post->update_metas($data);
			return $post;
		}
		function doubled()
		{
			$metas		= array('post_title'=>$this->body->post_title, 'post_content'  => $this->body->post_content);
			require_once(SHM_REAL_PATH."class/SMC_Object_type.php");
			$SMC_Object_Type	= new SMC_Object_Type();
			$object				= $SMC_Object_Type->object;
			foreach($object[static::get_type()] as $key=>$val)
			{
				if($key 		== "t") continue;
				$metas[$key]	= $this->get_meta($key);
			}
			return static::insert($metas);
			return  $metas;
		}
		static function delete($id)
		{
			if(is_numeric($id))
			{
				return wp_delete_post($id);
			}
			else
			{
				return wp_delete_post($id->ID);
			}
		}
		
		function update_metas($meta_array)
		{
			$data	= array();
			foreach($meta_array as $meta=>$val)
			{
				if( $meta	== 'post_title' || $meta	== 'post_content' )
				{
					$data[$meta] = $val;
					continue;
				}
				if( $meta	== 'title' || $meta	== 'name' || $meta == 'obj_type' )
				{
					continue;
				}
				//insertLog("update_metas", array($meta, $val));
				$this->update_meta($meta, $val);
			}
			if(count($data))
			{
				$data['ID'] = $this->id;
				$id			= wp_update_post($data);
				//insertLog("update_metas", $id);
			}
		}
		public function get_meta($name)
		{
			return get_post_meta($this->id, $name, true);
		}
		public function update_meta($name, $value)
		{
			update_post_meta( $this->id, $name, $value );
			return $value;
		}
		public function get($field)
		{
			return $this->body->$field;
		}
		function set($field)
		{
			$this->body->$field	= $field;
			wp_update_post($this->body);
		}
		public function get_the_author()
		{
			global $authordata;
			$autor_id		= $this->body->post_author;
			$authordata		= get_userdata($autor_id);
			$author			= apply_filters("the_author", $authordata->display_name);
			return $author;
		}		
		
		/*
		
		*/
		static function get_random($count=1)
		{
			$args		= array(
								'numberposts'	=> $count,
								'offset'		=> 0,
								'orderby'		=> "rand",
								'post_status' 	=> 'publish',
								'fields'		=> 'all',
								'post_type'		=> static::get_type(),
			);
			$p			= get_posts($args);
			return static::get_instance($p[0]);
		}
		
		/*
		
		*/
		static function get_all($metas=-1, $numberposts=-1, $offset=0, $order_by='title', $order='DESC', $order_by_meta="", $fields="all", $relation="AND")
		{
			$args		= array(
									"numberposts"		=> $numberposts,
									"offset"			=> $offset,
									'orderby'  			=> $order_by,
									'order'     		=> $order,
									'post_type' 		=> static::get_type(),
									'post_status' 		=> 'publish',	
									'fields'			=> $fields
								);
			if($order_by == "meta_value" || $order_by == "meta_value_num") 	
				$args['meta_key']	= $order_by_meta;
			if(is_array($metas))
			{
				$arr		= array();
				foreach($metas as $key=>$val)
				{
					$ar					= array();
					$ar["value"]		= is_array($val) ? $val["value"] : $val;
					$ar["key"]			= $key;
					if(is_array($val) && !isset($val["value"]))
						$ar["operator"]	= "OR";
					else
						$ar["compare"]	= is_array($val) ? $val["compare"] : "=";
					$arr[]				= $ar;
				}
				$args['meta_query']	= array('relation'		=> $relation);
				$args['meta_query'][] 	= $arr;				
			}
			//return $args;
			self::$all_posts	=  get_posts($args);
			return self::$all_posts;
		}
		static function get_all_count( $args=-1 )
		{
			if(is_array($args ))
			{
				$args["numberposts"] = -1;
				$args['fields'] = "ids";
				$args['post_status'] = "publish";
				return count(get_posts($args));
			}
			else
			{
				global $wpdb;
				$query = "SELECT COUNT(ID) FROM $wpdb->posts WHERE post_status='publish' AND post_type='".static::get_type()."';";
				return $wpdb->get_var( $query );
			}
			/**/
		}
		
		
		
		static function get_all_ids($metas=-1, $numberposts=-1, $offset=0, $order_by='title', $order='DESC', $is_update=false)
		{
			$args		= array(
									"numberposts"		=> $numberposts,
									"offset"			=> $offset * $numberposts,
									'orderby'  			=> $order_by,
									'order'     		=> $order,
									'fields'			=> "ids",
									'post_type' 		=> static::get_type(),
									'post_status' 		=> 'publish',									
								);
			if(is_array($metas))
			{
				$arr		= array();
				foreach($metas as $key=>$val)
				{
					$ar				= array();
					$ar["value"]	= $val;
					$ar["key"]		= $key;
					$ar["compare"]	= "=";
					$arr[]			= $ar;
				}
				//$args['meta_query']	= array('relation'		=> 'AND');
				$args['meta_query'][] = $arr;
				
			}
			//insertLog("SMC_Post", array("action" => "get_all_ids", "args"=>$args));
			static::$all_ids		= get_posts($args);
			return static::$all_ids;
		}
		
		/*
			
		*/
		static function amount_meta($meta_key, $post_ids=-1)
		{
			if(!is_array($post_ids))	return 0;
			global $wpdb;
			$ids					= array();
			foreach($post_ids as $post_id)
			{
				if( $post_id instanceof SMC_Post )
					$ids[]			= $post_id->id;
				else if( $post_id instanceof WP_Post )
					$ids[]			= $post_id->ID;
				else if( is_numeric($post_id ) )
					$ids[]			= $post_id;				
			}
			$query		= "SELECT SUM(meta_value) FROM " . $wpdb->prefix . "postmeta WHERE post_id IN(" . implode(",", $ids) . ") AND meta_key='count';";
			$amount		= $wpdb->get_var($query);
			return $amount;
		}
		
		/*
			
		*/
		static function wp_dropdown($params="-1")
		{
			if(!is_array($params))
				$params	= array();
			$hubs		= self::get_all($params['args']);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->ID.". ".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;	
		}
		
		static function dropdown($data_array, $params="-1")
		{
			if(!is_array($params))
				$params	= array();
			$hubs		= $data_array;
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub['ID']."' ".selected($hub->ID, $params['selected'], false).">".$hub['ID'].". ".$hub['post_title'] . "</option>";
			}
			$html		.= "</select>";
			return $html;	
		}
		static function get_type()
		{
			return "post";
		}
		
		
		static function init()
		{
			$typee	= static::get_type();
			add_action('admin_menu',							array(get_called_class(), 'my_extra_fields'));
			add_action("save_post_{$typee}",					array(get_called_class(), 'true_save_box_data'), 10);

			
			add_filter("manage_edit-{$typee}_columns", 			array(get_called_class(), 'add_views_column'), 4);
			add_filter("manage_edit-{$typee}_sortable_columns", array(get_called_class(), 'add_views_sortable_column'));
			add_filter("manage_{$typee}_posts_custom_column", 	array(get_called_class(), 'fill_views_column'), 5, 2);
			add_filter("pre_get_posts",							array(get_called_class(), 'add_column_views_request'));
			return;	
		}
		 	
		static function add_views_column( $columns )
		{
			require_once(FMRU_REAL_PATH."class/SMC_Object_type.php");
			$SMC_Object_type	= SMC_Object_Type::get_instance();
			$obj				= $SMC_Object_type->object [forward_static_call_array( array( get_called_class(),"get_type"), array()) ];
			$posts_columns = array(
				"cb" 				=> " ",
				"IDs"	 			=> __("ID", 'smp'),
				"title" 			=> __("Title")
			);
			foreach($obj as $key=>$value)
			{
				if($key == 't' ||$key == 'class' ) continue;
				$posts_columns[$key] = isset($value['name']) ? $value['name'] : $key;
			}		
			return $posts_columns;		
		}
			
		static function fill_views_column($column_name, $post_id) 
		{	
			$p					= static::get_instance($post_id);
			require_once(FMRU_REAL_PATH."class/SMC_Object_type.php");
				
			$SMC_Object_type	= SMC_Object_type::get_instance();
			$obj				= $SMC_Object_type->object [forward_static_call_array( array( get_called_class(),"get_type"), array()) ];
			
			switch( $column_name) 
			{		
				case 'IDs':
					$color				= $p->get_meta( "color" );
					if($post_id)
						echo "<div class='ids'><span style='background-color:#$color;'>ID</span>".$post_id. "</div>
					<p>";
					break;	
				case "map":
					echo "aaa";
					break;
				default:
					if(array_key_exists($column_name, $obj))
					{
						$meta			= $p->get_meta($column_name);
						switch($obj[$column_name]['type'])
						{
							case "number":
							case "string":
								echo $meta;
								break;
							case "date":
								echo $meta ? date("d.m.Y   H:i", $meta) : "";
								break;
							case "boolean":
								echo $meta ? "<img src='" . ZOMRO_URLPATH . "img/check_checked.png'>" : "<img src='" . ZOMRO_URLPATH . "img/check_unchecked.png'>";
								break;
							case "media":
								echo "<img style='height:140px; width:auto;' src='".wp_get_attachment_image_url($meta, array(140, 140))."'/>";
								break;
							case "post":
								if($meta)
									echo "<br><div class='ids' style='background-color:#$color;'><span>ID</span>".$meta. "</div>";
								break;
							case "id":
							default:							
								$elem			= $SMC_Object_type->get_object($meta, $obj[$column_name]["object"] );
								switch( $obj[$column_name]["object"])
								{
									case "user":
										if($meta)
										{
											$user = get_user_by("id", $meta);
											$display_name = $user ? $user->display_name : "==";
											echo  $display_name."<br><div class='ids'><span>ID</span>".$meta. "</div>
												<div style='background-color:#$color; width:15px;height:15px;'></div>";
										}
										break;
									case "post":
										if($meta)
										echo "<br><div class='ids'><span>ID</span>".$meta. "</div>
											<div style='background-color:#$color; width:15px;height:15px;'></div>";
										break;
									case "taxonomy":
									default:
										echo $meta ? $elem ['title'] ."<br><div class='ids'><span>ID</span>".$meta. "</div>
											<div style='background-color:#$color; width:15px;height:15px;'></div>" : "";
										break;
								}
								echo "----";
								break;
						}
					}
					break;
			}
		}
		
		// добавляем возможность сортировать колонку
		static function add_views_sortable_column($sortable_columns)
		{
			
			return $sortable_columns;
		}
		
		// изменяем запрос при сортировке колонки	
		static function add_column_views_request( $object )
		{
			
		}	
		
		
		static function my_extra_fields() 
		{
			add_meta_box( 'extra_fields', __('Parameters' ) , array(get_called_class(), 'extra_fields_box_func'), static::get_type(), 'normal', 'high'  );
			
		}
		static function extra_fields_box_func( $post )
		{	
			$lt					= static::get_instance( $post );
			//echo "AAAA" . static::get_type();
			echo static::view_admin_edit($lt);			
			wp_nonce_field( basename( __FILE__ ), static::get_type().'_metabox_nonce' );
		}
		static function true_save_box_data ( $post_id ) 
		{
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST[static::get_type().'_metabox_nonce' ] )
			|| !wp_verify_nonce( $_POST[static::get_type().'_metabox_nonce' ], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;		
			$lt					= static::get_instance( $post_id );
			$metas				= static::save_admin_edit($lt);
			$lt->update_metas( $metas );
			return $post_id;
		}
		static function view_admin_edit($obj)
		{
			echo "<div class='smc_description'>You must override static methods <b>view_admin_edit</b> and <b>save_admin_edit</b> in class <b>" .  get_called_class() . "</b>.</div>";
		}
		static function save_admin_edit($obj)
		{
			return array();
		}
	}
?>
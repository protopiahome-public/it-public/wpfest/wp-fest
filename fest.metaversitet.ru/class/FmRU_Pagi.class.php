<?php
class FmRU_Pagi
{
	public $current;
	public $count;
	public $step;
	function __construct($current, $count, $step = 24)
	{
		$this->current 	=  $current;
		$this->count 	=  $count;
		$this->step 	=  $step;
	}
	function draw()
	{
		$max = ($this->count / $this->step);
		for($i = 0; $i < $max; $i++)
		{
			$ii = $i+1;
			$articleElements .= $i != $this->current ? "
			<li class='page-item'>
				<span class='page-link' data-fmru_type='page' data-args='1|$i'>
					$ii 
				</span>
			</li>" :
			"<li class='page-item'>
				<span class='page-link disabled' >
					$ii 
				</span>
			</li>";
		}
		$html = $max>1 ? "
		<nav class='col-md-12'>
			<ul class='pagination justify-content-center'>
				<li class='page-item'>
					<a class='page-link' aria-label='Previous'  data-fmru_type='page' data-args='1|" .($this->current-1)."' >
						<span aria-hidden='true'><i class='fa fa-caret-left'></i></span>
						<span class='sr-only'>Previous</span>
					</a>
				</li>
				$articleElements 
				<li class='page-item'>
					<a class='page-link' aria-label='Next'  data-fmru_type='page' data-args='1|" .($this->current+1)."' >
						<span aria-hidden='true'><i class='fa fa-caret-right'></i></span>
						<span class='sr-only'>Next</span>
					</a>
				</li>
			</ul>
		</nav>" : "";
		return $html;
	}
}
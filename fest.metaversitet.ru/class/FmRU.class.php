<?php
function my_rest_api_func( WP_REST_Request $request ) 
{
	//return $request->get_params();
	require_once FMRU_REAL_PATH . "class/FmRU_REST.class.php";	
	return FmRU_REST::send( $request ); // "fmru_player"
}
class FmRU
{
	static $options;
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{	
		
		static::$options = get_option(FRMRU);
		add_action("wp_head",						array(__CLASS__, "set_styles"));
		add_action("admin_head",					array(__CLASS__, "admin_head"));
		add_action("init",							array(__CLASS__, "expert_redirect"));
		add_action( 'admin_menu',					array(__CLASS__, 'admin_page_handler'), 9);
		add_filter( 'template_include', 			array(__CLASS__, 'my_template'), 9, 1);
		add_filter( "ermak_body_script", 			array(__CLASS__, "ermak_body_script"),7);// null template content
		add_filter( "ermak_body_before", 			array(__CLASS__, "ermak_body_before"),7);// null template content
		add_action( 'admin_enqueue_scripts', 		array(__CLASS__, 'add_admin_js_script') );
		add_action( 'wp_enqueue_scripts', 			array(__CLASS__, 'add_frons_js_script'),2 );
		add_filter( 'posts_clauses', 				array(__CLASS__, 'posts_clauses_with_tax'), 10, 2);
		add_filter( "frmru_slide", 					array(__CLASS__, "frmru_slide"), 10, 3);
		add_action('add_admin_bar_menus', 			function(){
			remove_action( 'admin_bar_menu', 'wp_admin_bar_wp_menu', 10 ); 
			remove_action( 'admin_bar_menu', 'wp_admin_bar_customize_menu', 40);
			remove_action( 'admin_bar_menu', 'wp_admin_bar_updates_menu', 50 );
			remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
			remove_action( 'admin_bar_menu', 'wp_admin_bar_my_account_menu', 0 );
			remove_action( 'admin_bar_menu', 'wp_admin_bar_edit_menu', 80 );
		});
		add_action( 'admin_bar_menu', 				array(__CLASS__, 'my_admin_bar_render' ),40);
		
		//REST API
		add_action( 'rest_api_init', 				array(__CLASS__, "rest_api_init") );
		
		add_action( 'admin_notices',				array(__CLASS__, 'after_install_sticker_epr') , 23);
	}
	
	static function my_admin_bar_render( $wp_admin_bar ) 
	{
		
		//global $wp_admin_bar;
		$wp_admin_bar->add_menu( array(
			'parent' 	=> false, //'false' для корневого меню
						   //или ID нужного меню
			'id' 		=> 'fmru', // ID ссылки
			'title' 	=> __('Festival settngs', FRMRU), //заголовок ссылки
			'href' 		=> "/wp-admin/admin.php?page=rrmru_page" //имя файла	
			,'icon' 	=> 'dashicons-welcome-learn-more'
			,"meta"		=> array(
				'html'	=> ""
			)
		));
	}
	
	static function get_sticker()
	{
		return "
			<div class='col-md-3 col-sm-3' style='margin-bottom: 10px;'>
				<img src='".FMRU_URLPATH."img/logo3.svg' height=100/>
			</div>
			<div class='col-md-5 col-sm-5'>
				<div class='btn btn-primary fmru_stick_install w-100' style='height:100px; padding-top:35px!important;'>".
					__("Install defult staff", FRMRU).
				"</div>
			</div>
			<div class='col-md-4 col-sm-4' >
				<fmru>" . __("Festival", FRMRU). "</fmru>
				<div>
				" .__("You may config Festival by default.", FRMRU) .
				"</div>
			</div>";
	}
	static function after_install_sticker_epr()
	{
		if(!static::$options['is_sticker']) return;
		$eb_sticker = static::get_sticker();
		echo "
		<div class='updated notice is-dismissible1' id='install_xx_notice' style='padding:30px!important; position: relative;'>
			<div class='row'>".
				$eb_sticker .
			"</div>
			<span class='smc_desmiss_button'>
				<span class='screen-reader-text'>".__("Close")."</span>
			</span>
		</div>
		
		";
	}
	
	
	static function rest_api_init()
	{
		//site.url/wp-json/get_main/1
		register_rest_route( 'get_main', '/(?P<page_type>\w+)', array(
			'methods'  => "POST",
			'callback' => 'my_rest_api_func',
		) );
	}
	static function posts_clauses_with_tax ( $clauses, $wp_query ) 
	{
		global $wpdb;
		//array of sortable taxonomies
		$taxonomies = array('example-taxonomy', 'other-taxonomy');
		if (isset($wp_query->query['orderby']) && in_array($wp_query->query['orderby'], $taxonomies)) {
			$clauses['join'] .= "
				LEFT OUTER JOIN {$wpdb->term_relationships} AS rel2 ON {$wpdb->posts}.ID = rel2.object_id
				LEFT OUTER JOIN {$wpdb->term_taxonomy} AS tax2 ON rel2.term_taxonomy_id = tax2.term_taxonomy_id
				LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
			";
			$clauses['where'] .= " AND (taxonomy = '{$wp_query->query['orderby']}' OR taxonomy IS NULL)";
			$clauses['groupby'] = "rel2.object_id";
			$clauses['orderby']  = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC) ";
			$clauses['orderby'] .= ( 'ASC' == strtoupper( $wp_query->get('order') ) ) ? 'ASC' : 'DESC';
		}
		return $clauses;
	}
	/**/
	
	
	
	static function activate()
	{
		global $wpdb, $wp_filesystem, $frmu_special_roles;
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."frmru_member_expert` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `member_id` int(125) NOT NULL,
  `expert_id` int(125) NOT NULL,
  `descr` longtext NOT NULL,
  `unixtime` int(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."frmru_member_raiting` (
  `ID` int(125) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(125) unsigned NOT NULL,
  `expert_id` int(125) unsigned NOT NULL,
  `critery_id` int(125) unsigned NOT NULL,
  `raiting` float unsigned NOT NULL DEFAULT '0',
  `unixtime` int(128) unsigned NOT NULL DEFAULT '0',
  `description` longtext NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `member_critry_expert` (`member_id`,`expert_id`,`critery_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		
		add_role( 
			"Expert", __("Expert", FRMRU) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 1,
				'edit_posts'				=> 1,
				'delete_posts'				=> 1,
				'read'						=> 1
			)
		);
		add_role( 
			"Project_author", __("Project author", FRMRU) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 1,
				'edit_posts'				=> 1,
				'delete_posts'				=> 1,
				'read'						=> 1
			)
		);
		add_role( 
			"Tutor", __("Tutor", FRMRU) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 1,
				'edit_posts'				=> 1,
				'delete_posts'				=> 1,
				'read'						=> 1
			)
		);
		add_role( 
			"Project_member", __("Project member", FRMRU) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 1,
				'edit_posts'				=> 1,
				'delete_posts'				=> 1,
				'read'						=> 1
			)
		);
		
		$options = get_option(FRMRU);
		// page for members list
		$my_page = $options['page_id'];
		if (!$my_page||FALSE === get_post_status( $my_page )) 
		{
			$my_new_page = array(
				  'post_title' => __('Festival Members', FRMRU),
				  'post_content' => '',
				  'post_status' => 'publish',
				  'post_type' => 'page'
			);
			$my_page = wp_insert_post( $my_new_page );
		}
		
		//page of festival rules
		$my_page2 = $options['page_id2'];
		if (!$my_page2||FALSE === get_post_status( $my_page2 )) 
		{
			$my_new_page = array(
				  'post_title' => __('Festival rules', FRMRU),
				  'post_content' => '',
				  'post_status' => 'publish',
				  'post_type' => 'page'
			);
			$my_page2 = wp_insert_post( $my_new_page );
		}
		
		$idd = is_array($options) && $options['member_id'] ? $options['member_id'] : 1;
		$ddd = array(
				"status"			=> 0,
				"is_register"		=> false,
				"max_ganres"		=> 4,
				"refresh_rate"		=> 0.25,
				"member_per_page"	=> 120,
				"member_card"		=> "box", 
				"is_sticker"		=> true, 
				"auth_key"			=> MD5(time()),
				"member_id"			=> $idd,
				"yandex_cloud_key"	=> "",
				"page_id"			=> $my_page,
				"page_id2"			=> $my_page2,
				"enabled_rules"		=> 1,
				"fmru_evlbl_roles"	=> [  "Expert", "Project_author", "Tutor", "Project_member" ]
			);
        update_option( FRMRU, $ddd );
		if (!file_exists(ABSPATH . '/static')) 
		{
			mkdir(ABSPATH . '/static', 0777, true);
		}
		recurse_copy( 
			FMRU_REAL_PATH . 'build/static', 
			ABSPATH . '/static'
		);
		
		//require_once(FMRU_REAL_PATH."tpl/commands_create.php");
		//default_creating();
	}
	static function deactivate()
	{
		remove_role( "Expert");
		remove_role( "Project_author");
		remove_role( "Tutor");
	}
	
	static function admin_page_handler()
	{
		add_menu_page( 
			__('Festival', FRMRU), 
			__('Festival', FRMRU),
			'manage_options', 
			'rrmru_page', 
			array(__CLASS__, 'setting_pages'), 
			FMRU_URLPATH."img/logo2.svg", // icon url  
			'2'
		);
		add_submenu_page( 
			'rrmru_page',
			__('Results', FRMRU), 
			__('Results', FRMRU),
			'manage_options', 
			'rrmru_results_page', 
			array(__CLASS__, 'get_admin'),  
			'99'
		);
	}
	static function get_all_terms( $type )
	{
		$terms = get_terms( array(
			'taxonomy'      => array( $type ), 
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'fields'        => 'all'
		));
		return $terms;
	}
	static function setting_pages()	
	{	
		global $frmu_special_roles;
		if($_POST['pt'])
		{
			echo "start<br>";
			require_once(FMRU_REAL_PATH."tpl/SpeedCloud.php");
		}
		require_once(FMRU_REAL_PATH."tpl/input_file_form.php");
		$i = 0;
		foreach(get_frmu_special_roles() as $role)
		{
			$_roles .= "
				<input type='checkbox' name='fmru_evlbl_roles[]' class='checkbox' id='fmru_evlbl_roles$i' value='$role' " . checked( 1, in_array($role, static::$options['fmru_evlbl_roles']), false) . "'/>
				<label for='fmru_evlbl_roles$i'>" . __($role, FRMRU) . "</label>
				<div class='spacer-10' ></div>";
			$i++;
		}
		$i = 0;
		foreach(get_frmu_special_roles() as $role)
		{
			$add_user_roles .= "
				<input type='checkbox' name='fmru_add_user[]' class='checkbox' id='fmru_add_user$i' value='$role' />
				<label for='fmru_add_user$i'>" . __($role, FRMRU) . "</label>
				<div class='spacer-5' ></div>";
			$i++;
		}
		
		
		$fmru_users 	= get_option("fmru_users");
		if(!is_array($fmru_users)) 
			$fmru_users = [];
		$usrs = "<table class='table table-striped' style='max-width:700px;'>
		<thead class='thead-light'>
			<tr>
				<th>".__("Login", FRMRU)."</th>
				<th>".__("Password")."</th>
				<th>".__("Roles", FRMRU)."</th>
			</tr>
		</thead>
		<tbody>";
		foreach( $fmru_users as $u)
		{
			if(username_exists( $u['login'] ))
			{
				$user = get_user_by("login", $u['login']);
				$roles = [];
				foreach( $user->roles as $r)
				{
					$roles[] = " <span> " . __($r, FRMRU) . "</span>";
				}
				$usrs .= "
				<tr>
					<td>" . $u['login'] . "</td>
					<td>" . $u['pass'] . "</td>
					<td>" . implode(", ", $roles) . "</td>
				</tr>";
			}
		}
		$usrs .= "</tbody>
		</table>";
		$html = static::get_modals() . "
		<style>
			.nav.nav-tabs 
			{
				padding: 0;
			}
		</style>
		<section>
			<div class='container'>
				<div class='row'>
					<div class='col-md-1 critery_cell2'>
						
					</div>
					<div class='col-md-11 critery_cell2'>
						<h2><img src='".FMRU_URLPATH."img/logo3.svg' height=75/> " . __("Festival", FRMRU) . "</h2>
					</div>
				</div>
				<!--form method='post'  enctype='multipart/form-data'>
					<section>
						<input type='submit' name='pt' value='yandex me'/>
					</section>
				</form-->
				<nav>
				  <div class='nav nav-tabs' id='nav-tab' role='tablist'>
					<a class='nav-item nav-link active' id='nav-home-tab' data-toggle='tab' href='#nav-home' role='tab' aria-controls='nav-home' aria-selected='true'>".
						__("Main Settings", FRMRU).
					"</a>
					<a class='nav-item nav-link' id='nav-settings-tab' data-toggle='tab' href='#nav-settings' role='tab' aria-controls='nav-settings' aria-selected='false'>".
						__("Special settings", FRMRU).
					"</a>
					<a class='nav-item nav-link' id='nav-utilities-tab' data-toggle='tab' href='#nav-utilities' role='tab' aria-controls='nav-utilities' aria-selected='false'>".
						__("Utilities", FRMRU).
					"</a>
					<a class='nav-item nav-link' id='nav-users-tab' data-toggle='tab' href='#nav-users' role='tab' aria-controls='nav-users' aria-selected='false'>".
						__("Auto-created Users", FRMRU).
					"</a>
					<a class='nav-item nav-link' id='nav-spes-tab' data-toggle='tab' href='#nav-spes' role='tab' aria-controls='nav-spes' aria-selected='false'>".
						__("Professional Settings", FRMRU).
					"</a>
					<a class='nav-item nav-link' id='nav-contact-tab' data-toggle='tab' href='#nav-contact' role='tab' aria-controls='nav-contact' aria-selected='false'>".
						__("About").
					"</a>
				  </div>
				</nav>
				
				<div class='tab-content' id='nav-tabContent'>
				  <div class='tab-pane fade show active' id='nav-home' role='tabpanel' aria-labelledby='nav-home-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Status", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<div class='row'>
									<div class='col-md-6 col-sm-12'>
										<input type='radio' name='status' class='radio_full' id='status0' value='".BEFORE_START_PHASE."' " . checked(BEFORE_START_PHASE, static::$options['status'], false) . "'/>
										<label for='status0' data-hint='" . __("Before start Festival", FRMRU) . "'>
											<div class='cat_color_label " . 
												(static::$options['status'] == BEFORE_START_PHASE ? "ggreen" : "llight") . 
											"'> </div>
										</label>
										<div class='spacer-10' ></div>
									</div>
									<div class='col-md-6 col-sm-12'>
										<input class='form-control' id='exampleFormControlInput1' >
									</div>
									<div class='col-md-6 col-sm-12'>								
										<input type='radio' name='status' class='radio_full' id='status1' value='".PREPAIRE_PHASE."' " . checked(PREPAIRE_PHASE, static::$options['status'], false) . "'/>
										<label for='status1' data-hint='" . __("Project preparation phase", FRMRU) . "'>
											<div class='cat_color_label " . 
												(static::$options['status'] == PREPAIRE_PHASE ? "ggreen" : "llight") . 
											"'> </div>
										</label>
										<div class='spacer-10' ></div>
									</div>
									<div class='col-md-6 col-sm-12'>
										<input class='form-control' id='exampleFormControlInput1' >
									</div>
									<div class='col-md-6 col-sm-12'>
										<input type='radio' name='status' class='radio_full' id='status2' value='".PRESENTATION_PHASE."' " . checked(PRESENTATION_PHASE, static::$options['status'], false) . "'/>
										<label for='status2' data-hint='" . __("Project submission phase", FRMRU) . "'>
											<div class='cat_color_label " . 
												(static::$options['status'] == PRESENTATION_PHASE ? "ggreen" : "llight") . 
											"'> </div>
										</label>
										<div class='spacer-10' ></div>
									</div>
									<div class='col-md-6 col-sm-12'>
										<input class='form-control' id='exampleFormControlInput1' >
									</div>
									<div class='col-md-6 col-sm-12'>
										<input type='radio' name='status' class='radio_full' id='status3' value='".AFTER_FINISH_PHASE."' " . checked(AFTER_FINISH_PHASE, static::$options['status'], false) . "'/>
										<label for='status3' data-hint='" . __("Festival was closed", FRMRU) . "'>
											<div class='cat_color_label " . 
												(static::$options['status'] == AFTER_FINISH_PHASE ? "ggreen" : "llight") . 
											"'> </div>
										</label>
										<div class='spacer-10' ></div>
									</div>
									<div class='col-md-6 col-sm-12'>
										<input class='form-control' id='exampleFormControlInput1' >
									</div>
								</div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Logotype", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-6'>".
								get_input_file_form2( "logotype_url", static::$options['logotype_0'], "logotype_", 0 ).
							"</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Default Member thrumbnail", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-6'>".
								get_input_file_form2( "default_member_thrumb", static::$options['default_member_thrumb'], "default_member_thrumb", 0 ).
							"</div>
						</li>
					</ul>				  
				  </div>
				  <div class='tab-pane fade' id='nav-settings' role='tabpanel' aria-labelledby='nav-settings-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Page of Festival SPA", FRMRU).
								"<p>
								<small>".
									__("This page is main of application. Choose &laquo;-&raquo; for first page of site", FRMRU).
								"</small>
							</div>
							<div class='col-md-8 col-sm-12'>".
								wp_dropdown_pages( array(
									'selected'         => static::$options['page_id'],
									'echo'             => false,
									'ID'			   => "page_id",
									"class"			   => "form-control",
									"show_option_none" => "-"
								) ) .
								"<div class='spacer-10'></div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Page of Festival rules", FRMRU).
								"<p>
								<small>".
									__("Content of this page put to &laquo;About&raquo; link.", FRMRU).
								"</small>
							</div>
							<div class='col-md-2 col-sm-6'>
								<input type='checkbox' name='enabled_rules' id='enabled_rules' class='checkbox' value='1' ".checked(1, static::$options['enabled_rules'],0)."/>
								<label for='enabled_rules'>".__("Enabled page", FRMRU)."</label>
							</div>
							<div class='col-md-6 col-sm-6'>".
								wp_dropdown_pages( array(
									'selected'         => static::$options['page_id2'],
									'echo'             => false,
									'name'			   => "page_id2",
									"class"			   => "form-control",
									"show_option_none" => "--"
								) ) .
								"<div class='spacer-10'></div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Max ganres per Member", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='number' name='max_ganres'  class='form-control'  id='max_ganres' value='" . static::$options['max_ganres'] . "'/>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Refresh rate per second", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='number' name='refresh_rate'  class='form-control'  id='refresh_rate' value='" . static::$options['refresh_rate'] . "'/>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Default count member items per page", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='number' name='member_per_page'  class='form-control'  id='member_per_page' value='" . static::$options['member_per_page'] . "'/>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Default member card type", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='radio' name='member_card' class='radio_full' id='member_card0' value='card' " . checked("card", static::$options['member_card'],0) . "/>
								<label for='member_card0' data-hint='". __("Card", FRMRU) ."'>
									<div class='cat_color_label " . 
										(static::$options['member_card'] == 'card' ? "ggreen" : "llight") . 
									"'> </div>
								</label>
								<br>
								<input type='radio' name='member_card' class='radio_full' id='member_card1' value='stroke' " . checked("stroke", static::$options['member_card'],0) . "/>
								<label for='member_card1' data-hint='". __("Stroke", FRMRU) ."'>
									<div class='cat_color_label " . 
										(static::$options['member_card'] == 'stroke' ? "ggreen" : "llight") . 
									"'> </div>
								</label>
								<br>
								<input type='radio' name='member_card' class='radio_full' id='member_card2' value='box' " . checked("box", static::$options['member_card'],0) . "/>
								<label for='member_card2' data-hint='". __("Blind box", FRMRU) ."'>
									<div class='cat_color_label " . 
										(static::$options['member_card'] == 'box' ? "ggreen" : "llight") . 
									"'> </div>
								</label>
							</div>
						</li>
						<!--li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Yadex SpeechKit Cloud API Key", FRMRU).
							"<p>	
								<small>".
									sprintf(__("This %sservice%s let to whrite commentries from voice", FRMRU),
									"<a href='https://tech.yandex.ru/speechkit/cloud/'>",
									"</a>"
									).
								"</small>
							</div>
							<div class='col-md-8 col-sm-12'>
								<input name='yandex_cloud_key' class='form-control' value='" .static::$options['yandex_cloud_key'] . "' />
							</div>
						</li-->
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Visitors can create accounts", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='checkbox' name='is_register' id='is_register' class='checkbox' value='1' ".checked(1, static::$options['is_register'],0)."/>
								<label for='is_register'></label>
								<div class='spacer-10'></div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Raiting by Horoshkola", FRMRU).
							"<p>
							<small>" .
								__("Rounded raiting from 0 to 8", FRMRU) . 
							"</small>
							</div>
							<div class='col-md-8 col-sm-12'>
								<input type='checkbox' name='horo_raiting' id='horo_raiting' class='checkbox' value='1' ".checked(1, static::$options['horo_raiting'],0)."/>
								<label for='horo_raiting'></label>
								<div class='spacer-10'></div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Include Head of Wordpress template", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='checkbox' name='include_header' id='include_header' class='checkbox' value='1' ".checked(1, static::$options['include_header'],0)."/>
								<label for='include_header'></label>
								<div class='spacer-10'></div>
							</div>
						</li>
						
						
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Evalble special roles", FRMRU).
								"<p>	
								<small>".
									__("-", FRMRU).
								"</small>
							</div>
							<div class='col-md-8 col-sm-6'>".
								$_roles  .
							"</div>
						</li>
					</ul>
				</div>
				<div class='tab-pane fade' id='nav-utilities' role='tabpanel' aria-labelledby='nav-utilities-tab'>			  
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Add Projects", FRMRU).
							"</div>
							<div class='col-md-5 col-sm-12'>
								<input type='number' class='form-control' id='add_member_count' value='0'/>									
								<div class='spacer-10'></div>
								<div class='w-100'>".
									FmRU_Member::get_ganre_swicher([ "prefix" => "chmemb", "checked" => 1 ]).
								"</div>
							</div>
							<div class='col-md-3 col-sm-12'>
								<div class='btn btn-primary' id='add_member'>".
								__("Add Projects", FRMRU).
								"</div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Add Experts", FRMRU).
								"<div>
									<div class='spacer-10' ></div>
									$add_user_roles
								</div>
							</div>
							<div class='col-md-5 col-sm-12'>
								<input type='number' class='form-control' id='add_experts_count' value='0'/>								
								<div class='spacer-10'></div>
								<div class='w-100'>".
									FmRU_Member::get_ganre_swicher([ "prefix" => "chexp", "checked" => 1 ]).
								"</div>
								<p>
								<small>".
									__("Select roles, count and ganres for creatin several users", FRMRU) .
								"</small>
							</div>
							<div class='col-md-3 col-sm-12'>
								<div class='btn btn-primary' id='frmru_add_users'>".
								__("Add Experts", FRMRU).
								"</div>
							</div>
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Next Users's ID", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='number' class='form-control' id='user_id' value='" . get_option("fmru_user_cnt") . "'/>
							</div>
							
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Next Member's ID", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input type='number' class='form-control' id='member_id' value='" . static::$options["member_id"] . "'/>
							</div>
							
						</li>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Default configuration", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>".
								static::get_sticker() .
							"</div>
						</li>
					</ul>
				</div>
				<div class='tab-pane fade' id='nav-profile' role='tabpanel' aria-labelledby='nav-profile-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								__("Secret", FRMRU).
							"</div>
							<div class='col-md-8 col-sm-12'>
								<input name='secret' disabled class='form-control'  id='fsecret' value='" . static::$options['auth_key'] . "'/>
							</div>
						</li>
					</ul>
				</div>
				<div class='tab-pane fade' id='nav-users' role='tabpanel' aria-labelledby='nav-users-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-12 col-sm-12'>
								$usrs
							</div>
						</li>
					</ul>
				</div>
				<div class='tab-pane fade' id='nav-spes' role='tabpanel' aria-labelledby='nav-spes-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-12 col-sm-12'>
								<div class='lead'>".
									__("Correct CSS-style", FRMRU).
								"</div>
							</div>
							<code><pre><textarea id='correct_css' class='col-md-12 col-sm-12' rows=10>" . 
								static::$options['correct_css'] . 
							"</textarea></pre></code>
						</li>
					</ul>
				</div>
				<div class='tab-pane fade' id='nav-contact' role='tabpanel' aria-labelledby='nav-contact-tab'>
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='col-md-4 col-sm-12'>".
								
							"</div>
							<div class='col-md-8 col-sm-12'>
								
							</div>
						</li>
					</ul>
				</div>
				<div class='spacer-10'></div>
				<button type='button' class='btn btn-primary save_setting'>" . __("Save all changes", FRMRU). "</button> <span id='is_save' style='vertical-align:top; display:inline-block; opacity:0;'>
					<svg aria-hidden='true' viewBox='0 0 512 512' width='40' height='40' xmlns='http://www.w3.org/2000/svg'>
						<path fill='green' d='M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z'></path>
					</svg>
				</span>			
				
				
				
			</div>
		</section>";
		echo $html;
	}
	static function my_template($template)
	{
		global $post;
		if( 
			(static::$options['page_id'] > 0 && $post->ID == static::$options['page_id']) || 
			(int)static::$options['page_id'] < 1 && is_front_page() )
			return FMRU_REAL_PATH."template/empty.php";
		if( true  )
		{
			return FMRU_REAL_PATH."template/empty.php";
		}
		return $template;
	}
	static function ermak_body_before( $text )
	{
		global $post, $wp;
		if( 
			(static::$options['page_id'] > 0 && $post->ID == static::$options['page_id']) || 
			(int)static::$options['page_id'] < 1 && is_front_page() )
		{
			$html = "
			<noscript>You need to enable JavaScript to run this app.</noscript>
			<div id='main'></div>
			<script>".
				(
					get_current_user_id() > 0 ? 
					"" : //"window.auth = 'Z2VuYWdsOnphcTEyd3M=';" : 
					""
				).
			"</script>
			<script src='". FMRU_URLPATH ."/build/static/js/main.17712a37.js' />";
			/*
			$html = static::get_modals().
			"<div class='container colored' id='main'>
				<div id='cont'>" . 
					static::get_front() . 
				"<div>
			<div>";
			
			require_once(FMRU_REAL_PATH."tpl/slider.php");
			//require_once FMRU_REAL_PATH . "react/App.php";	
			$html = static::get_modals(). 
			get_slider_( 
				//get_slide_(get_react(), "", "", "main")
				get_slide_(static::get_front(), "", "", "main")
				//.( current_user_can("manage_options") ? get_slide_(static::get_admin(), "",  "", "admin") : "" )
			);		
			//$html = static::get_modals().get_react();
			*/
		}
		else if($post->post_type == FRMRU_PLAYER)
		{
			$html = FmRU_Member::get_screen( $post );
		}
		else if( stripos(add_query_arg( [], $wp->request ), "mru_member" ) )
		{
			$d = add_query_arg( [], $wp->request );
			$dd = explode("fmru_member_", $d);
			//$html = $dd[1];
			
			$html = "
			<noscript>You need to enable JavaScript to run this app.</noscript>
			<div id='main'></div>
			<script>
				window.fmru_member = " . $dd[1] . ";
			</script>
			<script src='". FMRU_URLPATH ."/build/static/js/main.17712a37.js' />";
		}
		else if(is_404())
		{
			require_once(FMRU_REAL_PATH."tpl/404.php");
			$html = FmRU404();
		}
		return $text . $html;
	}
	static function get_raiting_data()
	{
		global $wpdb;
		$query = "SELECT * FROM " . $wpdb->prefix . "frmru_member_raiting";
		return $wpdb->get_results($query, ARRAY_A );
	}
	static function get_front( $page_number = 0)
	{	
		require_once(FMRU_REAL_PATH."tpl/header.php");
		$pages = 36;
		
		//if(!current_user_can("manage_options")) 	return "<h1>Приносим извинения. Идут технические работы</h1>";
		$is_expert = FmRU_Expert::is_expert();
		$raitings = static::get_raiting_data();
		$all_memebers = FmRU_Member::get_all(-1, $pages, $page_number * $pages, "meta_value_num", "ASC", "order");
		
		$max = FmRU_Member::get_all_count();
		$pagin = new FmRU_Pagi( $page_number, $max, $pages );
		$pag = $pagin->draw();
		
		$members = "";
		foreach($all_memebers as $member)
		{
			$mem = FmRU_Member::get_instance($member);
			$rat = 0;
			$experts = array();
			foreach($raitings as $rait)
			{
				if($rait['member_id'] == $mem->id)
				{
					$experts[] = $rait['expert_id'];
					if($is_expert && $rait['expert_id'] != get_current_user_id())
						continue;
					$rat += $rait['raiting'];
				}
			}
			$xperts = count(array_unique($experts));
			$members .= $mem->get_large_form(array("experts"=>$xperts, "valuations"=>$rat ));
			/*	
			$class = $rat ? '' : ' noselect ';
			$_xperts = $xperts ? "
				<div class='xperts'>".
					$xperts.
				"</div>" : "";
			$members .= "
			<div class='col-md-2 col-sm-3 col-6'>". 
				//"<a href='" . get_permalink($mem->id) . "'>".
					"<div class='member $class' mid='".$mem->id."' frmru_type='" . FmRU_Member::get_type() . "' frmru_args='".$mem->id."'>
						<img src='" . FMRU_URLPATH . "img/empty.png' class='ssfr'/>
						<div class='member_title'>" .
							$mem->get_meta("order") .
						"</div>
						$_xperts
					</div>".
				//"</a>".
			"</div> ";
			*/
		}
		$logo = wp_get_attachment_image_src(static::$options['logotype_0'], "full")[0];
		$html = "
		<div class='container text-center'>".
			FmRU_header().
			"<!--section id='head' style=''>
				<div class='row'>
					<div class='col-xs-12 col-md-3 col-md-pull-4 col-sm-12'> ".
						
					"</div>  	
					<div class='col-xs-12 col-md-6 col-sm-12'>
						<a href='/'><img src='$logo' class='fmru_logo'/></a>
					</div>		
					<div class='col-xs-12 col-md-3 col-md-push-4 col-sm-12 login_form' style='margin-bottom:10px;'> " .
						static::get_login_form().
					"</div>	
				</div>
			</section-->
			<section id='members'>
				<div class='row'>	
					$pag
				</div>
				<div class='row'>	
					$members
				</div>
				<div class='row'>	
					$pag
				</div>
			</section>
		</div>
		";
		return $html;
	}
	static function add_frons_js_script()
	{		
		//js
		
		wp_register_style("main", FMRU_URLPATH . 'build/static/css/main.66ecfe85.css', array());
		wp_enqueue_style( "main");
		wp_register_script("bootstrap", plugins_url( '../js/bootstrap.min.js', __FILE__ ), array());
		wp_enqueue_script("bootstrap");	
		wp_register_script(FRMRU, plugins_url( '../js/fmru.js', __FILE__ ), array());
		wp_enqueue_script(FRMRU);
		wp_register_script("social-likes", 'https://cdn.jsdelivr.net/npm/social-likes/dist/social-likes.min.js', array());
		wp_enqueue_script("social-likes");
		
		//css
		wp_register_style(FRMRU, FMRU_URLPATH . 'build/assets/css/fmru.css', array());
		wp_enqueue_style( FRMRU);
		wp_register_style("bootstrap", FMRU_URLPATH . 'css/bootstrap.min.css', array());
		wp_enqueue_style( "bootstrap");
		wp_register_style("dashicons", "/wp-includes/css/dashicons.min.css?ver=4.9.6", array());
		wp_enqueue_style( "dashicons");
		wp_register_style("social-likes", "https://cdn.jsdelivr.net/npm/social-likes/dist/social-likes_birman.css", array());
		wp_enqueue_style( "social-likes");
		
		wp_register_style(
			"font-awesome", 
			"https://use.fontawesome.com/releases/v5.1.1/css/all.css", 
			array()
		);
		wp_enqueue_style( 'font-awesome');
		/*	*/
		

		//ajax
		wp_localize_script( 'jquery', 'myajax', array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('myajax-nonce')
		));		
	}
	static function add_admin_js_script()
	{	
			//css
			wp_register_style("bootstrap", FMRU_URLPATH . 'css/bootstrap.min.css', array());
			wp_enqueue_style( "bootstrap");
			wp_register_style("bootstrap-grid", FMRU_URLPATH . 'css/bootstrap-grid.css', array());
			wp_enqueue_style( "bootstrap-grid");
			wp_register_style("bootstrap-reboot", FMRU_URLPATH . 'css/bootstrap-reboot.css', array());
			wp_enqueue_style( "bootstrap-reboot");
			wp_register_style("formhelpers", FMRU_URLPATH . 'css/bootstrap-formhelpers.min.css', array());
			wp_enqueue_style( "formhelpers");
			wp_register_style(FRMRU, FMRU_URLPATH . 'css/fmru.css', array());
			wp_enqueue_style( FRMRU);
						
			wp_register_script("fmru_admin", plugins_url( '../js/fmru_admin.js', __FILE__ ), array());
			wp_enqueue_script("fmru_admin");
		
			//ajax
			wp_localize_script( 
				'jquery', 
				'myajax', 
				array(
					'url' => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('myajax-nonce')
				)
			);		
			wp_localize_script( 
				'jquery', 
				'mpp', 
				array(
					'url' => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('mpp-nonce')
				)
			);		
		$yes = substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/post.php?post=") && $_GET['action'] == "edit";
		$yes = $yes || $_SERVER["REQUEST_URI"] == "/wp-admin/admin.php?page=rrmru_page";
		$yes = $yes || $_SERVER["REQUEST_URI"] == "/wp-admin/admin.php?page=rrmru_results_page";
		$yes = $yes || substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/users.php");
		$yes = $yes || substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/term.php?taxonomy=fmru_group_player");
		//$yes = $yes || substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/edit.php?post_type=fmru_player");
		$yes = $yes || substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/edit-tags.php?taxonomy=fmru_group_player");
		$yes = $yes || $_SERVER["REQUEST_URI"] == "/wp-admin/edit-tags.php?taxonomy=fmru_category";
		$yes = $yes || substr_count ($_SERVER["REQUEST_URI"], "/wp-admin/term.php?taxonomy=fmru_category");
		
		//if( !$yes ) return;
		
			
			
			
			//js
			wp_register_script("bootstrap", plugins_url( '../js/bootstrap.min.js', __FILE__ ), array());
			wp_enqueue_script("bootstrap");
			wp_register_script("formhelpers", plugins_url( '../js/bootstrap-formhelpers.min.js', __FILE__ ), array());
			wp_enqueue_script("formhelpers");
			wp_register_style("frmu_admin", FMRU_URLPATH . 'css/frmu_admin.css', array());
			wp_enqueue_style( "frmu_admin");
					
			//wp_register_script("fmru_admin", plugins_url( '../js/fmru_admin.js', __FILE__ ), array());
			//wp_enqueue_script("fmru_admin");
			wp_register_script(FRMRU, plugins_url( '../js/fmru.js', __FILE__ ), array());
			wp_enqueue_script(FRMRU);
			
			// load media library scripts
			wp_enqueue_media();
			wp_enqueue_style( 'editor-buttons' );
			
			/*
			// load our js file
			wp_enqueue_script(
				'alternative-image',
				get_stylesheet_directory_uri() . '/js/alternative_image.admin.js',
				array( 'jquery' )
			);
			*/

			// add variables to our js file
			wp_localize_script(
				'alternative-image',
				'child_theme',
				array(
					'l10n'     => array(
						'uploaderTitle' => __( 'Set alternative image', 'child_theme' ),
						'uploaderButton' => __( 'Select image', 'child_theme' ),
					),
					'nonce' => wp_create_nonce( 'set_post_alternative_thumbnail_' . $post->ID ),
				)
			);
			wp_localize_script( 
				'wp-api', 
				'wpApiSettings', 
				array( 
					'root' => esc_url_raw( rest_url() ), 
					'nonce' => wp_create_nonce( 'wp_rest' )
				)
			);
				
		
		
	}
	static function ermak_body_script()
	{
		echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
		if( 
			(static::$options['page_id'] > 0 && $post->ID == static::$options['page_id']) || 
			(int)static::$options['page_id'] < 1 && is_front_page() )
		{
			/*	
			echo '
			<link rel="manifest" href="/manifest.json">
			<link href="/assets/css/fontawesome-all.css" media="all"/>
			<link rel="stylesheet" href="'. FMRU_URLPATH . 'build/assets/css/fmru.css?ver=4.9.6" />
			<link rel="stylesheet" href="http://fest3.metaversitet.ru/wp-includes/css/dashicons.min.css?ver=4.9.6" media="all"/>
			<script src="'. FMRU_URLPATH . 'build/assets/js/jquery-1.10.2.min.js"></script>
			<script src="'. FMRU_URLPATH . 'build/assets/js/bootstrap.min.js"></script>
			';
			
			$html = '				
			<script src="https://fb.me/react-0.14.5.js"></script>
			<script src="https://fb.me/react-dom-0.14.5.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.24/browser.min.js"></script>
			<link rel="stylesheet/less" id="less-css"  href="http://fest3.metaversitet.ru/wp-content/plugins/fest.metaversitet.ru/less/fmru.less" type="text/css" media="all" />';
			*/
		}
		if(!current_user_can("manage_options"))
		{
			echo "
			<style>
				#wpadminbar
				{
					display:none;
				}
				html 
				{
					margin-top: 0px!important;
				}
			</style>";
		}
	}
	static function get_modals()
	{
		
		$form1 = wp_login_form( array(
			'echo'           => false,
			'redirect'       => site_url( $_SERVER['REQUEST_URI'] ), 
			'form_id'        => 'loginform',
			'label_username' => __( 'Username' ),
			'label_password' => __( 'Password' ),
			'label_remember' => __( 'Remember Me' ),
			'label_log_in'   => __( 'Log In' ),
			'id_username'    => 'user_login',
			'id_password'    => 'user_pass',
			'id_remember'    => 'rememberme',
			'id_submit'      => 'wp-submit',
			'remember'       => true,
			'value_username' => NULL,
			'value_remember' => false 
		) );
		$form2 = "
			<ul class='list-group'>
				<li class='list-group-item'>
					<label class='col-sm-5 small'>".
						__("How can we call you?", FRMRU).
					"</label>
					 <div class='col-sm-7'>
						<input name='login'  class='form-control'  id='ftell'/>
					</div>
				</li>
				<li class='list-group-item'>
					<label class='col-sm-5 small'>".
						__("Enter secret word", FRMRU).
					"</label>
					<div class='col-sm-7'>
						<input name='secret'  class='form-control'  id='fsecret' />
					</div>
				</li>
				<li class='list-group-item'>
					<label class='col-sm-5 small'>".
						__("Enter your e-mail", FRMRU).
					"</label>
					<div class='col-sm-7'>
						<input name='email'  type='email' class='form-control' id='femail' />
					</div>
					<div class='col-sm-12 text-center small'>".
						 __("this field is only populated on the first visit", FRMRU ) .
					"</div>
				</li>
			</ul>
		</div>
		<div class='modal-footer'>
			<button type='button' class='btn btn-secondary' data-dismiss='modal'>" . __("Close"). "</button>
			<button type='button' class='btn btn-primary frmru_login'>" . __("Log in"). "</button>";
		
		$html = "
		<!-- Modal LOGIN -->
		<div class='modal fade' id='loginModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
		  <div class='modal-dialog' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title' id='exampleModalLabel'>".
						__("Log in please", FRMRU) . 
					"</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
					<div class='modal-body'>
						$form1					
					</div>
			</div>
		  </div>
		</div>
		<!-- Modal EMPTY -->
		<div class='modal fade' id='emptyModal' tabindex='-1' role='dialog' aria-labelledby='titleModal' aria-hidden='true'>
		  <div class='modal-dialog' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title' id='titleModal'>".
						
					"</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<div class='modal-body' id='contentModal'>
									
				</div>
				<div class='modal-footer'>
					<!--button type='button' class='btn btn-secondary' data-dismiss='modal'>" . __("Close"). "</button-->
					<button type='button' class='my_button' id='enterModal'>
						<span class='dashicons dashicons-controls-play'></span>
					</button>
				</div>
			</div>
		  </div>
		</div>";
		return $html;
	}
	public static function get_login_form()
	{
		$html = !is_user_logged_in() ? 
			//"<button type='button' class='btn btn-link' data-toggle='modal' data-target='#loginModal' style=''>".
				//__("Log in for experts", FRMRU).
				"<div class='fmRU_button light xl'  data-toggle='modal' data-target='#loginModal' >
					<span class='dashicons dashicons-admin-users'></span>
				</div>" 
			//. "</button>" 
			:
			"
			<a href='/wp-admin/profile.php' title='".__("Edit profile",FRMRU)."' class='btn btn-link' style='float:left; color:#FFF;'>" .
				wp_get_current_user()->display_name . 
			"</a>
			<a href=".wp_logout_url(get_permalink())." title='".__("Log out")."' class='fmRU_button light xl' style='color:#FFF;'>
				<span class='dashicons dashicons-migrate'> </span>
			</a>";
		return $html;
	}
	static function set_styles()
	{
		echo "
		<script>
			var is_admin = " . (current_user_can("manage_options") ? 1 : 0) . ";
			var voc = new Array();
			voc['Attantion'] = '". __("Attantion", FRMRU)."';
			voc['Send description'] = '". __("Send description", FRMRU)."';
			voc['Select category please'] = '". __("Select category please", FRMRU)."';
			voc['Insert no empty title'] = '". __("Insert no empty title", FRMRU)."';
			voc['Set text'] = '". __("Set text", FRMRU)."';
			voc['Change'] = '". __("Change", FRMRU)."';
			voc['Browse'] = '". __("Browse", FRMRU)."';
			voc['Expert'] = '". __("Expert", FRMRU)."';
			voc['Experts'] = '". __("Experts", FRMRU)."';
			voc['No exist yet.'] = '". __("No exist yet.", FRMRU)."';
			voc['Log in please'] = '". __("Log in please", FRMRU)."';
			voc['How can we call you?'] = '". __("How can we call you?", FRMRU)."';
			voc['Enter your e-mail'] = '". __("Enter your e-mail", FRMRU)."';
			voc['Log in for experts'] = '". __("Log in for experts", FRMRU)."';
			voc['Project author'] = '". __("Project author", FRMRU)."';
			voc['Successful'] = '". __("Successful", FRMRU)."';
			voc['Member'] = '". __("Member", FRMRU)."';
			voc['Members'] = '". __("Members", FRMRU)."';
			voc['Order'] = '". __("Order", FRMRU)."';
			voc['Valuations:'] = '". __("Valuations:", FRMRU)."';
			voc['Valuations'] = '". __("Valuations", FRMRU)."';
			voc['Raiting:'] = '". __("Raiting:", FRMRU)."';
			voc['Raiting'] = '". __("Raiting", FRMRU)."';
			voc['Experts, who put raiting:'] = '". __("Experts, who put raiting:", FRMRU)."';
			voc['Critery'] = '". __("Critery", FRMRU)."';
			voc['Categories'] = '". __("Categories", FRMRU)."';
			voc['Category'] = '". __("Category", FRMRU)."';
			voc['Basic Criteries'] = '". __("Basic Criteries", FRMRU)."';
			voc['Unique Criteries from Experts'] = '". __("Unique Criteries from Experts", FRMRU)."';
			voc['Put title of your critery'] = '". __("Put title of your critery", FRMRU)."';
			voc['Without fail choose parent category'] = '". __("Without fail choose parent category", FRMRU)."';
			voc['Expert-author'] = '". __("Expert-author", FRMRU)."';
			voc['Color'] = '". __("Color", FRMRU)."';
			voc['Expert\'s commentaries:'] = '". __("Expert's commentaries:", FRMRU)."';
			voc['Send description'] = '". __("Send description", FRMRU)."';
			voc['Set text'] = '". __("Set text", FRMRU)."';
			voc['Start comment'] = '". __("Start comment", FRMRU)."';
			voc['You allready send description for'] = '". __("You allready send description for", FRMRU)."';
			voc['Nobody no comment this.'] = '". __("Nobody no comment this.", FRMRU)."';
			voc['You must send description for maximal choice!'] = '". __("You must send description for maximal choice!", FRMRU)."';
			voc['Creation of new critery'] = '". __("Creation of new critery", FRMRU)."';
			voc['Create new critery'] = '". __("Create new critery", FRMRU)."';
			voc['Insert no empty title'] = '". __("Insert no empty title", FRMRU)."';
			voc['Select category please'] = '". __("Select category please", FRMRU)."';
			voc['Browse'] = '". __("Browse", FRMRU)."';
			voc['Change'] = '". __("Change", FRMRU)."';
		</script>";
	
	}
	static function admin_head()
	{
		if(!current_user_can("manage_options"))
		echo "
		<style>
			.user-rich-editing-wrap,
			.user-admin-color-wrap,
			.user-comment-shortcuts-wrap,
			.user-admin-bar-front-wrap,
			.user-language-wrap,
			.user-sessions-wrap,
			#adminmenumain
			{
				display:none;
			}
		</style>";
		echo "
		<style>
			th.column-order
			{
				width:100px;
			}
			th.column-valuations
			{
				width:150px;
			}
			th.column-image
			{
				width:200px;
			}
		</style>
			
		</script>";
		static::set_styles();
	}
	static function expert_redirect()
	{
		if( !current_user_can("manage_options") AND strpos($_SERVER['REQUEST_URI'], "wp-admin") AND !strpos($_SERVER['REQUEST_URI'], "profile.php"))
		{
			//wp_redirect("http://" . $_SERVER['HTTP_HOST'] . "/wp-admin/profile.php");
		}
		
		header("Cache-Control: no-cache, must-revalidate");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Content-Type: application/xml; charset=utf-8");
	}
	static function frmru_slide($html, $type, $args="")
	{
		switch($type)
		{
			case FRMRU_PLAYER:
				$html = FmRU_Member::get_screen( get_post($args[0]) ); // member post id
				return array("content" => $html, "url_key"=> FRMRU_PLAYER, "url_val" => $args[0]);
			case "page":
			default:
				$html = static::get_front( $args[1] );
				return array("content" => $html, "url_key"=> "", "url_val" => "");
		}
	}
	static function get_admin()
	{
		require(FMRU_REAL_PATH . "tpl/utilities.php");		
		require(FMRU_REAL_PATH . "tpl/utilities2.php");		
		file_put_contents( FMRU_REAL_PATH . "results/full_expert_list.csv" , get_full_expert_list( ));
		file_put_contents( FMRU_REAL_PATH . "results/members_results.csv" , get_fmru_members_csv("csv"));
		file_put_contents( FMRU_REAL_PATH . "results/experts_results.csv" , get_experts_member("csv"));
		$html = "
		<section>
			<div class='container'>
			
				
				<div class='row'>
					<div class='col-md-12 critery_cell2'>
						<h2>" . __("Statistics", FRMRU) . "
						<div class='pull-right'>
							<a class='btn btn-link' >" . 
								__("Get results by *.csv", FRMRU) . 
							":</a>
							<a class='btn btn-link' href='".FMRU_URLPATH . "results/members_results.csv'>".
								__("Members", FRMRU) . 
							"</a>
							<a class='btn btn-link' href='".FMRU_URLPATH . "results/experts_results.csv'>" . 
								__("Experts", FRMRU) . 
							"</a>
							<a class='btn btn-link' href='".FMRU_URLPATH . "results/full_expert_list.csv'>" . 
								__("Full statistics aroud Experts", FRMRU) . 
							"</a>
						</div></h2>
					</div>
				</div>
				<!-- Nav tabs -->
				<ul class='nav nav-tabs' role='tablist'>
					<li role='presentation' class='active'>
						<a href='#home' aria-controls='home' role='tab' data-toggle='tab'>" . 
							__("Members's statistics", FRMRU) . 
						"</a>
					</li>
					<li role='presentation'>
						<a href='#profile' aria-controls='profile' role='tab' data-toggle='tab'>" . 
							__("Experts's statistics", FRMRU) . 
						"</a>
					</li>
				</ul>
				
				<div class='tab-content'>
					<div role='tabpanel' class='tab-pane active' id='home'>					
						<div class='row'>
							<div class='col-md-12 critery_cell2'>
								<div class='display-4'>" . __("Members's statistics", FRMRU) . "</div>
							</div>
						</div>
						<div class='row'>			
						</div>
						<div class='row'>						
							<div class='col-md-12'>".
								get_fmru_members_csv("table").						
							"</div>
						</div>				
					</div>
					<div role='tabpanel' class='tab-pane' id='profile'>
						<div class='row'>
							<div class='col-md-12 critery_cell2'>
								<div class='display-4'>" . __("Experts's statistics", FRMRU) . "</div>
							</div>
						</div>
						<div class='row'>		
						</div>
						<div class='row'>						
							<div class='col-md-12'>".
								get_experts_member("table").						
							"</div>
						</div>	
					</div>
				</div>
								
				
				
			</div>
		</section>
		";
		echo $html;
	}
}


function _get_media($media_id, $size=300)
{
	$src	= $size == "full" ?  wp_get_attachment_image_src($media_id, $size): wp_get_attachment_image_src($media_id, array($size, $size));
	if($src)
	{
		return "<img style='height:auto; width:".$size."px;' src='".$src[0]."'/>";
	}
	else
	{
		return "<img style='opacity:1; height:".$size."px; width:auto;' src='"._get_default( $file_url )."'/>";
	}
}
function _get_default($file_url="")
{
	return FMRU_URLPATH."/img/empty.png";
}

function recurse_copy($src, $dst) 
{ 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file, $dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file, $dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}
<?php

class FmRU_Expert //extends SMC_Post
{
	static $instances;
	static function get_instance( $id )
	{
		if(!static::$instances)
			static::$instances = array();
		if(!static::$instances[$id])
			static::$instances[$id] = new static ($id);
		return static::$instances[$id];
	}
	public $user_id;
	public $user;
	public function __construct( $id )
	{
		$this->user_id = $id;
		$this->user = get_user_by("id", $id);
	}
	static function init()
	{	
		add_action( 'show_user_profile', 			array(__CLASS__, 'extra_user_profile_fields' ),9);		
		add_action( 'edit_user_profile', 			array(__CLASS__, 'extra_user_profile_fields' ),9);
		add_filter('manage_users_columns' , 		array(__CLASS__, 'add_extra_user_column'), 2);	
		add_action('manage_users_custom_column',	array(__CLASS__, 'user_column_content'), 99, 3);
		//add_action('init',			array(__CLASS__, 	'add_class'), 80 );		
		//parent::init();
	}
	static function extra_user_profile_fields($user)
	{
		echo '
			<table class="form-table clear_table1">
				<tr>
					<th>
						<label>'. __("Is expert", FRMRU).'</label>
					</th>
					<td>
						<input type="checkbox" class="checkbox" id="is_expert_'.$user->ID.'" name="is_expert" value="1" />
						<label for="is_expert_'.$user->ID.'" />
					</td>
				</tr>
			</table>
		';
		//$user->display_name;
	}
	static function add_extra_user_column($columns)
	{
		unset($columns['role']);
		$columns['is_expert'] = __("Roles", FRMRU);
		return $columns;
	}
	function user_column_content($value, $column_name, $user_id) 
	{
		global $frmu_special_roles;
		switch($column_name )
		{
			case "is_expert";
				$user = get_user_by("id", $user_id);
				$html = "
				<section>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_admin_$user_id' value='1' " . checked(1, (int)static::is_user_role("administrator", $user_id), 0) . " role_user_id='$user_id' role='administrator'/>		<label for='is_admin_$user_id'>".__("Administrator")."</label>
					</div>
					<div class='spacer-5'></div>";
				foreach($frmu_special_roles as $r)
				{
					if( in_array($r, FmRU::$options['fmru_evlbl_roles'])) 
						$html .= "<div class='row'>
							<input type='checkbox' class=checkbox id='is_".$r."_$user_id' value='1' " . checked(1, (int)static::is_user_role($r , $user_id), 0) . " role_user_id='$user_id' role='$r'/>		<label for='is_".$r."_$user_id'>".__( $r, FRMRU )."</label>
						</div>
						<div class='spacer-5'></div>";
				}
				return $html;
				return "
				<section>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_admin_$user_id' value='1' " . checked(1, (int)static::is_user_role("administrator", $user_id), 0) . " role_user_id='$user_id' role='administrator'/>		<label for='is_admin_$user_id'>".__("Administrator")."</label>
					</div>
					<div class='spacer-5'></div>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_expert_$user_id' value='1' " . checked(1, (int)static::is_user_role("Expert", $user_id), 0) . " role_user_id='$user_id' role='Expert'/>		<label for='is_expert_$user_id'>".__("Expert", FRMRU)."</label>
					</div>
					<div class='spacer-5'></div>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_tutor_$user_id' value='1' " . checked(1, (int)static::is_user_role("Tutor", $user_id), 0) . " role_user_id='$user_id' role='Tutor'/>		<label for='is_tutor_$user_id'>".__("Tutor", FRMRU)."</label>
					</div>
					<div class='spacer-5'></div>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_prauthor_$user_id' value='1' " . checked(1, (int)static::is_user_role("Project_author", $user_id), 0) . " role_user_id='$user_id' role='Project_author'/>		<label for='is_prauthor_$user_id'>".__("Project author", FRMRU)."</label>
					</div>
					<div class='spacer-5'></div>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_member_$user_id' value='1' " . checked(1, (int)static::is_user_role("Project_member", $user_id), 0) . " role_user_id='$user_id' role='Project_member'/>		
						<label for='is_member_$user_id'>".__("Project member", FRMRU)."</label>
					</div>
				</section>";
				break;
		}
		return $value;
	}
	
	public function is_critery_author( $member_id )
	{
		global $wpdb;
		$query = "
		SELECT post_id AS ID
		FROM $wpdb->postmeta
		LEFT JOIN $wpdb->posts AS posts ON posts.ID=$wpdb->postmeta.post_id 
		WHERE meta_key='expert_id'
		AND meta_value='$this->user_id'
		AND posts.post_status='publish';";
		//wp_die( $query );
		return $wpdb->get_results( $query );
	}
	
	public function insert_critery( $data, $term_id )
	{
		$critery = FmRU_Critery::insert($data);		
		wp_set_object_terms( (int)$critery->id, array( (int)$term_id ), FRMRU_CATEGORY);
		
		$ganres = get_terms( array(
			'taxonomy'      => array( FRMRU_GROUP ), 
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'fields'        => 'ids'
		));
		wp_set_object_terms( (int)$critery->id, $ganres, FRMRU_GROUP);		
		return  is_wp_error( $critery_id ) ? false : $critery;
	}
	function descript_member($member_id, $description)
	{
		global $wpdb;
		$query = "REPLACE INTO ".$wpdb->prefix."frmru_member_expert 
		VALUES (NULL, $member_id, ".$this->user_id.", '$description', " . time() . ")";
		return $wpdb->query($query);
		//
		return $wpdb->insert(
			$wpdb->prefix."frmru_member_expert",
			array(
				"expert_id"	=> $this->id, 
				"member_id"	=> $member_id, 
				"descr" 	=> $description ,
				"unixtime"	=> time()
			)
		);
	}
	function is_descript_member($member_id)
	{
		global $wpdb;
		$query = "SELECT * FROM ".$wpdb->prefix."frmru_member_expert 
		WHERE expert_id='".$this->user_id."' AND member_id='$member_id';";
		return $wpdb->get_results( $query );
	}
	function get_new_descript_form($member_id)
	{
		$member = FmRU_Member::get_instance($member_id);
		$html = !$this->is_descript_member($member_id) ? 
		"<section id='experts_descr' class='full_section-'>
			<div class='row'>
				<div class='col-md-1'></div>
				<div class='col-md-10'>
					<div class='display-4'>" .
						sprintf(__("Comment  %s", FRMRU), "<b>".$member->body->post_title."</b>"). 
					"</div>
				</div>
				<div class='col-md-1'></div>
			</div>
			<div class='spacer-10'></div>
			<div class='row'>
				<div style='width:100%'>
					<div class='col-md-6'>
						<textarea id='expert_descr' class='col-md-12' rows=7 placeholder='" . __("Start comment", FRMRU) . "'></textarea>
					</div>
					<div class='col-md-6'>
						<small>".
							__("Вы можете оставить текстовые комментарии и рекомендации для проектной команды.
							<p>Участникам Фестиваля важно получить обратную связь по вопросам:
							<ul>
								<li>Какие задачи для развития замысла могут быть рекомендованы?</li>
								<li>Какие задачи развития Вы рекомендуете для самих авторов – чему важно дальше учиться?</li>
							</ul>", FRMRU) .						
						"</small>
						<div class='spacer-10'></div>
						<p>
						<button class='btn btn-primary' id='set_expert_discr'>" . __("Insert comment", FRMRU) . "</button>
					</div>
				</div>
			</div>
		</section>" 
		: "
				<div style='width:100%; display:inline-block;'>
					<div class='col-md-12 critery_cell'>".
						__("You allready send description for", FRMRU).
					"</div>
				</div>"
		;
		return $html;
	}
	
	static function is_user_role( $role, $user_id = null ) 
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		if( ! $user )
			return false;
		return in_array( $role, (array) $user->roles );
	}
	static function get_roles( $user_id = null )
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$roles = array();
		if($user && is_array($user->roles))
		{
			foreach($user->roles as $role)
				$roles[] = $role;
		}
		return $roles;
	}
	static function is_expert( $user_id=null)
	{
		return 
			is_user_logged_in() && 
			static::is_user_role("Expert", $user_id) && 
			in_array( FmRU::$options['status'], [ PREPAIRE_PHASE, PRESENTATION_PHASE ] );
	}
	static function insert_users($data)
	{
		$fmru_users 	= get_option("fmru_users");
		$fmru_user_cnt 	= (int)get_option("fmru_user_cnt");
		if(!is_array($fmru_users)) 
			$fmru_users = [];
		$roles			= $data['roles'];
		$ganres			= $data['ganres'];
		$count			= $data['count'];
		$err = [];
		$c = 0;
		for($i = 0; $i < $count; $i++)
		{
			$pass	= substr( MD5(time() . rand(1, 10000000000000)), 0, 8);
			$name	= 'user' . $fmru_user_cnt;
			$userdata = array(
				'user_pass'       => $pass, // обязательно
				'user_login'      => $name, // обязательно
				'user_nicename'   => $name,
				'user_email'      => '',
				'display_name'    => $name,
				'nickname'        => $name,
				'first_name'      => $name,
				'last_name'       => '  ',
				'role'            => '', // (строка) роль пользователя
			);
			
			$user_id = wp_insert_user( $userdata );
			if($user_id)
			{
				$fmru_users[] = [ "login" => $name, "pass" => $pass ];
				$usser = get_user_by("id", $user_id);
				foreach($roles as $r)
				{
					$usser->add_role($r);
				}
				$fmru_user_cnt++;
				$c++;
			}
			else
				$err[] = $user_id->get_error_message();
		}
		update_option("fmru_users", $fmru_users);
		update_option("fmru_user_cnt", $fmru_user_cnt);
		return [$c, $err];
	}
}
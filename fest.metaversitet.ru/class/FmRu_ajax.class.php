<?php
class FmRU_ajax
{
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		add_action('wp_ajax_nopriv_myajax',	array(__CLASS__, 'myajax_submit') );
		add_action('wp_ajax_myajax',		array(__CLASS__, 'myajax_submit') );
		add_action('wp_ajax_myajax-admin', 	array(__CLASS__, 'myajax_submit'));	
	}
	static function myajax_submit()
	{
		$nonce = $_POST['nonce'];
		if ( !wp_verify_nonce( $nonce, 'myajax-nonce' ) ) die ( $_POST['params'][0] );
		$params	= $_POST['params'];	
		$d		= array( $_POST['params'][0], array() );				
		switch($params[0])
		{
			case "login":				
				$user = wp_signon(array(
					'user_login'    => $params[1],
					'user_password' => $params[2],
					'remember'      => true,
				), true);
				if(!is_wp_error($user))
				{
					wp_set_current_user( $user->ID, $user->user_login );
					wp_set_auth_cookie( $user->ID );
					do_action( 'wp_login', $user->user_login );
					$wp_json_basic_auth_error = true;
					$ans = "successfull";
				}
				else
				{
					$ans = $user->get_message();
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> $ans,
					)
				);
				break;
			case "frmru_add_users":	
				$data 		= $params[1];
				$roles		= $data['roles'];
				$ganres		= $data['ganres'];
				$count		= $data['count'];
				$users		= FmRU_Expert::insert_users($data);
				$d = array(	
					$params[0],
					array( 
						"a_alert" => sprintf(
							__("Create new anonimus users.<ul><li>Count: %s</li><li>Errors: %s</li></ul>", FRMRU),
							$users[0],
							implode( ", ", $users[1])
						)
					)
				);
				break;
			case "fmru_user_cnt":					
				$d = array(	
					$params[0],
					array( 
						"text"		=> $params[1],
					)
				);
				if( username_exists('user' . ($params[1])) )
				{
					$d[1]["a_alert"] = __("Users with deleting data are exists. Select more number!", FRMRU);
				}
				else
				{
					update_option("fmru_user_cnt", $params[1]);
				}
				break;
			case "save_setting":				
				update_option(FRMRU, $params[1]);
				$d = array(	
					$params[0],
					array( 
						"text"		=> $params[1],
					)
				);
				break;
			case "fmru_stick_install":					
				require_once( FMRU_REAL_PATH . "tpl/commands_create.php" );
				default_creating();
				FmRU::$options['is_sticker'] = false;
				update_option( FRMRU, FmRU::$options );
				$d = array(	
					$params[0],
					array( 
						"text"		=> $params[0],
					)
				);
				break;
			case "enabled_rules":	
				FmRU::$options['enabled_rules'] = $params[1];
				update_option( FRMRU, FmRU::$options );
				$d = array(	
					$params[0],
					array( 
						"text"		=>  $params[1],
					)
				);
				break;
			case "fmru_evlbl_roles":	
				FmRU::$options['fmru_evlbl_roles'] = $params[1];
				update_option( FRMRU, FmRU::$options );
				$d = array(	
					$params[0],
					array( 
						"text"		=>  $params[1],
					)
				);
				break;
			case "frmru_slide":	
				$slide_id = $params[1]['slide_id'];
				extract(apply_filters(
					"frmru_slide", 
					array( "content" => "content", "url_key"=>"url_key", "url_val"=>"url_val" ), 
					$params[1]['type'], 
					$params[1]['args']
				));
				$d = array(	
					$params[0],
					array( 
						'args'		=> $params[1]['args'][0],
						"slide_id"	=> $slide_id,
						"content"	=> $content,
						"url_key"	=> $url_key,
						"url_val"	=> $url_val,
						"id_disp"	=> 1,
					)
				);
				break;
			case "fill_critery":				
				$cretery_id = 97;
				$cat_id = 5;
				wp_set_object_terms( $cretery_id, array((int)$cat_id), FRMRU_CATEGORY);
				$d = array(	
					$params[0],
					array( 
						"text"		=> get_the_terms($cretery_id, FRMRU_CATEGORY),
					)
				);
				break;
			case "set_expert_discr":				
				$text = $params[1];
				$member_id = $params[2];
				if(!FmRU_Expert::is_user_role("Expert"))
				{
					$d = array(	
						$params[0],
						array( 
							"a_alert" => __("You haven't rights!", FRMRU),
						)
					);
				}
				$expert = FmRU_Expert::get_instance( get_current_user_id() );
				$succs 	= $expert->descript_member($member_id, $text);
				$d = array(	
					$params[0],
					array( 
						"succs"			=> $succs,
						"text"			=> $params[1],
						"experts_descr" => FmRU_Member::get_instance($member_id)->nn(),
					)
				);
				break;
			case "insert_expert_critery":				
				$cat_id 	= $params[1];
				$title	 	= $params[2];
				if(!FmRU_Expert::is_user_role("Expert"))
				{
					$d = array(	
						$params[0],
						array( 
							"a_alert"		=> __("You haven't rights!", FRMRU),
						)
					);
					break;
				}
				if($title == "")
				{
					$d = array(	
						$params[0],
						array( 
							"a_alert"		=> __("Insert no empty title", FRMRU),
						)
					);
					break;
				}
				$expert		= FmRU_Expert::get_instance( get_current_user_id() );				
				$data		= array(
					'post_name'		=> $title,
					'post_title'	=> $title,
					'post_content'	=> "",
					"expert_id"		=> $expert->user_id
				);
				$new	= $expert->insert_critery($data, $cat_id);
				$d = array(	
					$params[0],
					array( 
						"text"		=> $new->id,
						"a_alert"	=> sprintf(__("<p>Succsessful insert new uniq critery:<p> «%s». <p>Reload page please.", FRMRU), $new->body->post_title)
					)
				);
				break;
			case "single_raiting":				
				$expert_id 	= get_current_user_id();
				if(!FmRU_Expert::is_user_role("Expert", $expert_id))
				{
					$d = array(	
						$params[0],
						array( 
							"a_alert"		=> __("You haven't rights!", FRMRU),
						)
					);
					break;
				}
				$member_id 	= $params[1]["member_id"];
				$critery_id	= $params[1]["critery_id"];
				$raiting	= $params[1]["raiting"];
				$descr		= $params[1]["descr"];
				$member 	= FmRU_Member::get_instance($member_id);
				if( $raiting == 3 && $descr == "")
				{
					$params[1]["raiting"] 	= $member->get_raiting_critery(
						$member->get_raiting_data(), 
						$critery_id, 
					true );
					$d = array(	
						$params[0],
						array( 
							"raiting"		=> $member->get_full_raiting(),
							"text"			=> $params[1],
							"a_alert"		=> __("You must send description for maximal choice!", FRMRU),
						)
					);
					break;
				}
				$member->save_raiting( $expert_id, $critery_id, $raiting, $descr);
				$_raiting 	= $member->get_full_raiting();
				update_post_meta($member_id, "raiting", $_raiting);
				$d = array(	
					$params[0],
					array( 
						"text"			=> $params[1],
						"raiting"		=> (float)$_raiting,
					)
				);
				break;
			case "role_user_expert":				
				if(!current_user_can("manage_options"))
				{
					$d = array(
						$params[0],
						array(
							"a_alert" => __("Only Administrator can this", FRMRU)
						)
					);
					break;
				}
				$user_id = $params[1];
				$is		= $params[2];
				$role = $params[3];
				$user = get_user_by("id", $user_id);
				if($is)
				{					
					$user->add_role($role);
				}
				else
				{
					$user->remove_role($role);
				}
				$d = array(	
					$params[0],
					array( 
						$roles => $user->roles
					)
				);
				break;
			case "set_defult":
				require_once( FMRU_REAL_PATH . "tpl/commands_create.php" );
				default_creating();
				$d = array( $params[0], [] );
				break;
			case "logout":
				wp_logout();
				$d = array( $params[0], [] );
				break;
			case "add_member":
				$count = $params[1];
				$ganres = explode( ",", $params[2]);
				$n = array();
				foreach($ganres as $g)
					$n[] = (int)$g;
				require_once( FMRU_REAL_PATH . "tpl/commands_create.php" );
				$ans	= commands_create($count, $n);
				$d = array(	
					$params[0],
					array( 
						"a_alert" => sprintf(__("%s members are created", FRMRU),  count($ans) )
					)
				);
				break;
			default:
				do_action("myajax_submit", $params);
				break;
		}
		$d_obj		= json_encode(apply_filters("ajax_data", $d, $params));				
		print $d_obj;
		exit;
	}
	static function login($user)
	{
		$_user = wp_signon(array(
			'user_login'    => $user->user_login,
			'user_password' => get_user_meta($user->ID, "psw", true),
			'remember'      => true,
		), false);
		unset( $text );
		if ( is_wp_error($_user) ) 
		{
			$text = "Error: ".$_user->get_error_message();
		}
		else
		{
			$redirect = true;
		}
		return array( 
			"text"		=> $text,
			"redirect"	=> $redirect,
		);
	}
	
}
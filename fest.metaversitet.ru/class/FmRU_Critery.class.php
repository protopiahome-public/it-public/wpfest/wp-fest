<?php

class FmRU_Critery extends SMC_Post
{
	public $categories;
	function __construct($id)
	{
		parent::__construct($id);
		$this->categories = get_the_terms( $id, FRMRU_CATEGORY );
	}
	static function init()
	{	
		add_action( 'init',			array(__CLASS__, 	'add_class'), 91 );		
		add_action( 'init', 		array(__CLASS__, 	'create_taxonomy'), 12);
		add_action( 'parent_file',	array( __CLASS__, 	'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 	array(__CLASS__, 	'tax_add_admin_menus'), 11);
		//manage column for FRMRU_CATEGORY
		add_filter("manage_edit-".FRMRU_CATEGORY."_columns", array( __CLASS__,'ctg_columns')); 
		add_filter("manage_".FRMRU_CATEGORY."_custom_column",array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		// add_filter( 'manage_edit-'.FRMRU_CATEGORY.'_sortable_columns', array( __CLASS__,'sort_ctg'), 20 );
		// add_filter( 'request', 								array( __CLASS__,'column_orderby') );
		// add_filter("after-".FRMRU_CATEGORY."-table", 		array( __CLASS__,'after_ctg_table'), 10, 3);
		//add_action( FRMRU_CATEGORY.'_add_form_fields', 		array( __CLASS__, 'add_ctg'), 10, 2 );
		add_action( FRMRU_CATEGORY.'_edit_form_fields', 	array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_'.FRMRU_CATEGORY, 				array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_'.FRMRU_CATEGORY, 				array( __CLASS__, 'save_ctg'), 10);
		
		return;
		add_filter( 'parse_query', 				[ __CLASS__,'ba_admin_posts_filter' ] );
		add_action( 'restrict_manage_posts', 	[ __CLASS__,'ba_admin_posts_filter_restrict_manage_posts' ] );
		
		parent::init();
	}
	
	static function ba_admin_posts_filter( $query )
	{
		global $pagenow, $map_dropdown;
		if ( is_admin() && $pagenow=='edit.php' && static::get_type() == $query->get('post_type') ) 
		{
			$query->query_vars['tax_query'] = [];
			if (isset($_GET['ADMIN_FILTER_FIELD']) && $_GET['ADMIN_FILTER_FIELD'] > 1)
			{
				//$map = ShmMap::get_instance( $_GET['ADMIN_FILTER_FIELD'] );
				$query->query_vars['tax_query'][] = [
					'taxonomy' => FRMRU_CATEGORY,
					'field'    => 'id',
					'terms'    => $_GET['ADMIN_FILTER_FIELD']
				];
			}
			if (isset($_GET['ADMIN_FILTER_FIELD2']) && $_GET['ADMIN_FILTER_FIELD2'] > 1)
			{
				//$map = ShmMap::get_instance( $_GET['ADMIN_FILTER_FIELD'] );
				$query->query_vars['tax_query'][] = [
					'taxonomy' => FRMRU_GROUP,
					'field'    => 'id',
					'terms'    => $_GET['ADMIN_FILTER_FIELD2']
				];
			}
		}
	}

	static function ba_admin_posts_filter_restrict_manage_posts()
	{
		global $wpdb, $post, $wp_list_table, $map_dropdown;
		$current 			= isset($_GET['ADMIN_FILTER_FIELD'])? $_GET['ADMIN_FILTER_FIELD']:'';
		$current2 			= isset($_GET['ADMIN_FILTER_FIELD2'])? $_GET['ADMIN_FILTER_FIELD2']:'';
		if($post->post_type == static::get_type())
		{
			$args = array(
				'show_option_none'   => __("All Categories", FRMRU),
				'orderby'            => 'ID',
				'order'              => 'ASC',
				'echo'               => true,
				'selected'           => $current,
				'hierarchical'       => 0,
				'name'               => 'ADMIN_FILTER_FIELD',
				'id'                 => 'name',
				'class'              => 'postform',
				'taxonomy'           => FRMRU_CATEGORY,
				'hide_if_empty'      => false,
				'value_field'        => 'term_id', // значение value e option
				'required'           => false,
			); 
			wp_dropdown_categories( $args );
			
			$args = array(
				'show_option_none'   => __("all Ganres", FRMRU),
				'orderby'            => 'ID',
				'order'              => 'ASC',
				'echo'               => true,
				'selected'           => $current2,
				'hierarchical'       => 0,
				'name'               => 'ADMIN_FILTER_FIELD2',
				'id'                 => 'name',
				'class'              => 'postform',
				'taxonomy'           => FRMRU_GROUP,
				'hide_if_empty'      => false,
				'value_field'        => 'term_id', // значение value e option
				'required'           => false,
			); 
			wp_dropdown_categories( $args );
			?>
			<input name="pagenum" 			type="hidden" value="<?php echo $wp_list_table->get_pagenum();?>" />
			<input name="items_per_page" 	type="hidden" value="<?php echo $wp_list_table->get_items_per_page('per_page');?>" />
			
			<?php
		}
	}
		
	
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			'id' 			=> 'id',
			'name' 			=> __('Name'),
			'order'			=> __("Order", FRMRU),
			'color'			=> __("Color", FRMRU)
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'order': 
				$order = get_term_meta( $term_id, 'order', true ); 
				$out 		.= $order;
				break;	
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	  
			
			default:
				break;
		}
		return $out;    
	}
	static function add_ctg( $term, $tax_name )
	{
		if($term)
		{
			$term_id = $term->term_id;
			$color = get_term_meta($term_id, "color", true);
			$order = get_term_meta($term_id, "order", true);
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="color">
					<?php echo __("Color", FRMRU);  ?>
				</label> 
			</th>
			<td>
				<div class="bfh-colorpicker" data-name="color" data-color="<?php echo $color ?>">
				</div>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("Order", FRMRU);  ?>
				</label> 
			</th>
			<td>
				<input type='number' name='order' value='<?php echo $order; ?>' />
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "color", $_POST['color']);
		update_term_meta($term_id, "order", $_POST['order']);
	}
	static function get_type()
	{
		return FRMRU_CRITERY;
	}
	static function add_class()
	{
		$labels = array(
			'name' => __('Critery', FRMRU),
			'singular_name' => __("Critery", FRMRU),
			'add_new' => __("add Critery", FRMRU),
			'add_new_item' => __("add Critery", FRMRU),
			'edit_item' => __("edit Critery", FRMRU),
			'new_item' => __("add Critery", FRMRU),
			'all_items' => __("all Criteries", FRMRU),
			'view_item' => __("view Critery", FRMRU),
			'search_items' => __("search Critery", FRMRU),
			'not_found' =>  __("Critery not found", FRMRU),
			'not_found_in_trash' => __("no found Critery in trash", FRMRU),
			'menu_name' => __("Critery", FRMRU)
		);
		$args = array(
			 'labels' => $labels
			,'public' => true
			,'show_ui' => true // показывать интерфейс в админке
			,'has_archive' => true 
			,'exclude_from_search' => true
			,'menu_position' => 0.11 // порядок в меню
			,'show_in_menu' => "rrmru_page"
			,'supports' => array(  'title')
			,'capability_type' => 'page'
		);
		register_post_type(FRMRU_CRITERY, $args);
	}
	
	
	static function create_taxonomy()
	{
		register_taxonomy(
			FRMRU_CATEGORY, 
			array(FRMRU_CRITERY, "post"), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Categories", FRMRU),
					'singular_name'     => __("Category", FRMRU),
					'search_items'      => __('Search Category', FRMRU),
					'all_items'         => __('All Categories', FRMRU),
					'view_item '        => __('View Category', FRMRU),
					'parent_item'       => __('Parent Category', FRMRU),
					'parent_item_colon' => __('Parent Category:', FRMRU),
					'edit_item'         => __('Edit Category', FRMRU),
					'update_item'       => __('Update Category', FRMRU),
					'add_new_item'      => __('Add New Category', FRMRU),
					'new_item_name'     => __('New Category Name', FRMRU),
					'menu_name'         => __('Category', FRMRU),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == FRMRU_CATEGORY )
			$parent_file = 'rrmru_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'rrmru_page', 
			__("Categories", FRMRU), 
			__("Categories", FRMRU), 
			'manage_options', 
			'edit-tags.php?taxonomy='.FRMRU_CATEGORY
		);
    }
	
	static function view_admin_edit($obj)
	{
		$order		= $obj->get_meta("order");
		$expert_id	= $obj->get_meta("expert_id");
		$html	= "
		<div class='row'>
			<div class='col-md-6'>
				<div class='row'>
					<div class='col-md-6'>
						<label>" . __("Order", FRMRU) . "</label>
					</div>
					<div class='col-md-6'>
						<input type='number' value='$order' name='order'/>
					</div>
				</div>	
			</div>
			<div class='col-md-6'>
				<div class='row'>
					<div class='col-md-6'>
						<label>" . __("Expert-author", FRMRU) . "</label>
					</div>
					<div class='col-md-6'>".
						wp_dropdown_users(
							array(
									'show_option_none'  => "---",
									'echo'				=> false,
									'show'				=> 'display_name',
									'name'				=> 'expert_id',
									'class'				=> 'form-control',
									'multi'             => false,
									'selected'			=> $expert_id,
									"role"				=> "Expert"
								  )
						  );
					"</div>
				</div>	
			</div>
		</div>";
		return $html;
	}
	static function save_admin_edit($obj)
	{
		$arr = array(
			"order"		=> $_POST['order'],
			"expert_id"	=> $_POST['expert_id'],
		);
		return $arr;
	}
	
	static function add_views_column( $columns )	
	{		
		$columns = parent::add_views_column( $columns );
		$columns['category'] = __("Categories", FRMRU) . " | " . __("Ganres", FRMRU);
		return $columns; 
	}
	
	static function fill_views_column($column_name, $post_id) 
	{		
		$p	= static::get_instance($post_id);
		switch($column_name)
		{
			case "order":
				
				break;
			case "expert_id":
				$expert_id = $p->get_meta("expert_id");
				$user = get_user_by("id", $expert_id);
				echo $user ? "<a href='/wp-admin/user-edit.php?user_id=$expert_id'>".$user->display_name."</a>" : "--";
				break;
			case "category":
				echo get_the_term_list( $post_id, FRMRU_CATEGORY, "<B>".__("Categories", FRMRU). "</B>:<p>", "<br>", "</p>");
				echo get_the_term_list( $post_id, FRMRU_GROUP, "<B>".__("Ganres", FRMRU). "</B>:<p>", "<br>", "</p>");
				break;
			default:
				parent::fill_views_column($column_name, $post_id) ;
				break;
		}
		
	}
	static function get_all_by_cat($cat_id)
	{
		return get_posts( 
			array(
				
				'post_type' 		=> static::get_type(),
				'post_status' 		=> 'publish',	
				"numberposts" 		=> -1,
				'tax_query' => array(
					array(
						'taxonomy'	=> FRMRU_CATEGORY,
						'field'    	=> 'id',
						'terms'    	=> $cat_id
					)
				),
				"meta_query" => array(
					"relation"		=> "OR",
					array(
						"key"		=> "expert_id",
						"compare"	=> "NOT EXISTS"
					),
					array(
						"key"		=> "expert_id",
						"value"		=> 0,
						"compare"	=> "<"
					)
				)
			) 
		);
	}
	static function get_full_raiting_by_cat( $cat_id, $member_id, $only_mine=false )
	{
		global $wpdb;
		$query = "
			SELECT SUM(raiting) AS raiting, COUNT(DISTINCT expert_id) AS experts
			FROM " . $wpdb->prefix . "frmru_member_raiting
			WHERE critery_id IN
			(SELECT  post.ID
			FROM " . $wpdb->prefix . "posts AS post
			LEFT JOIN " . $wpdb->prefix . "term_relationships AS term ON term.object_id = post.ID
			WHERE term.term_taxonomy_id='$cat_id')
			AND member_id = '$member_id' ";
		if($only_mine)
			$query .= " AND expert_id=" . get_current_user_id();
		$res = $wpdb->get_results($query)[0];
		return (int)$res->raiting / (int)$res->experts / count(FmRU_Critery::get_all_categories());
	}
	static function get_descriptions_category($category_id, $member_id)
	{
		global $wpdb;
		$query = "		
		SELECT expert_id,description 
		FROM ".$wpdb->prefix."frmru_member_raiting 
		WHERE critery_id IN ( 
			SELECT post.ID 
			FROM ".$wpdb->prefix."posts AS post 
			LEFT JOIN ".$wpdb->prefix."term_relationships AS term ON term.object_id = post.ID 
			WHERE term.term_taxonomy_id='$category_id'
		) AND description != '' AND member_id='$member_id'";
		return $wpdb->get_results($query);
	}
	
	
	
	static function get_all_categories( $fields="all")
	{
		$args = array(
			'taxonomy' 		=> FRMRU_CATEGORY,
			'hide_empty' 	=> false,
			'orderby' 		=> 'meta_value_num',
			"fields"		=> $fields,
			'meta_query' 	=> [[
				'key' 		=> 'order',
				'type' 		=> 'NUMERIC',
			  ]],
		);
		$terms = get_terms( $args );
		return $terms;
	}
	static function get_all_uniq($member_id) 
	{
		$grids = static::get_groups_by_member($member_id);
		$args		= array(
			"numberposts"		=> -1,
			"offset"			=> 0,
			'post_type' 		=> static::get_type(),
			'post_status' 		=> 'publish',	
			'fields'			=> "ids",
			'tax_query'	=> array(
				"relation"		=> "AND",
				array(
					'taxonomy'	=> FRMRU_GROUP,
					'field'    	=> 'id',
					'terms'    	=> $grids,
					"operator"	=> "IN"
				)
			),
			"meta_query"		=> array(
				"relation"		=> "AND", 
				array(
					"key"		=> "expert_id",
					"compaire"	=> "EXISTS"
				),
				array(
					"key"		=> "expert_id",
					"value"		=> 0,
					"compare"	=> ">"
				)
			)
		);
		return get_posts($args);
	}
	
	
	
	//
	static function get_by_results($res_array, $only_titles = true)
	{
		$criteries = array();
		foreach($res_array as $res)
		{
			$obj = static::get_instance( $res->ID );
			$criteries[] = $only_titles ? $obj->body->post_title : $obj;
		}
		//var_dump($criteries);
		//wp_die();
		
		return $criteries; 
	}
	static function get_expert_new_cr_form( $member_id)
	{
		$expert = FmRU_Expert::get_instance(get_current_user_id());
		$aut_criteries = $expert->is_critery_author( $member_id );
		$is_enable = count( $aut_criteries ) >= 3;
		$html = $is_enable ? 
		"<div class='lg'>".
			sprintf(
				__("You allready create unique critery : %s", FRMRU), 
				"<b>". implode(", " , static::get_by_results( $aut_criteries, true )) . "</b>"
			) . 
		"</div>" :
		"<!-- Modal LOGIN -->
		<div class='modal fade' id='newCrModal' tabindex='-1' role='dialog' aria-labelledby='newCrLabel' aria-hidden='true'>
		  <div class='modal-dialog' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title' id='newCrLabel'>".
						__("Creation of new critery", FRMRU) . 
					"</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<div class='modal-body'>
		
						<div style='width:100%;'>
							
							<div class='col-md-12'>
								<small>".
									__("Put title of your critery", FRMRU) . 
								"</small>
								<div class='spacer-10'></div>
								<textarea id='new_critery_text' rows='8' class='col-md-12'></textarea>	
							</div>
									
							<div class='col-md-12'>
								<small>".
									__("Without fail choose parent category", FRMRU) . 
								"</small>
								<div class='spacer-10'></div>								
								<div class='col-md-12' id='select_new_cat'>
									<div class='spacer-10'></div>".
									static::get_choose_form() .
								"</div>						
								
							</div>
						</div>
				</div>
				<div class='modal-footer'>
					<div class='row'>
						<div class='col-md-12'>
							<button class='btn btn-primary col-md-12' id='insert_expert_critery'>".
								__("Create new critery", FRMRU).
							"</button>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class='spacer-10'></div>
		<div class='row'>
			<div class='col-md-3'>
				<button type='button' class='btn btn-primary col-md-12' data-toggle='modal' data-target='#newCrModal' style=''>".
					__("Create new critery", FRMRU).
				"</button>
			</div>
			<div class='col-md-9'>
				<div class='col-md-12'>
					<div class='small'>".
						sprintf(__("Now You created %s uniq Criteries",FRMRU), count($aut_criteries) ).
						"
						<div class='spacer-10'></div>									
					</div>
				</div>
				<p>
				Не более трех критериев!
				<ul>
					<li>В текстовое поле добавьте значимый для вас критерий, не учтенный в базовом списке.</li> 
					<li>Выберите категорию, к которой относится Ваш критерий</li>
					<li>Поздравляем! Теперь вы сможете оценить любую работу на этом Фестивале по созданному Вами критерию</li>
				</ul>
			</div>
		</div>";
		return $html;					
	}
	static function get_choose_form()
	{
		$all = static::get_all_categories();
		foreach($all as $cat)
		{
			$color = get_term_meta($cat->term_id, "color", true);
			$html .="
			<div class='col-md-12'>
				<input type='radio' value='$cat->term_id' id='choose_" . $cat->term_id . "' class='radio_full' name='choose_cat'/> 
				<label for='choose_" . $cat->term_id . "' data-hint='$cat->name'>
					<div class='cat_color_label' style='background-color:$color'></div>
				</label>
				
			</div>";
		}
		return $html;
	}
	
	static function count_offer_raiting($number, $count_experts)
	{
		return round($number / $count_experts, 2);
	}
	//
	static function get_groups_by_member($member_id)
	{
		$groups	= wp_get_object_terms( $member_id, FRMRU_GROUP );
		$gids	= array();
		if(!is_wp_error($groups) && count($groups))
			foreach($groups as $group)
				$gids[] = $group->term_id;
		return $gids;
	}
	static function get_by_member($member_id, $cat_id, $not_uniq = true)
	{	
		$grids = static::get_groups_by_member($member_id);
		$args = array(
			"numberposts"		=> -1,
			"offset"			=> 0,
			'post_type' 		=> static::get_type(),
			'post_status' 		=> 'publish',	
			'fields'			=> "all",
			'tax_query'	=> array(
				"relation"		=> "AND",
				array(
					'taxonomy'	=> FRMRU_CATEGORY,
					'field'    	=> 'id',
					'terms'    	=> $cat_id
				),
				array(
					'taxonomy'	=> FRMRU_GROUP,
					'field'    	=> 'id',
					'terms'    	=> $grids,
					"operator"	=> "IN"
				)
			),
		);
		if($not_uniq)			
		{
			$args["meta_query"] = array(
				"relation"		=> "OR",
				array(
					"key"		=> "expert_id",
					"compare"	=> "NOT EXISTS"
				),
				array(
					"key"		=> "expert_id",
					"value"		=> 0,
					"compare"	=> "<"
				)
			);
		}
		return get_posts( $args );
	}
	
}